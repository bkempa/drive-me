# Drive Me Web App

## Screenshots
### Homepage
![homepage.png](https://i.postimg.cc/YqDBrwWd/homepage.png)

### Search results
![search.png](https://i.postimg.cc/6qYJ4k15/wyniki-wyszukiwania.png)

### Drive details
![drive.png](https://i.postimg.cc/445T0cNk/szczegoly-przejazdu.png)

### Profile view
![profile.png](https://i.postimg.cc/3JxHQwdy/po-dodaniu-samochodu.png)

### Admin view
![admin.png](https://i.postimg.cc/J01Cnrx2/admin-uzytkownicy.png)

## Building instructions
### Building backend
```console
cd api
mvn clean install -DskipTests
```

### Building frontend
```console
cd frontend
npm run build --prod
```

### Running docker-compose
```console
docker-compose build
docker-compose up -d
```