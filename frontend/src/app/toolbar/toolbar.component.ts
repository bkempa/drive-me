import {Component, OnInit, ViewChild} from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';
import {map, shareReplay} from 'rxjs/operators';
import {NavigationEnd, Router} from "@angular/router";
import {AuthService} from "../login/service/auth.service";
import {Role} from "../user/user";
import {MatSnackBar} from "@angular/material/snack-bar";
import {MatMenuTrigger} from "@angular/material/menu";
import {MatDialog} from "@angular/material/dialog";
import {CreateNewRideDialogComponent} from "../rides/create-new-ride-dialog/create-new-ride-dialog.component";

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {
  @ViewChild('menuTrigger') menuTrigger!: MatMenuTrigger;
  dialogWidth: string = '30%';

  isHomepage: boolean = true;
  isLoginPage: boolean = false;
  isAuthenticated$ = this.authService.currentUser$.pipe(
    map(user => !!user),
    shareReplay(1)
  );

  isAdmin$ = this.authService.currentUser$.pipe(
    map(user => !!user && user.role === Role.ADMIN),
    shareReplay(1)
  );

  currentUsername$ = this.authService.currentUser$.pipe(
    map(user => user?.username),
    shareReplay(1)
  );

  constructor(private authService: AuthService, private router: Router, private _snackBar: MatSnackBar,
              public dialog: MatDialog, private mediaMatcher: MediaMatcher) {}

  ngOnInit(): void {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.isHomepage = event.url === '/';
        this.isLoginPage = event.url === '/login/0';
      }
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigateByUrl('/');
    this._snackBar.open("Wylogowano, do zobaczenia w drodze!", "Zamknij", {duration: 3000});
  }

  setDialogWidth() {
    if (this.mediaMatcher.matchMedia('(max-width: 600px)').matches)
      this.dialogWidth = '90%';
    else
      this.dialogWidth = '25%';
  }

  createNewRide() {
    this.setDialogWidth();
    this.dialog.open(CreateNewRideDialogComponent, {
      width: this.dialogWidth,
      restoreFocus: false
    });
  }
}
