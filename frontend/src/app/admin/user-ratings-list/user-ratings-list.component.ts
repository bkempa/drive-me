import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {User} from "../../user/user";
import {MatTableDataSource} from "@angular/material/table";
import {MatDialog} from "@angular/material/dialog";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatMenuTrigger} from "@angular/material/menu";
import {UserRatingService} from "../../user/user-rating/service/user-rating.service";
import {UserRating} from "../../user/user-rating/user-rating";
import {DeleteRatingDialogComponent} from "../../user/user-details/my-reviews-view/written-reviews/delete-rating-dialog/delete-rating-dialog.component";
import {UserService} from "../../user/service/user.service";
import {EditRatingDialogComponent} from "../../user/user-details/my-reviews-view/written-reviews/edit-rating-dialog/edit-rating-dialog.component";
import {forkJoin, Observable, Subject, switchMap, takeUntil} from "rxjs";
import {AuthService} from "../../login/service/auth.service";

@Component({
  selector: 'app-user-ratings-list',
  templateUrl: './user-ratings-list.component.html',
  styleUrls: ['./user-ratings-list.component.css']
})
export class UserRatingsListComponent implements OnInit, AfterViewInit {
  ratingsList!: UserRating[];
  dataSource = new MatTableDataSource<UserRating>(this.ratingsList);
  displayedColumns: string[] = ['passenger', 'driver', 'rate', 'comment', 'ratingDateTime', 'updateDateTime', 'action'];
  private destroy$ = new Subject();

  constructor(private userRatingService: UserRatingService, protected userService: UserService, public dialog: MatDialog,
              private authService: AuthService) {}

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild('menuTrigger') menuTrigger!: MatMenuTrigger;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit(): void {
    this.loadRatings();
    this.authService.userBlockedOrDeleted$.pipe(takeUntil(this.destroy$)).subscribe(() => this.loadRatings());
  }

  openDeleteDialog(userRating: UserRating) {
    const dialogRef = this.dialog.open(DeleteRatingDialogComponent, {
      data: userRating,
      restoreFocus: false
    });
    dialogRef.componentInstance.ratingDeleted.pipe(takeUntil(this.destroy$)).subscribe(() => {
      dialogRef.close();
      const index = this.ratingsList.findIndex((r) => r.id === userRating.id);
      if (index !== -1) {
        this.ratingsList.splice(index, 1);
        this.dataSource.data = this.ratingsList;
      }
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  openEditDialog(userRating: UserRating) {
    const dialogRef = this.dialog.open(EditRatingDialogComponent, {
      data: userRating,
      restoreFocus: false
    });
    dialogRef.componentInstance.ratingEdited.pipe(takeUntil(this.destroy$)).subscribe((editedRating) => {
      dialogRef.close();
      const index = this.ratingsList.findIndex((r) => r.id === userRating.id);
      if (index !== -1) {
        editedRating.updateDateTime = new Date().toISOString();
        this.ratingsList[index] = editedRating;
        this.dataSource.data = this.ratingsList;
      }
    });
  }

  numSequence(n: number): Array<number> {
    return Array(n);
  }

  private loadRatings() {
    this.userRatingService.getAllRatings().pipe(
      takeUntil(this.destroy$),
      switchMap((ratings) => {
        this.ratingsList = ratings;
        this.dataSource.data = this.ratingsList;
        const getUserObservables: Observable<[User, User]>[] = this.ratingsList.map((rating) => {
          const passengerUser = this.userService.getUserById(rating.id.passengerId);
          const driverUser = this.userService.getUserById(rating.id.driverId);
          return forkJoin([passengerUser, driverUser]);
        });
        return forkJoin(getUserObservables);
      })
    ).subscribe((usersData: [User, User][]) => {
      usersData.forEach(([passengerUser, driverUser], index) => {
        this.ratingsList[index].passengerUsername = passengerUser?.username;
        this.ratingsList[index].driverUsername = driverUser?.username;
      });
    });
  }
}
