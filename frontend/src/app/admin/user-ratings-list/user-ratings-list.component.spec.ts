import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserRatingsListComponent } from './user-ratings-list.component';

describe('UserRatingsListComponent', () => {
  let component: UserRatingsListComponent;
  let fixture: ComponentFixture<UserRatingsListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UserRatingsListComponent]
    });
    fixture = TestBed.createComponent(UserRatingsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
