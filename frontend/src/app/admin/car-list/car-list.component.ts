import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Car} from "../../car/car";
import {CarService} from "../../car/service/car.service";
import {MatTableDataSource} from "@angular/material/table";
import {MatDialog} from "@angular/material/dialog";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatMenuTrigger} from "@angular/material/menu";
import {EditCarDialogComponent} from "../../user/user-details/account-view/edit-car-dialog/edit-car-dialog.component";
import {DeleteCarAdminComponent} from "./delete-car-admin/delete-car-admin.component";
import {Subject, takeUntil} from "rxjs";
import {AuthService} from "../../login/service/auth.service";

@Component({
  selector: 'app-car-list',
  templateUrl: './car-list.component.html',
  styleUrls: ['./car-list.component.css']
})
export class CarListComponent implements OnInit, AfterViewInit {
  carsList!: Car[];
  dataSource = new MatTableDataSource<Car>(this.carsList);
  displayedColumns: string[] = ['id', 'make', 'model', 'color', 'plateNumber', 'productionYear', 'maxPassengers', 'action'];
  private destroy$ = new Subject();

  constructor(private carService: CarService, public dialog: MatDialog, private authService: AuthService) {}

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild('menuTrigger') menuTrigger!: MatMenuTrigger;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit(): void {
    this.loadCars();
    this.authService.userBlockedOrDeleted$.pipe(takeUntil(this.destroy$)).subscribe(() => this.loadCars());
  }

  openDeleteDialog(car: Car) {
    const dialogRef = this.dialog.open(DeleteCarAdminComponent, {
      data: car,
      restoreFocus: false
    });
    dialogRef.componentInstance.carDeleted.pipe(takeUntil(this.destroy$)).subscribe(() => {
      dialogRef.close();
      this.loadCars();
    });
  }

  openEditDialog(car: Car) {
    const dialogRef = this.dialog.open(EditCarDialogComponent, {
      data: car,
      restoreFocus: false
    });
    dialogRef.componentInstance.carUpdated.pipe(takeUntil(this.destroy$)).subscribe((editedCar) => {
      dialogRef.close();
      const index = this.carsList.findIndex((r) => r.id === editedCar.id);
      if (index !== -1) {
        this.carsList[index] = editedCar;
        this.dataSource.data = this.carsList;
      }
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  private loadCars() {
    this.carService.getAllCars().pipe(takeUntil(this.destroy$)).subscribe(cars => {
      this.carsList = cars.sort((a, b) => a.id! - b.id!);
      this.dataSource.data = this.carsList;
    });
  }
}
