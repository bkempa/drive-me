import {Component, EventEmitter, Inject, Output} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {Car} from "../../../car/car";
import {catchError, throwError} from "rxjs";
import {MatSnackBar} from "@angular/material/snack-bar";
import {CarService} from "../../../car/service/car.service";

@Component({
  selector: 'app-delete-car-admin',
  templateUrl: './delete-car-admin.component.html',
  styleUrls: ['./delete-car-admin.component.css']
})
export class DeleteCarAdminComponent {

  @Output() carDeleted = new EventEmitter<void>();

  constructor(@Inject(MAT_DIALOG_DATA) public car: Car, private carService: CarService, private _snackBar: MatSnackBar) {
  }

  deleteCar() {
    this.carService.deleteCar(this.car)
      .pipe(
        catchError((error) => {
          this._snackBar.open('Wystąpił błąd przy usuwaniu samochodu. Spróbuj ponownie', "Zamknij", {duration: 3000});
          return throwError(error);
        }),
      )
      .subscribe(() => {
        this._snackBar.open("Usunięto samochód!", "Zamknij", {duration: 3000});
        this.carDeleted.emit();
      });
  }
}
