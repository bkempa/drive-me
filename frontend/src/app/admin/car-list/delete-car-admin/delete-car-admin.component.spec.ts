import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteCarAdminComponent } from './delete-car-admin.component';

describe('DeleteCarAdminComponent', () => {
  let component: DeleteCarAdminComponent;
  let fixture: ComponentFixture<DeleteCarAdminComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DeleteCarAdminComponent]
    });
    fixture = TestBed.createComponent(DeleteCarAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
