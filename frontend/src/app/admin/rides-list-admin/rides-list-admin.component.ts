import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {MatDialog} from "@angular/material/dialog";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatMenuTrigger} from "@angular/material/menu";
import {Ride} from "../../rides/ride";
import {RidesService} from "../../rides/service/rides.service";
import {
  EditRideDialogComponent
} from "../../user/user-details/my-rides-view/edit-ride-dialog/edit-ride-dialog.component";
import {GeoService} from "../../services/geo/geo.service";
import {forkJoin, map, Subject, takeUntil} from "rxjs";
import {UserService} from "../../user/service/user.service";
import {MediaMatcher} from "@angular/cdk/layout";
import {
  DeleteRideDialogComponent
} from "../../user/user-details/my-rides-view/delete-ride-dialog/delete-ride-dialog.component";
import {AuthService} from "../../login/service/auth.service";

@Component({
  selector: 'app-rides-list-admin',
  templateUrl: './rides-list-admin.component.html',
  styleUrls: ['./rides-list-admin.component.css']
})
export class RidesListAdminComponent implements OnInit, OnDestroy, AfterViewInit {
  ridesList!: Ride[];
  dataSource = new MatTableDataSource<Ride>(this.ridesList);
  displayedColumns: string[] = ['id', 'startLocation', 'endLocation', 'startingTime', 'driver', 'car', 'price', 'placesAvailable', 'description', 'currentReservations', 'action'];
  dialogWidth: string = '30%';
  isLoadingAddresses: boolean = true;
  private destroy$ = new Subject();

  constructor(private ridesService: RidesService, public dialog: MatDialog, private geoService: GeoService,
              private userService: UserService, private mediaMatcher: MediaMatcher, private authService: AuthService) {}

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild('menuTrigger') menuTrigger!: MatMenuTrigger;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit(): void {
    this.loadRides();
    this.authService.userBlockedOrDeleted$.pipe(takeUntil(this.destroy$)).subscribe(() => this.loadRides());
  }

  openDeleteDialog(ride: Ride) {
    const dialogRef = this.dialog.open(DeleteRideDialogComponent, {
      data: ride,
      restoreFocus: false
    });

    dialogRef.componentInstance.rideDeleted.pipe(takeUntil(this.destroy$)).subscribe(() => {
      dialogRef.close();
      const index = this.ridesList.findIndex((r) => r.id === ride.id);
      if (index !== -1) {
        this.ridesList.splice(index, 1);
        this.dataSource.data = this.ridesList;
      }
    });
  }

  openEditDialog(ride: Ride) {
    this.setDialogWidth();
    const dialogRef = this.dialog.open(EditRideDialogComponent, {
      data: ride,
      restoreFocus: false,
      width: this.dialogWidth
    });
    dialogRef.componentInstance.rideEdited
      .pipe(takeUntil(this.destroy$))
      .subscribe((editedRide) => {
        dialogRef.close();
        const index = this.ridesList.findIndex((r) => r.id === editedRide.id);
        if (index !== -1) {
          this.fillAddressFromCoords(editedRide);
          this.ridesList[index] = editedRide;
          this.dataSource.data = this.ridesList;
        }
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  setDialogWidth() {
    if (this.mediaMatcher.matchMedia('(max-width: 600px)').matches)
      this.dialogWidth = '90%';
    else
      this.dialogWidth = '25%';
  }


  ngOnDestroy() {
    this.destroy$.complete();
  }

  private loadRides() {
    // this.ridesService.getAllRides().pipe(takeUntil(this.destroy$)).subscribe(rides => {
    //   this.ridesList = rides.sort((a, b) => a.id! - b.id!);
    //
    //   for (const ride of this.ridesList) {
    //     this.fillAddressFromCoords(ride);
    //
    //     if (ride.reservations) {
    //       for (const reservation of ride.reservations) {
    //         this.userService.getUserById(reservation.id.passengerId).pipe(takeUntil(this.destroy$)).subscribe(passenger => {
    //           reservation.passengerUsername = passenger.username;
    //           this.dataSource.data = this.ridesList;
    //         })
    //       }
    //     }
    //   }
    // });
    this.isLoadingAddresses = true;
    this.ridesService.getAllRides().pipe(takeUntil(this.destroy$)).subscribe(rides => {
      // Store the data in an array
      const rideObservables = rides.map(ride => {
        return this.fillAddressFromCoords(ride);
      });

      forkJoin(rideObservables).subscribe(() => {
        // After all data is loaded, update the dataSource
        this.ridesList = rides.sort((a, b) => a.id! - b.id!);
        this.dataSource.data = this.ridesList;
        this.isLoadingAddresses = false;
      });
    });
  }

  private fillAddressFromCoords(ride: Ride) {
    // const startLocation = ride.startLocation.coordinates;
    // const endLocation = ride.endLocation.coordinates;
    // // const startLocation = ride.startLocation;
    // // const endLocation = ride.endLocation;
    //
    // this.geoService.getAddressFromCoordinates(startLocation[0], startLocation[1]).subscribe(startAddress => {
    //   ride.startAddress = startAddress;
    //
    //   this.geoService.getAddressFromCoordinates(endLocation[0], endLocation[1]).subscribe(endAddress => {
    //     ride.endAddress = endAddress;
    //   });
    // });



    // const startLocation = ride.startLocation.coordinates;
    // const endLocation = ride.endLocation.coordinates;
    //
    // const startObservable = this.geoService.getAddressFromCoordinates(startLocation[0], startLocation[1]);
    // const endObservable = this.geoService.getAddressFromCoordinates(endLocation[0], endLocation[1]);
    //
    // return forkJoin([startObservable, endObservable]).pipe(
    //   map(([startAddress, endAddress]) => {
    //     ride.startAddress = startAddress;
    //     ride.endAddress = endAddress;
    //     return ride;
    //   })
    // );


    const startLocation = ride.startLocation.coordinates;
    const endLocation = ride.endLocation.coordinates;

    const startObservable = this.geoService.getAddressFromCoordinates(startLocation[0], startLocation[1]);
    const endObservable = this.geoService.getAddressFromCoordinates(endLocation[0], endLocation[1]);

    // Extract passengerIds from reservations
    if (ride.reservations) {
      const passengerIds = ride.reservations.map(reservation => reservation.id.passengerId);

      // Get User information for all passengers
      const passengerObservables = passengerIds.map(passengerId =>
        this.userService.getUserById(passengerId)
      );

      return forkJoin([startObservable, endObservable, ...passengerObservables]).pipe(
        map(([startAddress, endAddress, ...passengers]) => {
          ride.startAddress = startAddress;
          ride.endAddress = endAddress;

          // Update passengerUsername for each reservation
          if (ride.reservations) {
            ride.reservations.forEach((reservation, index) => {
              reservation.passengerUsername = passengers[index].username;
            });
          }

          return ride;
        })
      );
    }
    else {
      return forkJoin([startObservable, endObservable]).pipe(
        map(([startAddress, endAddress]) => {
          ride.startAddress = startAddress;
          ride.endAddress = endAddress;
          return ride;
        })
      );
    }
  }
}
