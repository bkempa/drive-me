import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RidesListAdminComponent } from './rides-list-admin.component';

describe('RidesListAdminComponent', () => {
  let component: RidesListAdminComponent;
  let fixture: ComponentFixture<RidesListAdminComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RidesListAdminComponent]
    });
    fixture = TestBed.createComponent(RidesListAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
