import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ArchivedRide} from "../../../rides/archive/archived-ride";
import {MatTableDataSource} from "@angular/material/table";
import {Subject, takeUntil} from "rxjs";
import {MatDialog} from "@angular/material/dialog";
import {GeoService} from "../../../services/geo/geo.service";
import {ArchivedRideService} from "../../../rides/archive/service/archived-ride.service";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatMenuTrigger} from "@angular/material/menu";

@Component({
  selector: 'app-rides-archive',
  templateUrl: './rides-archive.component.html',
  styleUrls: ['./rides-archive.component.css']
})
export class RidesArchiveComponent implements OnInit, OnDestroy, AfterViewInit {

  ridesList!: ArchivedRide[];
  dataSource = new MatTableDataSource<ArchivedRide>(this.ridesList);
  displayedColumns: string[] = ['id', 'startLocation', 'endLocation', 'startingTime', 'driver', 'car', 'price', 'passengers', 'description'];
  private destroy$ = new Subject();
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild('menuTrigger') menuTrigger!: MatMenuTrigger;

  constructor(private ridesService: ArchivedRideService, public dialog: MatDialog, private geoService: GeoService) {}

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit(): void {
    this.ridesService.getAllArchivedRides()
      .pipe(takeUntil(this.destroy$))
      .subscribe(rides => {
        this.ridesList = rides;
        for (const ride of this.ridesList) {
          const startLocation = ride.startLocation.coordinates;
          const endLocation = ride.endLocation.coordinates;

          this.geoService.getAddressFromCoordinates(startLocation[0], startLocation[1]).subscribe(startAddress => {
            ride.startAddress = startAddress;

            this.geoService.getAddressFromCoordinates(endLocation[0], endLocation[1]).subscribe(endAddress => {
              ride.endAddress = endAddress;
            });
          });
        }
        this.dataSource.data = this.ridesList;
      });
  }

  // openDeleteDialog(ride: Ride) {
  //   const dialogRef = this.dialog.open(DeleteRideDialogComponent, {data: ride, restoreFocus: false});
  //   dialogRef.afterClosed().subscribe(() => this.menuTrigger.focus());
  // }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnDestroy() {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}
