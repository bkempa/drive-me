import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RidesArchiveComponent } from './rides-archive.component';

describe('RidesArchiveComponent', () => {
  let component: RidesArchiveComponent;
  let fixture: ComponentFixture<RidesArchiveComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RidesArchiveComponent]
    });
    fixture = TestBed.createComponent(RidesArchiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
