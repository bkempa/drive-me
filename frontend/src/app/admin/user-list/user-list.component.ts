import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Role, User} from "../../user/user";
import {UserService} from "../../user/service/user.service";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatMenuTrigger} from "@angular/material/menu";
import {MatDialog} from "@angular/material/dialog";
import {BlockUserDialogComponent} from "./admin-action/block-user-dialog/block-user-dialog.component";
import {UnblockUserDialogComponent} from "./admin-action/unblock-user-dialog/unblock-user-dialog.component";
import {DeleteUserDialogComponent} from "./admin-action/delete-user-dialog/delete-user-dialog.component";
import {catchError, Subject, takeUntil, throwError} from "rxjs";
import {AuthService} from "../../login/service/auth.service";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements AfterViewInit, OnInit, OnDestroy {

  usersList!: User[];
  dataSource = new MatTableDataSource<User>(this.usersList);
  displayedColumns: string[] = ['id', 'username', 'firstName', 'lastName', 'dateOfBirth', 'phoneNumber', 'emailAddress', 'cars', 'role', 'blocked', 'rate', 'unblockDateTime', 'action'];
  private destroy$ = new Subject();
  Role = Role;
  currentUser$ = this.authService.currentUser$;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild('menuTrigger') menuTrigger!: MatMenuTrigger;

  constructor(public dialog: MatDialog, private userService: UserService, private authService: AuthService) {}

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit(): void {
    this.loadUsers();
  }

  openBlockDialog(user: User) {
    const dialogRef = this.dialog.open(BlockUserDialogComponent, {
      data: user,
      restoreFocus: false
    });
    dialogRef.componentInstance.userBlocked.pipe(takeUntil(this.destroy$)).subscribe(() => {
      dialogRef.close();
      this.authService.notifyUserBlockedOrDeleted();
      this.loadUsers();
    });
  }

  openUnblockDialog(user: User) {
    const dialogRef = this.dialog.open(UnblockUserDialogComponent, {
      data: user,
      restoreFocus: false
    });
    dialogRef.componentInstance.userUnblocked.pipe(takeUntil(this.destroy$)).subscribe(() => {
      dialogRef.close();
      this.authService.notifyUserBlockedOrDeleted();
      this.loadUsers();
    });
  }

  openDeleteDialog(user: User) {
    const dialogRef = this.dialog.open(DeleteUserDialogComponent, {
      data: user,
      restoreFocus: false
    });
    dialogRef.componentInstance.userDeleted
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        dialogRef.close();
        this.loadUsers();
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  private loadUsers() {
    this.userService.getAllUsers()
      .pipe(takeUntil(this.destroy$))
      .subscribe(users => {
        this.usersList = users.sort((a, b) => a.id! - b.id!);
        this.dataSource.data = this.usersList;
      });
  }

  grantAdminRights(user: User) {
    this.userService.grantAdminRights(user)
      .pipe(
        takeUntil(this.destroy$),
        catchError((error) => {
          return throwError(error);
        })
      )
      .subscribe(() => {
        this.loadUsers();
      });
  }

  revokeAdminRights(user: User) {
    this.userService.revokeAdminRights(user)
      .pipe(
        takeUntil(this.destroy$),
        catchError((error) => {
          return throwError(error);
        })
      )
      .subscribe(() => {
        this.loadUsers();
      });
  }

  ngOnDestroy() {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}
