import {Component, EventEmitter, Inject, Output} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {User} from "../../../../user/user";
import {MatSnackBar} from "@angular/material/snack-bar";
import {catchError, throwError} from "rxjs";
import {UserService} from "../../../../user/service/user.service";

@Component({
  selector: 'app-delete-user-dialog',
  templateUrl: './delete-user-dialog.component.html',
  styleUrls: ['./delete-user-dialog.component.css']
})
export class DeleteUserDialogComponent {

  @Output() userDeleted = new EventEmitter<void>();
  constructor(@Inject(MAT_DIALOG_DATA) public user: User, private userService: UserService, private _snackBar: MatSnackBar) {}

  deleteUser() {
    this.userService.deleteUser(this.user.id)
      .pipe(
        catchError((error) => {
          this._snackBar.open('Wystąpił błąd przy usuwaniu użytkownika. Spróbuj ponownie', "Zamknij", {duration: 3000});
          return throwError(error);
        })
      )
      .subscribe(() => {
        this._snackBar.open("Usunięto użytkownika!", "Zamknij", {duration: 3000});
        this.userDeleted.emit();
      });
  }
}
