import {Component, EventEmitter, Inject, Output} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {User} from "../../../../user/user";
import {MatSnackBar} from "@angular/material/snack-bar";
import {catchError, throwError} from "rxjs";
import {UserService} from "../../../../user/service/user.service";

@Component({
  selector: 'app-block-user-dialog',
  templateUrl: './block-user-dialog.component.html',
  styleUrls: ['./block-user-dialog.component.css']
})
export class BlockUserDialogComponent {
  daysSelected: string = '';
  @Output() userBlocked = new EventEmitter<void>();

  constructor(@Inject(MAT_DIALOG_DATA) public user: User, private userService: UserService, private _snackBar: MatSnackBar) {
  }


  blockUser() {
    if (this.daysSelected == 'forever') {
      this.userService.blockUserPermanently(this.user)
        .pipe(
          catchError((error) => {
            this._snackBar.open(`Wystąpił błąd przy blokowaniu użytkownika ${this.user.username}. Spróbuj ponownie`, "Zamknij", {duration: 3000});
            return throwError(error);
          })
        )
        .subscribe(() => {
          this._snackBar.open(`Zablokowano użytkownika ${this.user.username}!`, "Zamknij", {duration: 3000});
          this.userBlocked.emit();
        });
    } else {
      const days: number = +this.daysSelected;
      this.userService.blockUserForDaysNumber(this.user, days)
        .pipe(
          catchError((error) => {
            this._snackBar.open(`Wystąpił błąd przy blokowaniu użytkownika ${this.user.username}. Spróbuj ponownie`, "Zamknij", {duration: 3000});
            return throwError(error);
          })
        )
        .subscribe(() => {
          this._snackBar.open(`Zablokowano użytkownika ${this.user.username} na ${days} dni.`, "Zamknij", {duration: 3000});
          this.userBlocked.emit();
        });
    }
  }
}
