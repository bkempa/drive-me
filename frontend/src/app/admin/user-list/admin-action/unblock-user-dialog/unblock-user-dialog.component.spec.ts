import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnblockUserDialogComponent } from './unblock-user-dialog.component';

describe('UnblockUserDialogComponent', () => {
  let component: UnblockUserDialogComponent;
  let fixture: ComponentFixture<UnblockUserDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UnblockUserDialogComponent]
    });
    fixture = TestBed.createComponent(UnblockUserDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
