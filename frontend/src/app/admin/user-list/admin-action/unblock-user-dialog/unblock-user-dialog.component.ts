import {Component, EventEmitter, Inject, Output} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {User} from "../../../../user/user";
import {MatSnackBar} from "@angular/material/snack-bar";
import {catchError, throwError} from "rxjs";
import {UserService} from "../../../../user/service/user.service";

@Component({
  selector: 'app-unblock-user-dialog',
  templateUrl: './unblock-user-dialog.component.html',
  styleUrls: ['./unblock-user-dialog.component.css']
})
export class UnblockUserDialogComponent {
  @Output() userUnblocked = new EventEmitter<void>();

  constructor(@Inject(MAT_DIALOG_DATA) public user: User, private userService: UserService, private _snackBar: MatSnackBar) {
  }

  unblockUser() {
    this.userService.unblockUser(this.user)
      .pipe(
        catchError((error) => {
          this._snackBar.open(`Wystąpił błąd przy odblokowywaniu użytkownika ${this.user.username}. Spróbuj ponownie`, "Zamknij", {duration: 3000});
          return throwError(error);
        })
      )
      .subscribe(() => {
        this._snackBar.open(`Odblokowano użytkownika ${this.user.username}!`, "Zamknij", {duration: 3000});
        this.userUnblocked.emit();
      });
  }
}
