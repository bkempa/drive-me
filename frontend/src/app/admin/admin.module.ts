import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AdminRoutingModule} from './admin-routing.module';
import {AdminComponent} from "./admin.component";
import {UserListComponent} from "./user-list/user-list.component";
import {DeleteUserDialogComponent} from "./user-list/admin-action/delete-user-dialog/delete-user-dialog.component";
import {BlockUserDialogComponent} from "./user-list/admin-action/block-user-dialog/block-user-dialog.component";
import {UnblockUserDialogComponent} from "./user-list/admin-action/unblock-user-dialog/unblock-user-dialog.component";
import {CarListComponent} from "./car-list/car-list.component";
import {UserRatingsListComponent} from "./user-ratings-list/user-ratings-list.component";
import {RidesListAdminComponent} from "./rides-list-admin/rides-list-admin.component";
import {RidesArchiveComponent} from "./rides-list-admin/rides-archive/rides-archive.component";
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatSortModule} from "@angular/material/sort";
import {MatMenuModule} from "@angular/material/menu";
import {MatDialogModule} from "@angular/material/dialog";
import {MatSelectModule} from "@angular/material/select";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatTabsModule} from "@angular/material/tabs";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatToolbarModule} from "@angular/material/toolbar";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MeanRatingForUserPipe} from "../user/user-rating/pipes/mean-rating-for-user.pipe";
import { DeleteCarAdminComponent } from './car-list/delete-car-admin/delete-car-admin.component';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";


@NgModule({
  declarations: [
    AdminComponent,
    UserListComponent,
    DeleteUserDialogComponent,
    BlockUserDialogComponent,
    UnblockUserDialogComponent,
    CarListComponent,
    UserRatingsListComponent,
    RidesListAdminComponent,
    RidesArchiveComponent,
    MeanRatingForUserPipe,
    DeleteCarAdminComponent
  ],
    exports: [
        AdminComponent,
        MeanRatingForUserPipe
    ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    FormsModule,
    MatTabsModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatMenuModule,
    MatDialogModule,
    MatSelectModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatProgressSpinnerModule,
    // AppModule,
  ]
})
export class AdminModule {
}
