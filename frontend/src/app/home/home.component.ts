import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";
import {GeoService} from "../services/geo/geo.service";
import {Address} from "../services/geo/address";
import {switchMap, take} from "rxjs";
import {Router} from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent {

  now: Date = new Date();
  searchControl: FormGroup;
  distanceOptions: string[] = ['250 m', '500 m', '1 km'];

  constructor(private formBuilder: FormBuilder, private _snackBar: MatSnackBar, private geoService: GeoService,
              private router: Router) {
    this.searchControl = this.formBuilder.group({
      address: ['', Validators.required],
      destination: ['', Validators.required],
      date: [this.now, Validators.required],
      distance: [this.distanceOptions.at(0), Validators.required]
    });
  }

  findMyLocation() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          const lat = position.coords.latitude;
          const lng = position.coords.longitude;
          this.geoService
            .getAddressFromCoordinates(lng, lat)
            .pipe(
              take(1),
              switchMap((address) => {
                this.fillSearchFieldWithMyLocationAddress(address);
                return [];
              })
            )
            .subscribe({
              next: (address) => {
                this.fillSearchFieldWithMyLocationAddress(address);
              },
              error: () => {
                this._snackBar.open(`Błąd przy pobieraniu lokalizacji`, "Zamknij", {duration: 3000});
              },
            });
        },
        (error) => {
          this._snackBar.open(`Błąd przy pobieraniu lokalizacji: ${error}`, "Zamknij", {duration: 3000});
        }
      );
    } else
      this._snackBar.open("Twoja przeglądarka nie wspiera usługi lokalizacji.", "Zamknij", {duration: 3000});
  }

  fillSearchFieldWithMyLocationAddress(address: Address) {
    this.searchControl.patchValue({
      address: `${address.streetName} ${address.homeNumber}, ${address.city}`
    })
  }

  searchRides() {
    const {address, destination, date, distance} = this.searchControl.value;
    let distanceInMeters: number = 250;
    switch (distance) {
      case '250 m':
        distanceInMeters = 250;
        break;
      case '500 m':
        distanceInMeters = 500;
        break;
      case '1 km':
        distanceInMeters = 1000;
        break;
    }
    this.router.navigate(['/rides'], {
      queryParams: {
        address,
        destination,
        date: date.toISOString(),
        distanceInMeters,
      },
    });
  }

  navigateToRegistrationTab() {
    this.router.navigate(['/login', 1]);
  }
}
