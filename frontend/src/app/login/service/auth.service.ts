import {Injectable} from '@angular/core';
import {AuthenticationRequest} from "../auth-model/authentication-request";
import {
  BehaviorSubject,
  catchError,
  Observable, Subject,
  switchMap,
  tap,
  throwError
} from "rxjs";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {User} from "../../user/user";
import {UserService} from "../../user/service/user.service";
import jwt_decode from 'jwt-decode';
import {AuthenticationResponse} from "../auth-model/authentication-response";
import {RegisterRequest} from "../auth-model/register-request";
import {map} from "rxjs/operators";
import {environment} from "../../../environment/environment";


interface DecodedToken {
  sub: string;
  exp: number;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private currentUserSubject: BehaviorSubject<User | null> = new BehaviorSubject<User | null>(null);
  currentUser$: Observable<User | null> = this.currentUserSubject.asObservable();
  private rideCreatedSubject = new Subject<void>();
  rideCreated$: Observable<void> = this.rideCreatedSubject.asObservable();
  private userBlockedOrDeletedSubject = new Subject<void>();
  userBlockedOrDeleted$: Observable<void> = this.userBlockedOrDeletedSubject.asObservable();
  private ratingCreatedOrEditedSubject = new Subject<void>();
  ratingCreatedOrEdited$: Observable<void> = this.ratingCreatedOrEditedSubject.asObservable();

  constructor(private http: HttpClient, private userService: UserService) {}

  private updateCurrentUser(user: User | null) {
    this.currentUserSubject.next(user);
  }

  register(user: RegisterRequest): Observable<User> {
    return this.http.post<AuthenticationResponse>(`${environment.apiUrl}/user/auth/register`, user).pipe(
      catchError(this.handleError),
      switchMap((res: AuthenticationResponse) => {
        localStorage.setItem('access_token', res.token);
        return this.getUser(user.username).pipe(
          tap(user => {
            this.updateCurrentUser(user);
          })
        );
      })
    );
  }

  login(request: AuthenticationRequest): Observable<User> {
    return this.http.post<AuthenticationResponse>(`${environment.apiUrl}/user/auth/authenticate`, request).pipe(
      catchError(this.handleError),
      switchMap((res: AuthenticationResponse) => {
        localStorage.setItem('access_token', res.token);

        const decoded = AuthService.decodeToken(res.token);
        const username = decoded.sub;

        return this.getUser(username).pipe(
          tap(user => {
            this.updateCurrentUser(user);
          })
        );
      })
    );
  }

  logout() {
    localStorage.removeItem('access_token');
    this.updateCurrentUser(null);
  }

  private getUser(username: string): Observable<User> {
    return this.userService.getUserByUsername(username).pipe(
      catchError(this.handleError)
    );
  }

  static decodeToken(token: string): DecodedToken {
    return jwt_decode(token) as DecodedToken;
  }

  private handleError(error: HttpErrorResponse): Observable<never> {
    let msg = '';
    if (error.error instanceof ErrorEvent)
      msg = error.error.message;
    else if ((error.status === 401 || error.status === 400) && error.error && error.error.message)
      msg = error.error.message;
    else
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;

    return throwError(() => msg);
  }

  static isTokenExpired(token: DecodedToken): boolean {
    return token.exp < (Date.now() / 1000);
  }

  getCurrentUserId(): Observable<number | null> {
    return this.currentUser$.pipe(
      map(user => user ? user.id : null)
    );
  }

  notifyRideCreated() {
    this.rideCreatedSubject.next();
  }

  notifyUserBlockedOrDeleted() {
    this.userBlockedOrDeletedSubject.next();
  }

  notifyRatingCreatedOrEdited() {
    this.ratingCreatedOrEditedSubject.next();
  }
}
