import {Role} from "../../user/user";

export interface RegisterRequest {
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  dateOfBirth: Date;
  emailAddress: string;
  phoneNumber: string;
  role: Role;
}
