import { Component } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {AuthService} from "../service/auth.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {AuthenticationRequest} from "../auth-model/authentication-request";
import {catchError, EMPTY, tap} from "rxjs";

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent {
  loginFormGroup: FormGroup;
  hidePassword = true;

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router, private _snackBar: MatSnackBar) {
    this.loginFormGroup = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  login() {
    let request = new AuthenticationRequest(this.loginFormGroup.get('username')!.value, this.loginFormGroup.get('password')!.value);
    this.authService.login(request)
      .pipe(
        tap(() => {
          this.loginFormGroup.reset();
          this.router.navigateByUrl('/');
          this._snackBar.open("Logowanie przebiegło pomyślnie. W drogę!", "Zamknij", {duration: 3000});
        }),
        catchError((error) => {
          if (error.includes('is blocked')) {
            if (error.includes('permanently'))
              this._snackBar.open("Twoje konto zostało zablokowane na stałe.", "Zamknij", {duration: 5000});
            else {
              const indexOfLastSpace = error.lastIndexOf(" ");
              const unblockDateTime = new Date(error.substring(indexOfLastSpace + 1));
              this._snackBar.open("Twoje konto zostało zablokowane. Data odblokowania - " + unblockDateTime.toLocaleString(), "Zamknij", {duration: 5000});
            }
          } else
            this._snackBar.open(`Niepoprawne dane użytkownika`, "Zamknij", {duration: 3000});

          return EMPTY;
        })
      )
      .subscribe();
  }

  togglePasswordVisibility() {
    this.hidePassword = !this.hidePassword;
  }
}
