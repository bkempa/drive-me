import { Component } from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {AuthService} from "../service/auth.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {catchError, EMPTY, tap} from "rxjs";
import {Role} from "../../user/user";
import {RegisterRequest} from "../auth-model/register-request";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  registrationFormGroup: FormGroup;
  hideNewPassword = true;
  hideRepeatPassword = true;
  minDate: Date = new Date(1900, 1, 1);
  maxDate: Date = new Date();

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router, private _snackBar: MatSnackBar) {
    this.registrationFormGroup = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      birthDate: ['', Validators.required],
      phoneNumber: ['', [Validators.required, Validators.pattern(/^[\d\s+()-]+$/)]],
      newUsername: ['', Validators.required],
      newPassword: ['', [Validators.required, Validators.pattern(/^(?=.*\d)(?=.*[!@#$%^&*]+)(?=.*[A-Z])(?=.*[a-z]).{8,}$/)]],
      repeatPassword: new FormControl('', [Validators.required])
    });

    this.registrationFormGroup.get('repeatPassword')!.setValidators(this.passwordMatchValidator);
  }

  register() {
    const user = {
      username: this.registrationFormGroup.get('newUsername')!.value,
      password: this.registrationFormGroup.get('newPassword')!.value,
      firstName: this.registrationFormGroup.get('firstName')!.value,
      lastName: this.registrationFormGroup.get('lastName')!.value,
      dateOfBirth: new Date(this.registrationFormGroup.get('birthDate')!.value),
      phoneNumber: this.registrationFormGroup.get('phoneNumber')!.value,
      emailAddress: this.registrationFormGroup.get('email')!.value,
      role: Role.USER
    } as RegisterRequest;
    this.authService.register(user)
      .pipe(
        tap(() => {
          this.registrationFormGroup.reset();
          this.router.navigateByUrl('/');
          this._snackBar.open("Rejestracja przebiegła pomyślnie. Ruszajmy!", "Zamknij", {duration: 3000});
        }),
        catchError((error) => {
            if (error == 'This username is not available')
              this._snackBar.open("Nazwa użytkownika jest już zajęta.", "Zamknij", {duration: 5000});
            else if (error == 'An account for this e-mail already exists')
              this._snackBar.open("Konto zarejestrowane na podany adres e-mail już istnieje.", "Zamknij", {duration: 5000});
            else
              this._snackBar.open(`Niepoprawne dane użytkownika`, "Zamknij", {duration: 3000});
            return EMPTY;
          }
        )
      )
      .subscribe();
  }

  passwordMatchValidator(control: AbstractControl) {
    const password = control.root.get('newPassword');
    const repeatPassword = control.value;

    if (password && repeatPassword && password.value !== repeatPassword) {
      return { passwordMismatch: true };
    }

    return null;
  }

  toggleNewPasswordVisibility() {
    this.hideNewPassword = !this.hideNewPassword;
  }

  toggleRepeatPasswordVisibility() {
    this.hideRepeatPassword = !this.hideRepeatPassword;
  }
}
