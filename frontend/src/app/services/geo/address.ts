export interface Address {
  streetName: string;
  homeNumber: string;
  city: string;
}
