import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {map, Observable, of} from "rxjs";
import {Address} from "./address";
import {Point} from "../../rides/ride";
import {GeocodingResponse} from "./geocoding-response";

@Injectable({
  providedIn: 'root'
})
export class GeoService {

  private apiUrl: string = "https://nominatim.openstreetmap.org";

  constructor(private http: HttpClient) {}

  getAddressFromCoordinates(lon: number, lat: number): Observable<Address> {
    const params: HttpParams = new HttpParams()
      .set('lat', lat.toString())
      .set('lon', lon.toString())
      .set('format', 'json');
    return this.http.get<GeocodingResponse>(`${this.apiUrl}/reverse`, { params }).pipe(
      map((geocodingResponse: GeocodingResponse) => {
        const address: Address = {
          streetName: geocodingResponse.address?.road || '',
          homeNumber: geocodingResponse.address?.house_number || '',
          city: geocodingResponse.address?.city || geocodingResponse.address?.town || geocodingResponse.address?.village || ''
        };
        return address;
      })
    );
  }

  getCoordinatesFromAddress(address: string | null): Observable<Point | null> {
    if (address !== null) {
      const params: HttpParams = new HttpParams()
        .set('q', address)
        .set('format', 'json');
      return this.http.get<GeocodingResponse[]>(`${this.apiUrl}/search`, { params }).pipe(
        map((geocodingResponse: GeocodingResponse[]) => {
          if (geocodingResponse.length > 0) {
            const response: GeocodingResponse = geocodingResponse.at(0)!;
            const point: Point = {
              type: "point",
              coordinates: [parseFloat(response.lon), parseFloat(response.lat)]
            };
            return point;
          } else
            return null;
        })
      );
    } else
      return of(null);
  }
}
