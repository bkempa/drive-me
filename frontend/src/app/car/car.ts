export interface Car {
  id?: number;
  make: string;
  model: string;
  color: string;
  plateNumber: string;
  productionYear: number;
  maxPassengers: number;
}
