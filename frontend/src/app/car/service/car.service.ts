import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Car} from "../car";
import {environment} from "../../../environment/environment";

@Injectable({
  providedIn: 'root'
})
export class CarService {

  constructor(private http: HttpClient) {}

  getAllCars() {
    return this.http.get<Car[]>(`${environment.apiUrl}/cars/all`);
  }

  getUserCars(userId: number) {
    return this.http.get<Car[]>(`${environment.apiUrl}/cars/owner/${userId}`);
  }

  deleteCar(car: Car) {
    return this.http.delete(`${environment.apiUrl}/cars/${car.id}`);
  }

  createCar(newCar: Car, userId: number) {
    let params = new HttpParams().set('userId', userId);
    return this.http.post<Car>(`${environment.apiUrl}/cars`, newCar, {params});
  }

  deleteCarFromUser(car: Car, userId: number) {
    let params: HttpParams = new HttpParams().set('userId', userId);
    return this.http.patch(`${environment.apiUrl}/cars/${car.id}`, car, {params});
  }
}
