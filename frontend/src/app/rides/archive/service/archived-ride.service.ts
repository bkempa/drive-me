import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ArchivedRide} from "../archived-ride";
import {environment} from "../../../../environment/environment";

@Injectable({
  providedIn: 'root'
})
export class ArchivedRideService {

  constructor(private http: HttpClient) {}

  getAllArchivedRides() {
    return this.http.get<ArchivedRide[]>(`${environment.apiUrl}/rides/archive/all`);
  }

  getArchivedRidesByPassengerId(passengerId: number) {
    return this.http.get<ArchivedRide[]>(`${environment.apiUrl}/rides/archive/passenger/${passengerId}`);
  }

  getArchivedRidesByDriverId(driverId: number) {
    return this.http.get<ArchivedRide[]>(`${environment.apiUrl}/rides/archive/driver/${driverId}`);
  }
}
