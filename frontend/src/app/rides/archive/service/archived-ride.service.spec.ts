import { TestBed } from '@angular/core/testing';

import { ArchivedRideService } from './archived-ride.service';

describe('ArchivedRideService', () => {
  let service: ArchivedRideService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ArchivedRideService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
