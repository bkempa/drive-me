import {User} from "../../user/user";
import {Car} from "../../car/car";
import {Point} from "../ride";
import {Address} from "../../services/geo/address";

export interface ArchivedRide {
  id: number;
  startingTime: string;
  car?: Car;
  driver?: User;
  passengers: User[];
  price: number;
  startLocation: Point;
  endLocation: Point;
  description: string;
  startAddress?: Address;
  endAddress?: Address;
}
