import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {RidesService} from "../service/rides.service";
import {ActivatedRoute, Router} from "@angular/router";
import {GeoService} from "../../services/geo/geo.service";
import {filter, forkJoin, map, Observable, of, Subject, switchMap, takeUntil} from "rxjs";
import {Point, Ride} from "../ride";
import {MatDialog} from "@angular/material/dialog";
import {MatMenuTrigger} from "@angular/material/menu";
import {RideReservationComponent} from "../ride-reservation/ride-reservation.component";
import {RideReservationDialogInfo} from "../ride-reservation/ride-reservation";
import {SearchParameters} from "./search-parameters";
import {RefreshSearchingDialogComponent} from "./refresh-searching-dialog/refresh-searching-dialog.component";

@Component({
  selector: 'app-rides-list',
  templateUrl: './rides-list.component.html',
  styleUrls: ['./rides-list.component.css']
})
export class RidesListComponent implements OnInit, OnDestroy {

  commonRides$: Observable<Ride[]> = new Observable<Ride[]>();
  noRidesFound: boolean = false;
  startCoordinates$ = new Observable<Point | null>();
  endCoordinates$ = new Observable<Point | null>();
  @ViewChild('menuTrigger') menuTrigger!: MatMenuTrigger;
  private distanceInMeters: number = 0;
  private userStartAddress = '';
  private userDestinationAddress = '';
  private chosenDate: Date = new Date();
  private destroy$ = new Subject();

  constructor(private ridesService: RidesService, private route: ActivatedRoute, private geoService: GeoService,
              public dialog: MatDialog, private router: Router) {}

  ngOnInit(): void {
    this.noRidesFound = false;
    this.commonRides$ = this.route.queryParams.pipe(
      takeUntil(this.destroy$),
      switchMap((params) => {
        const {address, destination, date, distanceInMeters} = params;
        this.chosenDate = new Date(date);
        this.distanceInMeters = distanceInMeters;
        this.userStartAddress = address;
        this.userDestinationAddress = destination;
        this.startCoordinates$ = this.geoService.getCoordinatesFromAddress(address);
        this.endCoordinates$ = this.geoService.getCoordinatesFromAddress(destination);
        return this.findRides();
      })
    );
  }

  changeSearching() {
    const inputData = {
      startAddress: this.userStartAddress,
      destinationAddress: this.userDestinationAddress,
      startingDateTime: this.chosenDate,
      searchRadiusInMeters: this.distanceInMeters,
    } as SearchParameters;
    const dialogRef = this.dialog.open(RefreshSearchingDialogComponent, {
      data: inputData,
      restoreFocus: false
    });

    dialogRef.afterClosed().subscribe((result: SearchParameters) => {
      if (result) {
        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: {
            address: result.startAddress,
            destination: result.destinationAddress,
            date: result.startingDateTime.toISOString(),
            distanceInMeters: result.searchRadiusInMeters,
          },
          queryParamsHandling: 'merge',
        });
        this.ngOnInit();
      }
    });
  }

  rideReservationDialog(ride: Ride) {
    this.startCoordinates$
      .pipe(takeUntil(this.destroy$))
      .subscribe(startCoords => {
        this.endCoordinates$
          .pipe(takeUntil(this.destroy$))
          .subscribe(endCoords => {
            const inputData = {
              ride: ride,
              userStartPosition: startCoords,
              userEndPosition: endCoords,
              radiusInMeters: this.distanceInMeters,
              userStartAddress: this.userStartAddress,
              userEndAddress: this.userDestinationAddress,
            } as RideReservationDialogInfo;
            const dialogRef = this.dialog.open(RideReservationComponent, {
              data: inputData,
              restoreFocus: false
            });
            dialogRef.componentInstance.refreshList
              .pipe(takeUntil(this.destroy$))
              .subscribe(() => this.ngOnInit());
          });
      });
  }

  ngOnDestroy() {
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  private findRides() {
    return forkJoin([
      this.startCoordinates$,
      this.endCoordinates$
    ]).pipe(
      takeUntil(this.destroy$),
      map(([startCoords, endCoords]) => ({startCoords, endCoords})),
      filter(({startCoords, endCoords}) => startCoords !== null && endCoords !== null),
      switchMap(({startCoords, endCoords}) => {
        return this.ridesService.getRidesByStartAndEndLocation(
          startCoords!.coordinates.at(0),
          startCoords!.coordinates.at(1),
          endCoords!.coordinates.at(0),
          endCoords!.coordinates.at(1),
          this.distanceInMeters.toString()
        );
      }),
      map((rides) => {
        return rides.filter((ride) => {
          const plusOneDay: Date = new Date(this.chosenDate.valueOf() + 24 * 60 * 60 * 1000);
          return new Date(ride.startingTime) >= this.chosenDate && new Date(ride.startingTime) <= plusOneDay && ride.placesAvailable > 0;
        }).sort((a, b) => new Date(a.startingTime).getTime() - new Date(b.startingTime).getTime());
      }),
      switchMap((filteredRides) => {
        if (filteredRides.length === 0)
          return of([]);

        const getAddressObservables = filteredRides.map((ride) => {
          const {startLocation, endLocation} = ride;
          const startLon = startLocation.coordinates.at(0);
          const startLat = startLocation.coordinates.at(1);
          const endLon = endLocation.coordinates.at(0);
          const endLat = endLocation.coordinates.at(1);

          if (startLon === undefined || startLat === undefined || endLon === undefined || endLat === undefined)
            return of({...ride, startAddress: undefined, endAddress: undefined});

          const startAddress$ = this.geoService.getAddressFromCoordinates(startLon, startLat);
          const endAddress$ = this.geoService.getAddressFromCoordinates(endLon, endLat);

          return forkJoin([startAddress$, endAddress$]).pipe(
            map(([startAddress, endAddress]) => ({
              ...ride,
              startAddress: startAddress,
              endAddress: endAddress
            }))
          );
        });
        return forkJoin(getAddressObservables);
      })
    );
  }
}
