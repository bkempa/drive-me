export interface SearchParameters {
  startAddress: string;
  destinationAddress: string;
  startingDateTime: Date;
  searchRadiusInMeters: number;
}
