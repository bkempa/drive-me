import {Component, Inject} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {switchMap, take} from "rxjs";
import {Address} from "../../../services/geo/address";
import {MatSnackBar} from "@angular/material/snack-bar";
import {GeoService} from "../../../services/geo/geo.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {SearchParameters} from "../search-parameters";

@Component({
  selector: 'app-refresh-searching-dialog',
  templateUrl: './refresh-searching-dialog.component.html',
  styleUrls: ['./refresh-searching-dialog.component.css']
})
export class RefreshSearchingDialogComponent {

  now: Date = new Date();
  searchControl: FormGroup;
  distanceOptions: string[] = ['250 m', '500 m', '1 km'];

  constructor(@Inject(MAT_DIALOG_DATA) public searchParams: SearchParameters, private formBuilder: FormBuilder,
              private _snackBar: MatSnackBar, private geoService: GeoService,
              private dialogRef: MatDialogRef<RefreshSearchingDialogComponent>) {
    let chosenDistanceIndex;
    switch (searchParams.searchRadiusInMeters) {
      case 250:
        chosenDistanceIndex = 0;
        break;
      case 500:
        chosenDistanceIndex = 1;
        break;
      case 1000:
        chosenDistanceIndex = 2;
        break;
      default:
        chosenDistanceIndex = 0;
        break;
    }

    if (searchParams.startingDateTime < new Date())
      searchParams.startingDateTime = new Date();

    this.searchControl = this.formBuilder.group({
      address: [searchParams.startAddress, Validators.required],
      destination: [searchParams.destinationAddress, Validators.required],
      date: [searchParams.startingDateTime, Validators.required],
      distance: [this.distanceOptions.at(chosenDistanceIndex), Validators.required]
    });
  }

  findMyLocation() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          const lat = position.coords.latitude;
          const lng = position.coords.longitude;
          this.geoService
            .getAddressFromCoordinates(lng, lat)
            .pipe(
              take(1),
              switchMap((address) => {
                this.fillSearchFieldWithMyLocationAddress(address);
                return [];
              })
            )
            .subscribe({
              next: (address) => {
                this.fillSearchFieldWithMyLocationAddress(address);
              },
              error: () => {
                this._snackBar.open(`Błąd przy pobieraniu lokalizacji`, "Zamknij", {duration: 3000});
              },
            });
        },
        (error) => {
          this._snackBar.open(`Błąd przy pobieraniu lokalizacji: ${error}`, "Zamknij", {duration: 3000});
        }
      );
    } else
      this._snackBar.open("Twoja przeglądarka nie wspiera usługi lokalizacji.", "Zamknij", {duration: 3000});
  }

  fillSearchFieldWithMyLocationAddress(address: Address) {
    this.searchControl.patchValue({
      address: `${address.streetName} ${address.homeNumber}, ${address.city}`
    })
  }

  searchRides() {
    const {address, destination, date, distance} = this.searchControl.value;
    let distanceInMeters: number = 250;
    switch (distance) {
      case '250 m':
        distanceInMeters = 250;
        break;
      case '500 m':
        distanceInMeters = 500;
        break;
      case '1 km':
        distanceInMeters = 1000;
        break;
    }

    const updatedParams = {
      startAddress: address,
      destinationAddress: destination,
      startingDateTime: date,
      searchRadiusInMeters: distanceInMeters
    } as SearchParameters;

    this.dialogRef.close(updatedParams);
  }
}
