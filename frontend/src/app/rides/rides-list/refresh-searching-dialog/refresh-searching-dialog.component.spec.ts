import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RefreshSearchingDialogComponent } from './refresh-searching-dialog.component';

describe('RefreshSearchingDialogComponent', () => {
  let component: RefreshSearchingDialogComponent;
  let fixture: ComponentFixture<RefreshSearchingDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RefreshSearchingDialogComponent]
    });
    fixture = TestBed.createComponent(RefreshSearchingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
