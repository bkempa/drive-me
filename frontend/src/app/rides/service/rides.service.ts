import {Injectable} from '@angular/core';
import {Ride} from "../ride";
import {HttpClient, HttpParams} from "@angular/common/http";
import {of} from "rxjs";
import {environment} from "../../../environment/environment";
import {RideReservation} from "../ride-reservation/ride-reservation";

@Injectable({
  providedIn: 'root'
})
export class RidesService {

  constructor(private http: HttpClient) {
  }

  getAllRides() {
    return this.http.get<Ride[]>(`${environment.apiUrl}/rides/all`);
  }

  getRidesByStartAndEndLocation(startLon: number | undefined, startLat: number | undefined, endLon: number | undefined,
                                endLat: number | undefined, radiusInMeters: string | null) {
    if (startLon && startLat && endLat && endLon && radiusInMeters) {
      let queryParams = new HttpParams()
        .append("start-lat", startLat)
        .append("start-lon", startLon)
        .append("end-lat", endLat)
        .append("end-lon", endLon)
        .append("dist", parseInt(radiusInMeters));
      return this.http.get<Ride[]>(`${environment.apiUrl}/rides/start-end`, {params: queryParams});
    } else
      return of([]);
  }

  getRidesByDriverId(driverId: number) {
    return this.http.get<Ride[]>(`${environment.apiUrl}/rides/driver/${driverId}`);
  }

  getRidesByPassengerId(passengerId: number) {
    return this.http.get<Ride[]>(`${environment.apiUrl}/rides/passenger/${passengerId}`);
  }

  createNewRide(newRide: Ride) {
    return this.http.post(`${environment.apiUrl}/rides`, newRide);
  }

  reserveRide(reservation: RideReservation) {
    return this.http.post(`${environment.apiUrl}/rides/reservations`, reservation);
  }

  removeReservation(rideId: number, passengerId: number) {
    let httpParams = new HttpParams().set('passengerId', passengerId);
    return this.http.delete(`${environment.apiUrl}/rides/reservations/${rideId}`, {params: httpParams});
  }

  editRide(ride: Ride) {
    return this.http.put(`${environment.apiUrl}/rides/${ride.id!}`, ride);
  }

  deleteRide(rideId: number) {
    return this.http.delete(`${environment.apiUrl}/rides/${rideId}`);
  }
}
