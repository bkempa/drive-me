import {Car} from "../car/car";
import {User} from "../user/user";
import {RideReservation} from "./ride-reservation/ride-reservation";
import {Address} from "../services/geo/address";

export interface Ride {
  id?: number;
  placesAvailable: number;
  startingTime: string;
  car: Car;
  driver: User;
  price: number;
  startLocation: Point;
  endLocation: Point;
  description: string;
  reservations?: RideReservation[];
  startAddress?: Address;
  endAddress?: Address;
}

export interface Point {
  type: string;
  coordinates: number[];
}
