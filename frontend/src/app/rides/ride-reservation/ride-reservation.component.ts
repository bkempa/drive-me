import {Component, EventEmitter, Inject, OnDestroy, Output} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import * as Leaflet from 'leaflet';
import {RideReservation, RideReservationDialogInfo, RideReservationKey} from "./ride-reservation";
import {AuthService} from "../../login/service/auth.service";
import {map, shareReplay} from "rxjs/operators";
import {catchError, Subject, takeUntil, throwError} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {MatSnackBar} from "@angular/material/snack-bar";
import {RidesService} from "../service/rides.service";

Leaflet.Icon.Default.imagePath = 'assets/';

@Component({
  selector: 'app-ride-reservation',
  templateUrl: './ride-reservation.component.html',
  styleUrls: ['./ride-reservation.component.css']
})
export class RideReservationComponent implements OnDestroy {
  private destroy$ = new Subject();

  @Output() refreshList = new EventEmitter<void>();

  constructor(@Inject(MAT_DIALOG_DATA) public data: RideReservationDialogInfo, private authService: AuthService,
              private http: HttpClient, private _snackBar: MatSnackBar, private rideService: RidesService) {}

  isAuthenticated$ = this.authService.currentUser$.pipe(
    map(user => !!user),
    shareReplay(1)
  );
  driverAge = new Date().getFullYear() - new Date(this.data.ride.driver.dateOfBirth).getFullYear();
  selectedValue: number = 1;
  map!: Leaflet.Map;
  markers: Leaflet.Marker[] = [];
  options = {
    layers: [
      Leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
      })
    ],
    zoom: 16,
    center: {
      lat: (this.data.ride.startLocation.coordinates.at(1)! + this.data.ride.endLocation.coordinates.at(1)!) / 2,
      lng: (this.data.ride.startLocation.coordinates.at(0)! + this.data.ride.endLocation.coordinates.at(0)!) / 2
      // lat: (this.data.ride.startLocation.at(1)! + this.data.ride.endLocation.at(1)!) / 2,
      // lng: (this.data.ride.startLocation.at(0)! + this.data.ride.endLocation.at(0)!) / 2
    }
  }

  initMarkers() {
    const initialMarkers = [
      {
        position: {
          lat: this.data.userStartPosition.coordinates.at(1)!,
          lng: this.data.userStartPosition.coordinates.at(0)!
        },
        address: this.data.userStartAddress
      },
      {
        position: {
          lat: this.data.ride.startLocation.coordinates.at(1)!,
          lng: this.data.ride.startLocation.coordinates.at(0)!
          // lat: this.data.ride.startLocation.at(1)!,
          // lng: this.data.ride.startLocation.at(0)!
        },
        address: `${this.data.ride.startAddress?.streetName} ${this.data.ride.startAddress?.homeNumber}, ${this.data.ride.startAddress?.city}`
      },
      {
        position: {
          lat: this.data.ride.endLocation.coordinates.at(1)!,
          lng: this.data.ride.endLocation.coordinates.at(0)!
          // lat: this.data.ride.endLocation.at(1)!,
          // lng: this.data.ride.endLocation.at(0)!
        },
        address: `${this.data.ride.endAddress?.streetName} ${this.data.ride.endAddress?.homeNumber}, ${this.data.ride.endAddress?.city}`
      },
      {
        position: {
          lat: this.data.userEndPosition.coordinates.at(1)!,
          lng: this.data.userEndPosition.coordinates.at(0)!
        },
        address: this.data.userEndAddress
      }
    ];

    const markerCoordinates: Leaflet.LatLng[] = [];

    for (let i = 0; i < initialMarkers.length; i++) {
      const data = initialMarkers[i];
      const {lat, lng} = data.position;
      const marker = this.generateMarker(data);
      if (initialMarkers.indexOf(data) == 0 || initialMarkers.indexOf(data) == 3)
        this.generateCircle(data).addTo(this.map);
      markerCoordinates.push(Leaflet.latLng(lat, lng));
      marker.addTo(this.map).bindPopup(`<b>${data.address}</b>`);
      this.markers.push(marker);
    }

    const dashedPolyline1 = Leaflet.polyline(markerCoordinates.slice(0, 2), {color: 'green', dashArray: '10, 10'});
    dashedPolyline1.addTo(this.map);

    const solidPolyline = Leaflet.polyline(markerCoordinates.slice(1, 3), {color: 'green'});
    solidPolyline.addTo(this.map);

    const dashedPolyline2 = Leaflet.polyline(markerCoordinates.slice(2, 4), {color: 'green', dashArray: '10, 10'});
    dashedPolyline2.addTo(this.map);

    this.map.fitBounds(Leaflet.latLngBounds(markerCoordinates));
  }

  generateMarker(data: any) {
    return Leaflet.marker(data.position);
  }

  generateCircle(data: any) {
    return Leaflet.circle(data.position, {radius: this.data.radiusInMeters});
  }

  onMapReady($event: Leaflet.Map) {
    this.map = $event;
    this.initMarkers();
  }

  getOptions(maxValue: number): number[] {
    return Array.from({length: maxValue}, (_, i) => i + 1);
  }

  reserveRide() {
    this.authService.currentUser$.pipe(takeUntil(this.destroy$)).subscribe((user) => {
      if (user) {
        const reservationKey = {
          rideId: this.data.ride.id,
          passengerId: user.id
        } as RideReservationKey;
        const reservation = {
          id: reservationKey,
          ride: this.data.ride,
          passenger: user,
          numberOfPassengers: this.selectedValue
        } as RideReservation;

        this.rideService.reserveRide(reservation)
          .pipe(
            catchError((error) => {
              this._snackBar.open(`Wystąpił błąd przy rezerwacji przejazdu. Odśwież stronę i spróbuj ponownie`, "Zamknij", {duration: 3000});
              return throwError(error);
            })
          )
          .subscribe(() => {
            this._snackBar.open(`Zarezerowano przejazd! Szczegóły dostępne w zakładce "Moje przejazdy" w sekcji "Moje konto".`, "Zamknij", {duration: 3000});
            this.refreshList.emit();
          });
      }
    })
  }

  ngOnDestroy() {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}
