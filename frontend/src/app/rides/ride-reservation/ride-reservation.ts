import {Point, Ride} from "../ride";
import {User} from "../../user/user";

export interface RideReservation {
  id: RideReservationKey;
  ride: Ride;
  passenger: User;
  numberOfPassengers: number;
  passengerUsername?: string;
  passengerPhoneNumber?: string;
}

export interface RideReservationKey {
  rideId: number;
  passengerId: number;
}

export interface RideReservationDialogInfo {
  ride: Ride;
  userStartPosition: Point;
  userEndPosition: Point;
  radiusInMeters: number,
  userStartAddress: string,
  userEndAddress: string
}
