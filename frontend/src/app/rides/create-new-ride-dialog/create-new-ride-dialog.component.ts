import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AuthService} from "../../login/service/auth.service";
import {RidesService} from "../service/rides.service";
import {catchError, Subject, Subscription, switchMap, take, takeUntil, throwError} from "rxjs";
import {Address} from "../../services/geo/address";
import {GeoService} from "../../services/geo/geo.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Car} from "../../car/car";
import {MatDialog} from "@angular/material/dialog";
import {
  CreateCarDialogComponent
} from "../../user/user-details/account-view/create-car-dialog/create-car-dialog.component";
import {MatMenuTrigger} from "@angular/material/menu";
import {User} from "../../user/user";
import {Ride} from "../ride";
import {CarService} from "../../car/service/car.service";

@Component({
  selector: 'app-create-new-ride-dialog',
  templateUrl: './create-new-ride-dialog.component.html',
  styleUrls: ['./create-new-ride-dialog.component.css']
})
export class CreateNewRideDialogComponent implements OnInit, OnDestroy {

  formControl: FormGroup;
  userCars: Car[] = [];
  availablePlaces: number[] = [];
  private maxPlacesSub: Subscription = new Subscription();
  @ViewChild('menuTrigger') menuTrigger!: MatMenuTrigger;
  currentUser: User | null = null;
  now: Date = new Date();
  maxPlacesAvailable: number = 8;
  private destroy$ = new Subject<void>();


  constructor(private authService: AuthService, private rideService: RidesService, private geoService: GeoService,
              private _snackBar: MatSnackBar, private formBuilder: FormBuilder, private dialog: MatDialog,
              private carService: CarService) {
    const priceValidator = (control: FormControl) => {
      if (!control.value)
        return null;

      const valid = /^\d+(?:[.,]\d{1,2})?$/.test(control.value);
      return valid ? null : {invalidPrice: true};
    };


    this.formControl = this.formBuilder.group({
      address: ['', Validators.required],
      destination: ['', Validators.required],
      date: [this.now, Validators.required],
      car: [null, Validators.required],
      placesAvailable: [null, Validators.required],
      price: ['', [priceValidator, Validators.required]],
      description: ['']
    });

    this.formControl.get('car')?.valueChanges.subscribe((selectedCar: Car) => {
      if (selectedCar)
        this.maxPlacesAvailable = selectedCar.maxPassengers;
    });
  }

  ngOnInit(): void {
    this.refreshUserData();
    this.maxPlacesSub = this.formControl.get('car')!.valueChanges.subscribe((selectedCar: Car | null) => {
      if (selectedCar)
        this.availablePlaces = Array.from({length: selectedCar.maxPassengers}, (_, i) => i + 1);
      else
        this.availablePlaces = [];

      this.formControl.get('placesAvailable')!.setValue(null);
    });
  }

  findMyLocation() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          const lat = position.coords.latitude;
          const lng = position.coords.longitude;
          this.geoService
            .getAddressFromCoordinates(lng, lat)
            .pipe(
              take(1),
              switchMap((address) => {
                this.fillSearchFieldWithMyLocationAddress(address);
                return [];
              })
            )
            .subscribe({
              next: (address) => {
                this.fillSearchFieldWithMyLocationAddress(address);
              },
              error: () => {
                this._snackBar.open(`Błąd przy pobieraniu lokalizacji`, "Zamknij", {duration: 3000});
              },
            });
        },
        (error) => {
          this._snackBar.open(`Błąd przy pobieraniu lokalizacji: ${error}`, "Zamknij", {duration: 3000});
        }
      );
    } else
      this._snackBar.open("Twoja przeglądarka nie wspiera usługi lokalizacji.", "Zamknij", {duration: 3000});
  }

  fillSearchFieldWithMyLocationAddress(address: Address) {
    this.formControl.patchValue({
      address: `${address.streetName} ${address.homeNumber}, ${address.city}`
    })
  }

  createNewRide() {
    if (this.formControl.valid) {
      this.geoService.getCoordinatesFromAddress(this.formControl.get('address')!.value)
        .subscribe((startingLocation) => {
          if (startingLocation) {
            this.geoService.getCoordinatesFromAddress(this.formControl.get('destination')!.value)
              .subscribe((destLocation) => {
                if (destLocation) {
                  const newRide = {
                    placesAvailable: this.formControl.get('placesAvailable')!.value,
                    startingTime: (this.formControl.get('date')!.value).toISOString(),
                    car: this.formControl.get('car')!.value,
                    driver: this.currentUser,
                    price: Math.round(parseFloat(this.formControl.get('price')!.value) * 100),
                    startLocation: startingLocation,
                    endLocation: destLocation,
                    description: this.formControl.get('description')!.value,
                  } as Ride;

                  this.rideService.createNewRide(newRide)
                    .pipe(
                      catchError((error) => {
                        this._snackBar.open("Wystąpił błąd przy tworzeniu przejazdu. Upewnij się, że wszystkie wprowadzone dane są poprawne i spróbuj ponownie", "Zamknij", {duration: 3000});
                        return throwError(() => error);
                      }),
                    )
                    .subscribe(() => {
                      this._snackBar.open("Stworzono przejazd!", "Zamknij", {duration: 3000});
                      this.authService.notifyRideCreated();
                      this.dialog.closeAll();
                    })
                }
              })
          }
        })
    }
  }

  ngOnDestroy() {
    this.maxPlacesSub.unsubscribe();
    this.destroy$.next();
    this.destroy$.complete();
  }

  openCreateNewCarDialog() {
    this.refreshUserData();
    const dialogRef = this.dialog.open(CreateCarDialogComponent, {
      data: this.currentUser,
      restoreFocus: false
    });
    dialogRef.componentInstance.carCreated
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        dialogRef.close();
        this.refreshUserData();
      })
  }

  private refreshUserData() {
    this.authService.currentUser$.pipe(takeUntil(this.destroy$)).subscribe((user) => {
      if (user) {
        this.currentUser = user;
        this.carService.getUserCars(user.id).pipe(takeUntil(this.destroy$)).subscribe((cars) => {
          this.userCars = cars;
          this.formControl.get('car')?.setValue(this.userCars.at(this.userCars.length - 1));
        })
      }
    });

  }
}
