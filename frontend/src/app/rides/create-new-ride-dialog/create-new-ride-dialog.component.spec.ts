import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateNewRideDialogComponent } from './create-new-ride-dialog.component';

describe('CreateNewRideDialogComponent', () => {
  let component: CreateNewRideDialogComponent;
  let fixture: ComponentFixture<CreateNewRideDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CreateNewRideDialogComponent]
    });
    fixture = TestBed.createComponent(CreateNewRideDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
