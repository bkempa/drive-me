import {Car} from "../car/car";
import {UserRating} from "./user-rating/user-rating";

export interface User {
  id: number;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  dateOfBirth: string;
  phoneNumber: string;
  emailAddress: string;
  role: Role;
  blocked: boolean;
  receivedRates: UserRating[];
  writtenRates: UserRating[];
  cars: Car[];
  unblockDateTime: string;
}

export enum Role {
  ADMIN = 'ADMIN',
  USER = 'USER'
}
