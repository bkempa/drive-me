import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../user";
import {environment} from "../../../environment/environment";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${environment.apiUrl}/user/all`);
  }

  getUserById(userId: number): Observable<User> {
    return this.http.get<User>(`${environment.apiUrl}/user/${userId}`);
  }

  getUserByUsername(username: string) {
    return this.http.get<User>(`${environment.apiUrl}/user/username/${username}`);
  }

  blockUserPermanently(user: User) {
    return this.http.put(`${environment.apiUrl}/user/block/perm`, user);
  }

  blockUserForDaysNumber(user: User, days: number) {
    return this.http.put(`${environment.apiUrl}/user/block/${days}`, user);
  }

  deleteUser(userId: number) {
    return this.http.delete(`${environment.apiUrl}/user/${userId}`);
  }

  unblockUser(user: User) {
    return this.http.put(`${environment.apiUrl}/user/unblock`, user);
  }

  grantAdminRights(user: User) {
    return this.http.put(`${environment.apiUrl}/user/admin/grant`, user)
  }

  revokeAdminRights(user: User) {
    return this.http.put(`${environment.apiUrl}/user/admin/revoke`, user)
  }
}
