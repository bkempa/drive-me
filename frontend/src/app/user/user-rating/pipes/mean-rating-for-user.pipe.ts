import { Pipe, PipeTransform } from '@angular/core';
import {User} from "../../user";

@Pipe({
  name: 'meanRatingForUser'
})
export class MeanRatingForUserPipe implements PipeTransform {

  transform(user: User): number {
    if (!user || !user.receivedRates)
      return 0;

    const totalRatings = user.receivedRates.length;
    const sumRatings = user.receivedRates.reduce((sum, rating) => sum + rating.rate, 0);
    return totalRatings > 0 ? sumRatings / totalRatings : 0;
  }

}
