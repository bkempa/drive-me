import {Pipe, PipeTransform} from '@angular/core';
import {UserRating} from "../user-rating";

@Pipe({
  name: 'meanRating'
})
export class MeanRatingPipe implements PipeTransform {

  transform(ratings: UserRating[]): number {
    if (!ratings)
      return 0;

    const totalRatings = ratings.length;
    const sumRatings = ratings.reduce((sum, rating) => sum + rating.rate, 0);
    return totalRatings > 0 ? sumRatings / totalRatings : 0;
  }

}
