import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {UserRating} from "../user-rating";
import {environment} from "../../../../environment/environment";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UserRatingService {

  constructor(private http: HttpClient) { }

  getAllRatings() {
    return this.http.get<UserRating[]>(`${environment.apiUrl}/user/ratings/all`);
  }

  editRating(userRating: UserRating) {
    return this.http.put(`${environment.apiUrl}/user/ratings`, userRating);
  }

  deleteRating(authorId: number, driverId: number) {
    let queryParams: HttpParams = new HttpParams()
      .set('authorId', authorId)
      .set('driverId', driverId);
    return this.http.delete(`${environment.apiUrl}/user/ratings`, {params: queryParams})
  }

  createRating(newRating: UserRating) {
    return this.http.post(`${environment.apiUrl}/user/ratings`, newRating);
  }

  getRatingsWrittenByUser(userId: number): Observable<UserRating[]> {
    return this.http.get<UserRating[]>(`${environment.apiUrl}/user/ratings/id/${userId}/written`);
  }
}
