export interface UserRating {
  id: UserRatingKey;
  rate: number;
  comment: string;
  ratingDateTime?: string;
  updateDateTime?: string;
  passengerUsername?: string;
  driverUsername?: string;
}

export interface UserRatingKey {
  passengerId: number;
  driverId: number;
}
