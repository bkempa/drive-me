import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyArchivedRidesAsDriverComponent } from './my-archived-rides-as-driver.component';

describe('MyArchivedRidesAsDriverComponent', () => {
  let component: MyArchivedRidesAsDriverComponent;
  let fixture: ComponentFixture<MyArchivedRidesAsDriverComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MyArchivedRidesAsDriverComponent]
    });
    fixture = TestBed.createComponent(MyArchivedRidesAsDriverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
