import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ArchivedRide} from "../../../../rides/archive/archived-ride";
import {MatTableDataSource} from "@angular/material/table";
import {Subject, takeUntil} from "rxjs";
import {ArchivedRideService} from "../../../../rides/archive/service/archived-ride.service";
import {MatDialog} from "@angular/material/dialog";
import {GeoService} from "../../../../services/geo/geo.service";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatMenuTrigger} from "@angular/material/menu";
import {AuthService} from "../../../../login/service/auth.service";

@Component({
  selector: 'app-my-archived-rides-as-driver',
  templateUrl: './my-archived-rides-as-driver.component.html',
  styleUrls: ['./my-archived-rides-as-driver.component.css']
})
export class MyArchivedRidesAsDriverComponent implements OnInit, OnDestroy, AfterViewInit {

  ridesList!: ArchivedRide[];
  dataSource = new MatTableDataSource<ArchivedRide>(this.ridesList);
  displayedColumns: string[] = ['startingTime', 'startLocation', 'endLocation', 'car', 'price', 'passengers', 'description'];
  private destroy$ = new Subject();
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild('menuTrigger') menuTrigger!: MatMenuTrigger;

  constructor(private ridesService: ArchivedRideService, public dialog: MatDialog, private geoService: GeoService,
              private authService: AuthService) {
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit(): void {
    this.authService.getCurrentUserId()
      .pipe(takeUntil(this.destroy$))
      .subscribe(userId => {
        if (userId) {
          this.ridesService.getArchivedRidesByDriverId(userId)
            .pipe(takeUntil(this.destroy$))
            .subscribe(rides => {
              this.ridesList = rides;

              for (const ride of this.ridesList) {
                const startLocation = ride.startLocation.coordinates;
                const endLocation = ride.endLocation.coordinates;

                this.geoService.getAddressFromCoordinates(startLocation[0], startLocation[1])
                  .pipe(takeUntil(this.destroy$))
                  .subscribe(startAddress => {
                    ride.startAddress = startAddress;

                    this.geoService.getAddressFromCoordinates(endLocation[0], endLocation[1])
                      .pipe(takeUntil(this.destroy$))
                      .subscribe(endAddress => {
                        ride.endAddress = endAddress;
                      });
                  });
              }
              this.dataSource.data = this.ridesList;
            });
        }
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnDestroy() {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}
