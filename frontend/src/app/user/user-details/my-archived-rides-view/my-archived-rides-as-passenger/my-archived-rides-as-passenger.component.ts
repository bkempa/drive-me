import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ArchivedRide} from "../../../../rides/archive/archived-ride";
import {MatTableDataSource} from "@angular/material/table";
import {Subject, takeUntil} from "rxjs";
import {ArchivedRideService} from "../../../../rides/archive/service/archived-ride.service";
import {MatDialog} from "@angular/material/dialog";
import {GeoService} from "../../../../services/geo/geo.service";
import {AuthService} from "../../../../login/service/auth.service";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatMenuTrigger} from "@angular/material/menu";
import {Ride} from "../../../../rides/ride";
import {CreateRatingDialogComponent} from "./create-rating-dialog/create-rating-dialog.component";
import {User} from "../../../user";
import {UserService} from "../../../service/user.service";

@Component({
  selector: 'app-my-archived-rides-as-passenger',
  templateUrl: './my-archived-rides-as-passenger.component.html',
  styleUrls: ['./my-archived-rides-as-passenger.component.css']
})
export class MyArchivedRidesAsPassengerComponent implements OnInit, OnDestroy, AfterViewInit {

  ridesList!: ArchivedRide[];
  dataSource = new MatTableDataSource<ArchivedRide>(this.ridesList);
  displayedColumns: string[] = ['startingTime', 'startLocation', 'endLocation', 'driver', 'car', 'price', 'description', 'action'];
  currentUser!: User;
  private destroy$ = new Subject();

  constructor(private ridesService: ArchivedRideService, public dialog: MatDialog, private geoService: GeoService,
              private authService: AuthService, private userService: UserService) {}

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild('menuTrigger') menuTrigger!: MatMenuTrigger;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit(): void {
    this.loadCurrentUser();
    this.getArchivedRides();
  }

  openRateDriverDialog(ride: Ride) {
    const dialogRef = this.dialog.open(CreateRatingDialogComponent, {data: ride, restoreFocus: false});
    dialogRef.componentInstance.ratingCreated
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.ngOnInit();
        this.authService.notifyRatingCreatedOrEdited();
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  hasRatedDriver(driverId: number): boolean {
    return (
      this.currentUser &&
      this.currentUser.writtenRates &&
      this.currentUser.writtenRates.some((rating) => rating.id.driverId === driverId)
    );
  }

  ngOnDestroy() {
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  private getArchivedRides() {
    this.authService.currentUser$.subscribe(user => {
      if (user) {
        this.ridesService.getArchivedRidesByPassengerId(user.id)
          .pipe(takeUntil(this.destroy$))
          .subscribe(rides => {
            this.ridesList = rides;
            this.ridesList.forEach((ride) => {
              const startLocation = ride.startLocation.coordinates;
              const endLocation = ride.endLocation.coordinates;

              this.geoService.getAddressFromCoordinates(startLocation[0], startLocation[1])
                .pipe(takeUntil(this.destroy$))
                .subscribe(startAddress => {
                  ride.startAddress = startAddress;

                  this.geoService.getAddressFromCoordinates(endLocation[0], endLocation[1])
                    .pipe(takeUntil(this.destroy$))
                    .subscribe(endAddress => {
                      ride.endAddress = endAddress;
                    });
                });
            });
            this.dataSource.data = this.ridesList;
          });
      }
    });
  }

  private loadCurrentUser() {
    this.authService.currentUser$.pipe(takeUntil(this.destroy$)).subscribe((user) => {
      if (user) {
        this.userService.getUserById(user.id).pipe(takeUntil(this.destroy$)).subscribe((user) => {
          this.currentUser = user;
        });
      }
    })
  }
}
