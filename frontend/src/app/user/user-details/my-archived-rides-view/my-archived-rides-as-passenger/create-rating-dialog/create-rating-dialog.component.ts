import {Component, EventEmitter, Inject, OnDestroy, Output} from '@angular/core';
import {AuthService} from "../../../../../login/service/auth.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {FormBuilder, FormGroup} from "@angular/forms";
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {UserRating, UserRatingKey} from "../../../../user-rating/user-rating";
import {catchError, Subject, takeUntil, throwError} from "rxjs";
import {UserRatingService} from "../../../../user-rating/service/user-rating.service";
import {Ride} from "../../../../../rides/ride";

@Component({
  selector: 'app-create-rating-dialog',
  templateUrl: './create-rating-dialog.component.html',
  styleUrls: ['./create-rating-dialog.component.css']
})
export class CreateRatingDialogComponent implements OnDestroy {
  formControl: FormGroup;
  private destroy$ = new Subject();
  ratingArr: number[] = [];
  private newRating: number = 5;
  @Output() ratingCreated = new EventEmitter<void>();

  constructor(@Inject(MAT_DIALOG_DATA) public data: Ride, private authService: AuthService, private _snackBar: MatSnackBar,
              private formBuilder: FormBuilder, private ratingService: UserRatingService) {
    this.formControl = this.formBuilder.group({
      comment: ['']
    });

    for (let index = 0; index < 5; index++)
      this.ratingArr.push(index);
  }

  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  onClick(rating: number) {
    this.newRating = rating;
  }

  showIcon(index: number) {
    if (this.newRating >= index + 1)
      return 'star';
    else
      return 'star_border';
  }

  createRating() {
    if (this.formControl.valid) {
      this.authService.currentUser$
        .pipe(takeUntil(this.destroy$))
        .subscribe((user) => {
          if (user) {
            const key: UserRatingKey = {
              passengerId: user.id,
              driverId: this.data.driver.id
            }
            const newRating: UserRating = {
              id: key,
              rate: this.newRating,
              comment: this.formControl.get('comment')?.value
            }

            this.ratingService.createRating(newRating)
              .pipe(
                takeUntil(this.destroy$),
                catchError((error) => {
                  this._snackBar.open("Wystąpił błąd przy dodawaniu oceny. Spróbuj ponownie", "Zamknij", {duration: 3000});
                  return throwError(error);
                }),
              )
              .subscribe(() => {
                this._snackBar.open("Dodano opinię!", "Zamknij", {duration: 3000});
                this.ratingCreated.emit();
              });
          }
        })
    }
  }
}
