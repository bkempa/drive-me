import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateRatingDialogComponent } from './create-rating-dialog.component';

describe('CreateRatingDialogComponent', () => {
  let component: CreateRatingDialogComponent;
  let fixture: ComponentFixture<CreateRatingDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CreateRatingDialogComponent]
    });
    fixture = TestBed.createComponent(CreateRatingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
