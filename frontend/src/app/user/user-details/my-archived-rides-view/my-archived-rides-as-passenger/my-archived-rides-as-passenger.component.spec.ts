import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyArchivedRidesAsPassengerComponent } from './my-archived-rides-as-passenger.component';

describe('MyArchivedRidesAsPassengerComponent', () => {
  let component: MyArchivedRidesAsPassengerComponent;
  let fixture: ComponentFixture<MyArchivedRidesAsPassengerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MyArchivedRidesAsPassengerComponent]
    });
    fixture = TestBed.createComponent(MyArchivedRidesAsPassengerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
