import { Component } from '@angular/core';

@Component({
  selector: 'app-my-archived-rides-view',
  templateUrl: './my-archived-rides-view.component.html',
  styleUrls: ['./my-archived-rides-view.component.css']
})
export class MyArchivedRidesViewComponent {

  constructor() {}
}
