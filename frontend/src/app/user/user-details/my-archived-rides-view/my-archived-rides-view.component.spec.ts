import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyArchivedRidesViewComponent } from './my-archived-rides-view.component';

describe('MyArchivedRidesViewComponent', () => {
  let component: MyArchivedRidesViewComponent;
  let fixture: ComponentFixture<MyArchivedRidesViewComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MyArchivedRidesViewComponent]
    });
    fixture = TestBed.createComponent(MyArchivedRidesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
