import {Component, Input} from '@angular/core';
import {UserRating} from "../../../user-rating/user-rating";
import {UserService} from "../../../service/user.service";

@Component({
  selector: 'app-received-reviews',
  templateUrl: './received-reviews.component.html',
  styleUrls: ['./received-reviews.component.css']
})
export class ReceivedReviewsComponent {

  @Input() rates!: UserRating[];
  pageIndex = 0;
  pageSize = 20;

  constructor(protected userService: UserService) {
  }

  numSequence(n: number): Array<number> {
    return Array(n);
  }

  getRatesOnCurrentPage(): UserRating[] {
    const startIndex = this.pageIndex * this.pageSize;
    const endIndex = startIndex + this.pageSize;
    return this.rates.slice(startIndex, endIndex);
  }
}
