import {Component, EventEmitter, Inject, Output} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {UserRating} from "../../../../user-rating/user-rating";
import {FormBuilder, FormGroup} from "@angular/forms";
import {catchError, Subject, takeUntil, throwError} from "rxjs";
import {MatSnackBar} from "@angular/material/snack-bar";
import {UserRatingService} from "../../../../user-rating/service/user-rating.service";

@Component({
  selector: 'app-edit-rating-dialog',
  templateUrl: './edit-rating-dialog.component.html',
  styleUrls: ['./edit-rating-dialog.component.css']
})
export class EditRatingDialogComponent {

  formControl: FormGroup;
  private destroy$ = new Subject();
  @Output() ratingEdited = new EventEmitter<UserRating>();

  ratingArr: number[] = [];
  private newRating: number = this.userRating.rate;

  constructor(@Inject(MAT_DIALOG_DATA) public userRating: UserRating, private fb: FormBuilder, private ratingService: UserRatingService,
              private _snackBar: MatSnackBar) {

    this.formControl = this.fb.group({
      comment: [userRating.comment]
    });

    for (let index = 0; index < 5; index++)
      this.ratingArr.push(index);
  }

  onClick(rating: number) {
    this.newRating = rating;
  }

  showIcon(index: number) {
    if (this.newRating >= index + 1)
      return 'star';
    else
      return 'star_border';
  }

  updateRating() {
    if (this.formControl.valid) {
      this.userRating.comment = this.formControl.get('comment')?.value;
      this.userRating.rate = this.newRating;

      this.ratingService.editRating(this.userRating)
        .pipe(
          takeUntil(this.destroy$),
          catchError((error) => {
            this._snackBar.open("Wystąpił błąd przy edycji opinii. Spróbuj ponownie", "Zamknij", {duration: 3000});
            return throwError(error);
          }),
        )
        .subscribe(() => {
          this._snackBar.open("Edytowano opinię", "Zamknij", {duration: 3000});
          this.ratingEdited.emit(this.userRating);
        });
    }
  }
}
