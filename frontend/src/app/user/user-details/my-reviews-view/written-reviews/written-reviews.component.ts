import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {UserRating} from "../../../user-rating/user-rating";
import {UserService} from "../../../service/user.service";
import {DeleteRatingDialogComponent} from "./delete-rating-dialog/delete-rating-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {MatMenuTrigger} from "@angular/material/menu";
import {EditRatingDialogComponent} from "./edit-rating-dialog/edit-rating-dialog.component";
import {filter, forkJoin, map, Observable, of, Subject, switchMap, takeUntil} from "rxjs";
import {AuthService} from "../../../../login/service/auth.service";
import {UserRatingService} from "../../../user-rating/service/user-rating.service";

@Component({
  selector: 'app-written-reviews',
  templateUrl: './written-reviews.component.html',
  styleUrls: ['./written-reviews.component.css']
})
export class WrittenReviewsComponent implements OnInit, OnDestroy {

  rates: UserRating[] = [];
  writtenRates$: Observable<UserRating[]> = new Observable<UserRating[]>();
  @ViewChild('menuTrigger') menuTrigger!: MatMenuTrigger;
  pageIndex = 0;
  pageSize = 20;
  private destroy$ = new Subject();

  constructor(protected userService: UserService, public dialog: MatDialog, private authService: AuthService,
              private userRatingService: UserRatingService) {}

  ngOnInit() {
    this.getWrittenRates();
    this.authService.ratingCreatedOrEdited$.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.getWrittenRates();
    })
  }

  editRate(review: UserRating) {
    const dialogRef = this.dialog.open(EditRatingDialogComponent, {
      data: review,
      restoreFocus: false
    });
    dialogRef.componentInstance.ratingEdited
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.authService.notifyRatingCreatedOrEdited();
        this.getWrittenRates();
      });
  }

  deleteReview(review: UserRating) {
    const dialogRef = this.dialog.open(DeleteRatingDialogComponent, {
      data: review,
      restoreFocus: false
    });
    dialogRef.componentInstance.ratingDeleted
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.authService.notifyRatingCreatedOrEdited();
        this.getWrittenRates();
      });
  }

  numSequence(n: number): Array<number> {
    return Array(n);
  }

  pageChanged(event: any): void {
    this.pageIndex = event.pageIndex;
  }

  getRatesOnCurrentPage(): UserRating[] {
    const startIndex = this.pageIndex * this.pageSize;
    const endIndex = startIndex + this.pageSize;
    return this.rates.slice(startIndex, endIndex);
  }

  private getWrittenRates() {
    this.writtenRates$ = this.authService.currentUser$.pipe(
      takeUntil(this.destroy$),
      filter((user) => !!user),
      switchMap((user) => {
        const username = user!.username;
        return this.userService.getUserByUsername(username).pipe(
          switchMap((currentUser) => {
            return this.userRatingService.getRatingsWrittenByUser(currentUser.id).pipe(
              switchMap((rates) => {
                if (rates.length === 0)
                  return of([]);
                const getUserObservables = rates.map((rate) => this.userService.getUserById(rate.id.driverId));
                return forkJoin(getUserObservables).pipe(
                  map((drivers) => {
                    return rates.map((rate, index) => {
                      return {
                        ...rate,
                        driverUsername: drivers[index].username,
                      };
                    });
                  })
                );
              })
            );
          })
        );
      })
    );

    this.writtenRates$.pipe(takeUntil(this.destroy$)).subscribe((rates) => {
      this.rates = rates;
    });
  }

  ngOnDestroy() {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}
