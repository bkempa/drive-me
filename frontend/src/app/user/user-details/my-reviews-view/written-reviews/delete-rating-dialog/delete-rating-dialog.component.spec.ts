import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteRatingDialogComponent } from './delete-rating-dialog.component';

describe('DeleteCommentDialogComponent', () => {
  let component: DeleteRatingDialogComponent;
  let fixture: ComponentFixture<DeleteRatingDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DeleteRatingDialogComponent]
    });
    fixture = TestBed.createComponent(DeleteRatingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
