import {Component, EventEmitter, Inject, Output} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {UserRating} from "../../../../user-rating/user-rating";
import {catchError, throwError} from "rxjs";
import {MatSnackBar} from "@angular/material/snack-bar";
import {UserRatingService} from "../../../../user-rating/service/user-rating.service";

@Component({
  selector: 'app-delete-comment-dialog',
  templateUrl: './delete-rating-dialog.component.html',
  styleUrls: ['./delete-rating-dialog.component.css']
})
export class DeleteRatingDialogComponent {

  @Output() ratingDeleted = new EventEmitter<void>();

  constructor(@Inject(MAT_DIALOG_DATA) public userRating: UserRating, private ratingService: UserRatingService,
              private _snackBar: MatSnackBar) {}

  deleteRating() {
    this.ratingService.deleteRating(this.userRating.id.passengerId, this.userRating.id.driverId)
      .pipe(
        catchError((error) => {
          this._snackBar.open('Wystąpił błąd przy usuwaniu opinii. Spróbuj ponownie', "Zamknij", {duration: 3000});
          return throwError(error);
        }),
      )
      .subscribe(() => {
        this._snackBar.open("Usunięto opinię", "Zamknij", {duration: 3000});
        this.ratingDeleted.emit();
      });
  }
}
