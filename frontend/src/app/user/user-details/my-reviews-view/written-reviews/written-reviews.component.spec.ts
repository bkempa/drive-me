import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WrittenReviewsComponent } from './written-reviews.component';

describe('WrittenReviewsComponent', () => {
  let component: WrittenReviewsComponent;
  let fixture: ComponentFixture<WrittenReviewsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [WrittenReviewsComponent]
    });
    fixture = TestBed.createComponent(WrittenReviewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
