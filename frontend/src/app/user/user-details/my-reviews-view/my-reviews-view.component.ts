import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../../login/service/auth.service";
import {filter, forkJoin, map, Observable, switchMap} from "rxjs";
import {UserService} from "../../service/user.service";
import {UserRating} from "../../user-rating/user-rating";

@Component({
  selector: 'app-my-reviews-view',
  templateUrl: './my-reviews-view.component.html',
  styleUrls: ['./my-reviews-view.component.css']
})
export class MyReviewsViewComponent implements OnInit {

  receivedRates$: Observable<UserRating[]> = new Observable<UserRating[]>();

  constructor(private authService: AuthService, private userService: UserService) {}

  ngOnInit() {
    this.receivedRates$ = this.authService.currentUser$.pipe(
      filter(user => !!user),
      map(user => user!.receivedRates),
      switchMap(receivedRates => {
        const getUserObservables = receivedRates.map(rate => this.userService.getUserById(rate.id.passengerId));
        return forkJoin(getUserObservables).pipe(
          map(users => {
            return receivedRates.map((rate, index) => {
              return { ...rate, passengerUsername: users[index].username };
            });
          })
        );
      })
    );
  }
}
