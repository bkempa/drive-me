import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyReviewsViewComponent } from './my-reviews-view.component';

describe('MyReviewsViewComponent', () => {
  let component: MyReviewsViewComponent;
  let fixture: ComponentFixture<MyReviewsViewComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MyReviewsViewComponent]
    });
    fixture = TestBed.createComponent(MyReviewsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
