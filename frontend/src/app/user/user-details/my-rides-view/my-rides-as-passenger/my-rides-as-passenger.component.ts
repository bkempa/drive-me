import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Ride} from "../../../../rides/ride";
import {MatMenuTrigger} from "@angular/material/menu";
import {MatDialog} from "@angular/material/dialog";
import {MyRidesAsPassengerDialogComponent} from "./my-rides-as-passenger-dialog/my-rides-as-passenger-dialog.component";
import {map, of, Subject, switchMap, takeUntil} from "rxjs";
import {RidesService} from "../../../../rides/service/rides.service";
import {GeoService} from "../../../../services/geo/geo.service";
import {ActivatedRoute} from "@angular/router";
import {UserService} from "../../../service/user.service";

@Component({
  selector: 'app-my-rides-as-passenger',
  templateUrl: './my-rides-as-passenger.component.html',
  styleUrls: ['./my-rides-as-passenger.component.css']
})
export class MyRidesAsPassengerComponent implements OnInit, OnDestroy {

  rides: Ride[] = [];
  @ViewChild('menuTrigger') menuTrigger!: MatMenuTrigger;
  private destroy$ = new Subject();

  constructor(public dialog: MatDialog, private ridesService: RidesService,
              private geoService: GeoService, private router: ActivatedRoute, private userService: UserService) {
  }

  ngOnInit() {
    this.getUserRidesAsPassenger();
  }

  rideDetailsDialog(ride: Ride) {
    const dialogRef = this.dialog.open(MyRidesAsPassengerDialogComponent, {
      data: ride,
      restoreFocus: false
    });
    dialogRef.componentInstance.refreshList
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        dialogRef.close();
        this.getUserRidesAsPassenger();
      });
  }

  ngOnDestroy() {
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  private getUserRidesAsPassenger() {
    this.router.paramMap
      .pipe(
        takeUntil(this.destroy$),
        map((params) => params.get('username')),
        switchMap((username) => {
          if (username)
            return this.userService.getUserByUsername(username);
          else
            return of(null);
        })
      )
      .subscribe(user => {
        if (user) {
          this.ridesService.getRidesByPassengerId(user.id)
            .pipe(takeUntil(this.destroy$))
            .subscribe(rides => {
              this.rides = rides;

              for (const ride of this.rides) {
                const startLocation = ride.startLocation.coordinates;
                const endLocation = ride.endLocation.coordinates;

                this.geoService.getAddressFromCoordinates(startLocation[0], startLocation[1])
                  .pipe(takeUntil(this.destroy$))
                  .subscribe(startAddress => {
                    ride.startAddress = startAddress;

                    this.geoService.getAddressFromCoordinates(endLocation[0], endLocation[1])
                      .pipe(takeUntil(this.destroy$))
                      .subscribe(endAddress => {
                        ride.endAddress = endAddress;
                      });
                  });
              }
            })
        }
      });
    if (this.rides)
      this.rides.sort((a, b) => new Date(a.startingTime).getTime() - new Date(b.startingTime).getTime());
  }
}
