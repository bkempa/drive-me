import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyRidesAsPassengerComponent } from './my-rides-as-passenger.component';

describe('MyRidesAsPassengerComponent', () => {
  let component: MyRidesAsPassengerComponent;
  let fixture: ComponentFixture<MyRidesAsPassengerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MyRidesAsPassengerComponent]
    });
    fixture = TestBed.createComponent(MyRidesAsPassengerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
