import {Component, EventEmitter, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {Ride} from "../../../../../rides/ride";
import * as Leaflet from "leaflet";
import {AuthService} from "../../../../../login/service/auth.service";
import {catchError, Subject, takeUntil, throwError} from "rxjs";
import {MatSnackBar} from "@angular/material/snack-bar";
import {RidesService} from "../../../../../rides/service/rides.service";

@Component({
  selector: 'app-my-rides-as-passenger-dialog',
  templateUrl: './my-rides-as-passenger-dialog.component.html',
  styleUrls: ['./my-rides-as-passenger-dialog.component.css']
})
export class MyRidesAsPassengerDialogComponent {
  private destroy$ = new Subject();
  refreshList = new EventEmitter<void>();
  map!: Leaflet.Map;
  markers: Leaflet.Marker[] = [];
  options = {
    layers: [
      Leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
      })
    ],
    zoom: 16,
    center: {
      lat: (this.ride.startLocation.coordinates.at(1)! + this.ride.endLocation.coordinates.at(1)!) / 2,
      lng: (this.ride.startLocation.coordinates.at(0)! + this.ride.endLocation.coordinates.at(0)!) / 2
    }
  }

  constructor(@Inject(MAT_DIALOG_DATA) public ride: Ride, private rideService: RidesService, private authService: AuthService,
              private _snackBar: MatSnackBar) {}


  removeReservation() {
    this.authService.getCurrentUserId().pipe(takeUntil(this.destroy$)).subscribe((userId) => {
      if (userId) {
        this.rideService.removeReservation(this.ride.id!, userId)
          .pipe(
            takeUntil(this.destroy$),
            catchError((error) => {
              this._snackBar.open(`Wystąpił błąd przy anulowaniu rezerwacji przejazdu. Odśwież stronę i spróbuj ponownie`, "Zamknij", {duration: 3000});
              return throwError(error);
            })
          )
          .subscribe(() => {
            this._snackBar.open(`Anulowano rezerwację.`, "Zamknij", {duration: 3000});
            this.refreshList.emit();
          });
      }
    });
  }


  initMarkers() {
    const initialMarkers = [
      {
        position: {
          lat: this.ride.startLocation.coordinates.at(1)!,
          lng: this.ride.startLocation.coordinates.at(0)!
        },
        address: `${this.ride.startAddress?.streetName} ${this.ride.startAddress?.homeNumber}, ${this.ride.startAddress?.city}`
      },
      {
        position: {
          lat: this.ride.endLocation.coordinates.at(1)!,
          lng: this.ride.endLocation.coordinates.at(0)!
        },
        address: `${this.ride.endAddress?.streetName} ${this.ride.endAddress?.homeNumber}, ${this.ride.endAddress?.city}`
      }
    ];

    const markerCoordinates: Leaflet.LatLng[] = [];

    for (let i = 0; i < initialMarkers.length; i++) {
      const data = initialMarkers[i];
      const {lat, lng} = data.position;
      const marker = this.generateMarker(data);
      markerCoordinates.push(Leaflet.latLng(lat, lng));
      marker.addTo(this.map).bindPopup(`<b>${data.address}</b>`);
      this.markers.push(marker);
    }

    Leaflet.polyline(markerCoordinates, {color: 'green'}).addTo(this.map);

    this.map.fitBounds(Leaflet.latLngBounds(markerCoordinates));
  }

  generateMarker(data: any) {
    return Leaflet.marker(data.position);
  }

  onMapReady($event: Leaflet.Map) {
    this.map = $event;
    this.initMarkers();
  }
}
