import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyRidesAsPassengerDialogComponent } from './my-rides-as-passenger-dialog.component';

describe('MyRidesAsPassengerDialogComponent', () => {
  let component: MyRidesAsPassengerDialogComponent;
  let fixture: ComponentFixture<MyRidesAsPassengerDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MyRidesAsPassengerDialogComponent]
    });
    fixture = TestBed.createComponent(MyRidesAsPassengerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
