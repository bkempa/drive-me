import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditRideDialogComponent } from './edit-ride-dialog.component';

describe('EditRideDialogComponent', () => {
  let component: EditRideDialogComponent;
  let fixture: ComponentFixture<EditRideDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EditRideDialogComponent]
    });
    fixture = TestBed.createComponent(EditRideDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
