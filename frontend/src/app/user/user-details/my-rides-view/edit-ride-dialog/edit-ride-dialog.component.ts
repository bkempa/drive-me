import {Component, EventEmitter, Inject, OnDestroy, Output, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from "@angular/material/dialog";
import {Ride} from "../../../../rides/ride";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Car} from "../../../../car/car";
import {CreateCarDialogComponent} from "../../account-view/create-car-dialog/create-car-dialog.component";
import {MatMenuTrigger} from "@angular/material/menu";
import {catchError, Subject, switchMap, take, takeUntil, throwError} from "rxjs";
import {Address} from "../../../../services/geo/address";
import {GeoService} from "../../../../services/geo/geo.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {RidesService} from "../../../../rides/service/rides.service";

@Component({
  selector: 'app-edit-ride-dialog',
  templateUrl: './edit-ride-dialog.component.html',
  styleUrls: ['./edit-ride-dialog.component.css']
})
export class EditRideDialogComponent implements OnDestroy {

  formControl: FormGroup;
  userCars: Car[] = [];
  now: Date = new Date();
  @ViewChild('menuTrigger') menuTrigger!: MatMenuTrigger;
  maxPlacesAvailable: number = 8;
  @Output() rideEdited = new EventEmitter<Ride>();
  private destroy$ = new Subject();


  constructor(@Inject(MAT_DIALOG_DATA) public ride: Ride, private fb: FormBuilder, private dialog: MatDialog,
              private geoService: GeoService, private _snackBar: MatSnackBar, private rideService: RidesService) {

    this.userCars = ride.driver.cars;
    this.maxPlacesAvailable = ride.car.maxPassengers;

    const priceValidator = (control: FormControl) => {
      if (!control.value)
        return null;

      const valid = /^\d+(?:[.,]\d{1,2})?$/.test(control.value);
      return valid ? null : {invalidPrice: true};
    };

    const startLocation = ride.startLocation.coordinates;
    const endLocation = ride.endLocation.coordinates;

    this.geoService.getAddressFromCoordinates(startLocation[0], startLocation[1])
      .pipe(takeUntil(this.destroy$))
      .subscribe(startAddress => {
        ride.startAddress = startAddress;

        this.geoService.getAddressFromCoordinates(endLocation[0], endLocation[1])
          .pipe(takeUntil(this.destroy$))
          .subscribe(endAddress => {
            ride.endAddress = endAddress;
          });
      });


    this.formControl = this.fb.group({
      address: [`${ride.startAddress?.streetName} ${ride.startAddress?.homeNumber}, ${ride.startAddress?.city}`, Validators.required],
      destination: [`${ride.endAddress?.streetName} ${ride.endAddress?.homeNumber}, ${ride.endAddress?.city}`, Validators.required],
      date: [ride.startingTime, Validators.required],
      car: [this.ride.car, Validators.required],
      placesAvailable: [ride.placesAvailable, Validators.required],
      price: [ride.price / 100, [priceValidator, Validators.required]],
      description: [ride.description]
    });

    this.formControl.get('car')?.valueChanges.subscribe((selectedCar: Car) => {
      if (selectedCar)
        this.maxPlacesAvailable = selectedCar.maxPassengers;
    });
  }

  findMyLocation() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          const lat = position.coords.latitude;
          const lng = position.coords.longitude;
          this.geoService
            .getAddressFromCoordinates(lng, lat)
            .pipe(
              take(1),
              switchMap((address) => {
                this.fillSearchFieldWithMyLocationAddress(address);
                return [];
              })
            )
            .subscribe({
              next: (address) => {
                this.fillSearchFieldWithMyLocationAddress(address);
              },
              error: () => {
                this._snackBar.open(`Błąd przy pobieraniu lokalizacji`, "Zamknij", {duration: 3000});
              },
            });
        },
        (error) => {
          this._snackBar.open(`Błąd przy pobieraniu lokalizacji: ${error}`, "Zamknij", {duration: 3000});
        }
      );
    } else
      this._snackBar.open("Twoja przeglądarka nie wspiera usługi lokalizacji.", "Zamknij", {duration: 3000});
  }

  fillSearchFieldWithMyLocationAddress(address: Address) {
    this.formControl.patchValue({
      address: `${address.streetName} ${address.homeNumber}, ${address.city}`
    })
  }

  openCreateNewCarDialog() {
    const dialogRef = this.dialog.open(CreateCarDialogComponent, {
      data: this.ride.driver,
      restoreFocus: false
    });
    dialogRef.afterClosed().subscribe(() => this.menuTrigger.focus());
  }

  editRide() {
    if (this.formControl.valid) {
      this.geoService.getCoordinatesFromAddress(this.formControl.get('address')!.value)
        .subscribe((startingLocation) => {
          if (startingLocation) {
            this.geoService.getCoordinatesFromAddress(this.formControl.get('destination')!.value)
              .subscribe((destLocation) => {
                if (destLocation) {
                  this.ride.placesAvailable = this.formControl.get('placesAvailable')!.value;
                  this.ride.startingTime = new Date(this.formControl.get('date')!.value).toISOString();
                  this.ride.car = this.formControl.get('car')!.value;
                  this.ride.price = Math.round(parseFloat(this.formControl.get('price')!.value) * 100);
                  this.ride.startLocation = startingLocation;
                  this.ride.endLocation = destLocation;
                  this.ride.description = this.formControl.get('description')!.value;

                  this.rideService.editRide(this.ride)
                    .pipe(
                      catchError((error) => {
                        this._snackBar.open("Wystąpił błąd przy edycji przejazdu. Upewnij się, że wszystkie wprowadzone dane są poprawne i spróbuj ponownie", "Zamknij", {duration: 3000});
                        return throwError(() => error);
                      }),
                    )
                    .subscribe(() => {
                      this._snackBar.open("Edytowano przejazd!", "Zamknij", {duration: 3000});
                      this.rideEdited.emit(this.ride);
                    })
                }
              })
          }
        })
    }
  }

  ngOnDestroy() {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}
