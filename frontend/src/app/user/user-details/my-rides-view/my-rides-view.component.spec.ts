import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyRidesViewComponent } from './my-rides-view.component';

describe('MyRidesViewComponent', () => {
  let component: MyRidesViewComponent;
  let fixture: ComponentFixture<MyRidesViewComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MyRidesViewComponent]
    });
    fixture = TestBed.createComponent(MyRidesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
