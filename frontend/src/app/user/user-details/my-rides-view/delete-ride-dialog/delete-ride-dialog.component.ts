import {Component, EventEmitter, Inject, Output} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {Ride} from "../../../../rides/ride";
import {catchError, throwError} from "rxjs";
import {MatSnackBar} from "@angular/material/snack-bar";
import {RidesService} from "../../../../rides/service/rides.service";

@Component({
  selector: 'app-delete-ride-dialog',
  templateUrl: './delete-ride-dialog.component.html',
  styleUrls: ['./delete-ride-dialog.component.css']
})
export class DeleteRideDialogComponent {

  @Output() rideDeleted = new EventEmitter<void>();

  constructor(@Inject(MAT_DIALOG_DATA) public ride: Ride, private rideService: RidesService, private _snackBar: MatSnackBar) {}

  deleteRide() {
    this.rideService.deleteRide(this.ride.id!)
      .pipe(
        catchError((error) => {
          this._snackBar.open("Wystąpił błąd przy usuwaniu przejazdu. Spróbuj ponownie", "Zamknij", {duration: 3000});
          return throwError(error);
        }),
      )
      .subscribe(() => {
        this._snackBar.open("Usunięto przejazd!", "Zamknij", {duration: 3000});
        this.rideDeleted.emit();
      });
  }
}
