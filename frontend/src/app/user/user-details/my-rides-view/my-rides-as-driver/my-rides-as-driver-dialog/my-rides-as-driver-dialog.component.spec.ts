import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyRidesAsDriverDialogComponent } from './my-rides-as-driver-dialog.component';

describe('MyRidesAsDriverDialogComponent', () => {
  let component: MyRidesAsDriverDialogComponent;
  let fixture: ComponentFixture<MyRidesAsDriverDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MyRidesAsDriverDialogComponent]
    });
    fixture = TestBed.createComponent(MyRidesAsDriverDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
