import {Component, EventEmitter, Inject, OnDestroy, Output, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from "@angular/material/dialog";
import {Ride} from "../../../../../rides/ride";
import * as Leaflet from "leaflet";
import {DeleteRideDialogComponent} from "../../delete-ride-dialog/delete-ride-dialog.component";
import {MatMenuTrigger} from "@angular/material/menu";
import {Subject, takeUntil} from "rxjs";

@Component({
  selector: 'app-my-rides-as-driver-dialog',
  templateUrl: './my-rides-as-driver-dialog.component.html',
  styleUrls: ['./my-rides-as-driver-dialog.component.css']
})
export class MyRidesAsDriverDialogComponent implements OnDestroy {

  constructor(@Inject(MAT_DIALOG_DATA) public ride: Ride, public dialog: MatDialog) {}

  @Output() rideDeleted = new EventEmitter<void>();
  @ViewChild('menuTrigger') menuTrigger!: MatMenuTrigger;
  private destroy$ = new Subject();

  map!: Leaflet.Map;
  markers: Leaflet.Marker[] = [];
  options = {
    layers: [
      Leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
      })
    ],
    zoom: 16,
    center: {
      lat: (this.ride.startLocation.coordinates.at(1)! + this.ride.endLocation.coordinates.at(1)!) / 2,
      lng: (this.ride.startLocation.coordinates.at(0)! + this.ride.endLocation.coordinates.at(0)!) / 2
    }
  }

  deleteRide() {
    const dialogRef = this.dialog.open(DeleteRideDialogComponent, {
      data: this.ride,
      restoreFocus: false
    });
    dialogRef.componentInstance.rideDeleted.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.rideDeleted.emit();
    });
  }

  initMarkers() {
    const initialMarkers = [
      {
        position: {
          lat: this.ride.startLocation.coordinates.at(1)!,
          lng: this.ride.startLocation.coordinates.at(0)!
          // lat: this.ride.startLocation.at(1)!,
          // lng: this.ride.startLocation.at(0)!
        },
        address: `${this.ride.startAddress?.streetName} ${this.ride.startAddress?.homeNumber}, ${this.ride.startAddress?.city}`
      },
      {
        position: {
          lat: this.ride.endLocation.coordinates.at(1)!,
          lng: this.ride.endLocation.coordinates.at(0)!
          // lat: this.ride.endLocation.at(1)!,
          // lng: this.ride.endLocation.at(0)!
        },
        address: `${this.ride.endAddress?.streetName} ${this.ride.endAddress?.homeNumber}, ${this.ride.endAddress?.city}`
      }
    ];

    const markerCoordinates: Leaflet.LatLng[] = [];

    for (let i = 0; i < initialMarkers.length; i++) {
      const data = initialMarkers[i];
      const {lat, lng} = data.position;
      const marker = this.generateMarker(data);
      markerCoordinates.push(Leaflet.latLng(lat, lng));
      marker.addTo(this.map).bindPopup(`<b>${data.address}</b>`);
      this.markers.push(marker);
    }

    Leaflet.polyline(markerCoordinates, {color: 'green'}).addTo(this.map);

    this.map.fitBounds(Leaflet.latLngBounds(markerCoordinates));
  }

  generateMarker(data: any) {
    return Leaflet.marker(data.position);
  }

  onMapReady($event: Leaflet.Map) {
    this.map = $event;
    this.initMarkers();
  }

  ngOnDestroy() {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}
