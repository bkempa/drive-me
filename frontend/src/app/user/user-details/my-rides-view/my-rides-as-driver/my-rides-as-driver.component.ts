import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Ride} from "../../../../rides/ride";
import {MatMenuTrigger} from "@angular/material/menu";
import {MatDialog} from "@angular/material/dialog";
import {MyRidesAsDriverDialogComponent} from "./my-rides-as-driver-dialog/my-rides-as-driver-dialog.component";
import {EditRideDialogComponent} from "../edit-ride-dialog/edit-ride-dialog.component";
import {MediaMatcher} from "@angular/cdk/layout";
import {AuthService} from "../../../../login/service/auth.service";
import {GeoService} from "../../../../services/geo/geo.service";
import {UserService} from "../../../service/user.service";
import {map, of, Subject, switchMap, takeUntil} from "rxjs";
import {RidesService} from "../../../../rides/service/rides.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-my-rides-as-driver',
  templateUrl: './my-rides-as-driver.component.html',
  styleUrls: ['./my-rides-as-driver.component.css']
})
export class MyRidesAsDriverComponent implements OnInit, OnDestroy {

  rides: Ride[] = [];
  @ViewChild('menuTrigger') menuTrigger!: MatMenuTrigger;
  dialogWidth: string = '30%';
  private destroy$ = new Subject();

  constructor(public dialog: MatDialog, private mediaMatcher: MediaMatcher, private authService: AuthService,
              private geoService: GeoService, private userService: UserService, private ridesService: RidesService,
              private router: ActivatedRoute) {
  }

  ngOnInit() {
    this.getUserRidesAsDriver();
    this.authService.rideCreated$.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.getUserRidesAsDriver();
    });
  }

  rideDetailsDialog(ride: Ride) {
    const dialogRef = this.dialog.open(MyRidesAsDriverDialogComponent, {
      data: ride,
      restoreFocus: false
    });
    dialogRef.componentInstance.rideDeleted.subscribe(() => {
      dialogRef.close();
      this.getUserRidesAsDriver();
    });
  }

  editRide(ride: Ride) {
    this.setDialogWidth();
    const dialogRef = this.dialog.open(EditRideDialogComponent, {
      data: ride,
      restoreFocus: false,
      width: this.dialogWidth
    });
    dialogRef.componentInstance.rideEdited
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        dialogRef.close();
        this.getUserRidesAsDriver();
      });
  }

  setDialogWidth() {
    if (this.mediaMatcher.matchMedia('(max-width: 600px)').matches)
      this.dialogWidth = '90%';
    else
      this.dialogWidth = '25%';
  }

  ngOnDestroy() {
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  private getUserRidesAsDriver() {
    this.router.paramMap
      .pipe(
        map((params) => params.get('username')),
        switchMap((username) => {
          if (username)
            return this.userService.getUserByUsername(username);
          else
            return of(null);
        })
      )
      .pipe(takeUntil(this.destroy$))
      .subscribe((user) => {
        if (user) {
          this.ridesService.getRidesByDriverId(user.id).pipe(takeUntil(this.destroy$)).subscribe(rides => {
            this.rides = rides;

            for (const ride of this.rides) {
              const startLocation = ride.startLocation.coordinates;
              const endLocation = ride.endLocation.coordinates;

              this.geoService.getAddressFromCoordinates(startLocation[0], startLocation[1])
                .pipe(takeUntil(this.destroy$))
                .subscribe(startAddress => {
                  ride.startAddress = startAddress;

                  this.geoService.getAddressFromCoordinates(endLocation[0], endLocation[1])
                    .pipe(takeUntil(this.destroy$))
                    .subscribe(endAddress => {
                      ride.endAddress = endAddress;
                    });
                });

              if (ride.reservations != undefined) {
                for (const reservation of ride.reservations) {
                  this.userService.getUserById(reservation.id.passengerId)
                    .pipe(takeUntil(this.destroy$))
                    .subscribe(passenger => {
                      reservation.passengerUsername = passenger.username;
                      reservation.passengerPhoneNumber = passenger.phoneNumber;
                    })
                }
              }
            }
          });
        }
      })
    if (this.rides)
      this.rides.sort((a, b) => new Date(a.startingTime).getTime() - new Date(b.startingTime).getTime());
  }
}
