import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyRidesAsDriverComponent } from './my-rides-as-driver.component';

describe('MyRidesAsDriverComponent', () => {
  let component: MyRidesAsDriverComponent;
  let fixture: ComponentFixture<MyRidesAsDriverComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MyRidesAsDriverComponent]
    });
    fixture = TestBed.createComponent(MyRidesAsDriverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
