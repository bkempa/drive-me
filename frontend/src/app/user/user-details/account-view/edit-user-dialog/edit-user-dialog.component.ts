import {Component, EventEmitter, Inject, Output} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {User} from "../../../user";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {catchError, throwError} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {RegisterRequest} from "../../../../login/auth-model/register-request";

@Component({
  selector: 'app-edit-user-dialog',
  templateUrl: './edit-user-dialog.component.html',
  styleUrls: ['./edit-user-dialog.component.css']
})
export class EditUserDialogComponent {

  editFormGroup: FormGroup;
  hideNewPassword = true;
  @Output() userUpdated = new EventEmitter<void>();


  constructor(@Inject(MAT_DIALOG_DATA) public user: User, private fb: FormBuilder, private http: HttpClient) {
    this.editFormGroup = this.fb.group({
      firstName: [user.firstName, Validators.required],
      lastName: [user.lastName, Validators.required],
      email: [user.emailAddress, [Validators.required, Validators.email]],
      birthDate: [user.dateOfBirth, Validators.required],
      phoneNumber: [user.phoneNumber, [Validators.required, Validators.pattern(/^[\d\s+()-]+$/)]],
      newPassword: ['', Validators.pattern(/^(?=.*\d)(?=.*[!@#$%^&*]+)(?=.*[A-Z])(?=.*[a-z]).{8,}$/)]
    });
  }

  save() {
    if (this.editFormGroup.valid) {
      let request: RegisterRequest;
      if (this.editFormGroup.get('newPassword')?.value == '') {
        request = {
          firstName: this.editFormGroup.get('firstName')!.value,
          lastName: this.editFormGroup.get('lastName')!.value,
          emailAddress: this.editFormGroup.get('email')!.value,
          dateOfBirth: this.editFormGroup.get('birthDate')!.value,
          phoneNumber: this.editFormGroup.get('phoneNumber')!.value,
        } as RegisterRequest;
      } else {
        request = {
          firstName: this.editFormGroup.get('firstName')!.value,
          lastName: this.editFormGroup.get('lastName')!.value,
          emailAddress: this.editFormGroup.get('email')!.value,
          dateOfBirth: this.editFormGroup.get('birthDate')!.value,
          phoneNumber: this.editFormGroup.get('phoneNumber')!.value,
          password: this.editFormGroup.get('newPassword')!.value
        } as RegisterRequest;
      }

      this.http.put(`/user/${this.user.id!}`, request)
        .pipe(
          catchError((error) => {
            console.error('Error updating the user:', error);
            return throwError(error);
          }),
        )
        .subscribe((response) => {
          console.log('User updated successfully:', response);
          this.userUpdated.emit();
        });

    }
  }

  toggleNewPasswordVisibility() {
    this.hideNewPassword = !this.hideNewPassword;
  }
}
