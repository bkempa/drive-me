import {Component, EventEmitter, Inject, Output} from '@angular/core';
import {Car} from "../../../../car/car";
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {catchError, throwError} from "rxjs";

@Component({
  selector: 'app-edit-car-dialog',
  templateUrl: './edit-car-dialog.component.html',
  styleUrls: ['./edit-car-dialog.component.css']
})
export class EditCarDialogComponent {

  formControl: FormGroup;
  currentYear: number = new Date().getFullYear();
  @Output() carUpdated = new EventEmitter<Car>();

  constructor(@Inject(MAT_DIALOG_DATA) public car: Car, private formBuilder: FormBuilder, private http: HttpClient) {

    this.formControl = this.formBuilder.group({
      make: [car.make, Validators.required],
      color: [car.color, Validators.required],
      model: [car.model, Validators.required],
      plateNumber: [car.plateNumber, Validators.required],
      productionYear: [car.productionYear, Validators.required],
      maxPassengers: [car.maxPassengers, Validators.required]
    });
  }

  saveCar() {
    if (this.formControl.valid) {
      this.car.make = this.formControl.get('make')!.value;
      this.car.color = this.formControl.get('color')!.value;
      this.car.model = this.formControl.get('model')!.value;
      this.car.plateNumber = this.formControl.get('plateNumber')!.value;
      this.car.productionYear = parseInt(this.formControl.get('productionYear')!.value);
      this.car.maxPassengers = parseInt(this.formControl.get('maxPassengers')!.value);

      this.http.put(`/cars/${this.car.id!}`, this.car)
        .pipe(
          catchError((error) => {
            console.error('Error updating the car:', error);
            return throwError(error);
          }),
        )
        .subscribe((response) => {
          console.log('Car updated successfully:', response);
          this.carUpdated.emit(this.car);
        });
    }
  }
}
