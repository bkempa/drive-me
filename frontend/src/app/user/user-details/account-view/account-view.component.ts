import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {User} from "../../user";
import {Car} from "../../../car/car";
import {EditCarDialogComponent} from "./edit-car-dialog/edit-car-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {MatMenuTrigger} from "@angular/material/menu";
import {
  EditUserDialogComponent
} from "./edit-user-dialog/edit-user-dialog.component";
import {DeleteCarDialogComponent} from "./delete-car-dialog/delete-car-dialog.component";
import {CreateCarDialogComponent} from "./create-car-dialog/create-car-dialog.component";
import {map, Observable, of, Subject, switchMap, takeUntil} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {UserService} from "../../service/user.service";

@Component({
  selector: 'app-account-view',
  templateUrl: './account-view.component.html',
  styleUrls: ['./account-view.component.css']
})
export class AccountViewComponent implements OnInit, OnDestroy {

  private destroy$ = new Subject<void>();
  user$: Observable<User | null> = new Observable<User | null>();
  user: User | null = null;
  carList!: Car[];
  @ViewChild('menuTrigger') menuTrigger!: MatMenuTrigger;

  constructor(public dialog: MatDialog, private router: ActivatedRoute, private userService: UserService) {
  }

  ngOnInit(): void {
    this.user$ = this.router.paramMap.pipe(
      map((params) => params.get('username')),
      switchMap((username) => {
        if (username)
          return this.userService.getUserByUsername(username);
        else
          return of(null);
      })
    );

    this.user$.pipe(takeUntil(this.destroy$)).subscribe((user) => {
      if (user) {
        this.user = user;
        this.carList = user.cars
      }
    });
  }


  addNewCar() {
    const dialogRef = this.dialog.open(CreateCarDialogComponent, {
      id: 'createCarDialogId',
      data: this.user,
      restoreFocus: false,
    });
    dialogRef.componentInstance.carCreated.pipe(takeUntil(this.destroy$)).subscribe(() => {
      dialogRef.close();
      this.refreshCarList();
    });
  }

  editUser() {
    this.user$.pipe(takeUntil(this.destroy$)).subscribe((user) => {
      const dialogRef = this.dialog.open(EditUserDialogComponent, {
        id: 'editUserDialogId',
        data: user,
        restoreFocus: false
      });
      dialogRef.componentInstance.userUpdated.pipe(takeUntil(this.destroy$)).subscribe(() => {
        dialogRef.close();
        this.refreshUserData();
      });
    })
  }

  editCar(car: Car) {
    const dialogRef = this.dialog.open(EditCarDialogComponent, {
      id: 'editCarDialogId',
      data: car,
      restoreFocus: false
    });
    dialogRef.componentInstance.carUpdated.pipe(takeUntil(this.destroy$)).subscribe(() => {
      dialogRef.close();
      this.refreshCarList();
    });
  }

  deleteCar(car: Car) {
    const dialogRef = this.dialog.open(DeleteCarDialogComponent, {
      id: 'deleteCarDialogId',
      data: car,
      restoreFocus: false
    });

    dialogRef.componentInstance.carDeleted.pipe(takeUntil(this.destroy$)).subscribe(() => {
      dialogRef.close();
      this.refreshCarList();
    });
  }

  private refreshCarList() {
    this.user$.pipe(takeUntil(this.destroy$)).subscribe((user) => {
      if (user)
        this.carList = user.cars;
    });
  }

  private refreshUserData() {
    this.user$.pipe(takeUntil(this.destroy$)).subscribe((user) => {
      if (user)
        this.user = user;
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
