import {Component, EventEmitter, Inject, OnDestroy, Output} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {User} from "../../../user";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Car} from "../../../../car/car";
import {AuthService} from "../../../../login/service/auth.service";
import {catchError, Subject, takeUntil, throwError} from "rxjs";
import {MatSnackBar} from "@angular/material/snack-bar";
import {CarService} from "../../../../car/service/car.service";

@Component({
  selector: 'app-create-car-dialog',
  templateUrl: './create-car-dialog.component.html',
  styleUrls: ['./create-car-dialog.component.css']
})
export class CreateCarDialogComponent implements OnDestroy {

  private destroy$ = new Subject<void>();
  formControl: FormGroup;
  currentYear: number = new Date().getFullYear();
  @Output() carCreated = new EventEmitter<void>();

  constructor(@Inject(MAT_DIALOG_DATA) user: User, private formBuilder: FormBuilder, private carService: CarService,
              private authService: AuthService, private _snackBar: MatSnackBar) {

    this.formControl = this.formBuilder.group({
      make: ['', Validators.required],
      color: ['', Validators.required],
      model: ['', Validators.required],
      plateNumber: ['', Validators.required],
      productionYear: ['', Validators.required],
      maxPassengers: ['', Validators.required]
    });
  }

  createNewCar() {
    if (this.formControl.valid) {
      const newCar: Car = {
        make: this.formControl.get('make')!.value,
        color: this.formControl.get('color')!.value,
        model: this.formControl.get('model')!.value,
        plateNumber: this.formControl.get('plateNumber')!.value,
        productionYear: parseInt(this.formControl.get('productionYear')!.value),
        maxPassengers: parseInt(this.formControl.get('maxPassengers')!.value),
      };


      this.authService.getCurrentUserId().pipe(takeUntil(this.destroy$)).subscribe((userId) => {
        if (userId) {
          this.carService.createCar(newCar, userId)
            .pipe(
              takeUntil(this.destroy$),
              catchError((error) => {
                this._snackBar.open("Wystąpił błąd przy tworzeniu samochodu. Upewnij się, że wszystkie wprowadzone dane są poprawne i spróbuj ponownie", "Zamknij", {duration: 3000});
                return throwError(error);
              }),
            )
            .subscribe(() => {
              this._snackBar.open("Stworzono samochód!", "Zamknij", {duration: 3000});
              this.carCreated.emit();
              this.formControl.reset();
            });
        }
      });
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
