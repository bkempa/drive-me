import {Component, EventEmitter, Inject, OnDestroy, Output} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {Car} from "../../../../car/car";
import {catchError, Subject, takeUntil, throwError} from "rxjs";
import {AuthService} from "../../../../login/service/auth.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {CarService} from "../../../../car/service/car.service";

@Component({
  selector: 'app-delete-car-dialog',
  templateUrl: './delete-car-dialog.component.html',
  styleUrls: ['./delete-car-dialog.component.css']
})
export class DeleteCarDialogComponent implements OnDestroy {

  private destroy$ = new Subject<void>();
  @Output() carDeleted = new EventEmitter<void>();

  constructor(@Inject(MAT_DIALOG_DATA) public car: Car, private authService: AuthService, private carService: CarService,
              private _snackBar: MatSnackBar) {}

  deleteCar() {
    this.authService.currentUser$.pipe(takeUntil(this.destroy$)).subscribe((user) => {
      if (user) {
        this.carService.deleteCarFromUser(this.car, user.id)
          .pipe(
            takeUntil(this.destroy$),
            catchError((error) => {
              this._snackBar.open("Wystąpił błąd przy usuwaniu samochodu. Spróbuj ponownie", "Zamknij", {duration: 3000});
              return throwError(error);
            }),
          )
          .subscribe(() => {
            this._snackBar.open("Usunięto samochód!", "Zamknij", {duration: 3000});
            this.carDeleted.emit();
          });
      }
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
