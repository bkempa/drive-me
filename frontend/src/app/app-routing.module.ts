import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {HomeComponent} from "./home/home.component";
import {AdminComponent} from "./admin/admin.component";
import {UserDetailsComponent} from "./user/user-details/user-details.component";
import {userDetailsGuard} from "./guards/user-details/user-details.guard";
import {onlyAdminGuard} from "./guards/only-admin/only-admin.guard";
import {RidesListComponent} from "./rides/rides-list/rides-list.component";
import {RidesArchiveComponent} from "./admin/rides-list-admin/rides-archive/rides-archive.component";
import {AccessDeniedComponent} from "./login/access-denied/access-denied.component";

const routes: Routes = [
  {path: 'rides', component: RidesListComponent},
  {path: 'admin', component: AdminComponent, canActivate: [onlyAdminGuard]},
  // {path: 'admin', loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule)},
  // {path: 'admin', loadComponent: () => import('./admin/admin.module').then(m => m.AdminModule)},
  {path: 'login/:tabIndex', component: LoginComponent},
  {path: 'archive', component: RidesArchiveComponent, canActivate: [onlyAdminGuard]},
  {path: 'profile/:username', component: UserDetailsComponent, canActivate: [userDetailsGuard]},
  {path: '', component: HomeComponent},
  {path: '**', component: AccessDeniedComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
