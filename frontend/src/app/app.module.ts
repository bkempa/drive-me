import {DEFAULT_CURRENCY_CODE, LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {RidesListComponent} from './rides/rides-list/rides-list.component';
import localePl from '@angular/common/locales/pl';
import {registerLocaleData} from "@angular/common";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {RequestInterceptor} from "./request.interceptor";
import {RideReservationComponent} from './rides/ride-reservation/ride-reservation.component';
import {UserRatingComponent} from './user/user-rating/user-rating.component';
import {LoginComponent} from './login/login.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {ToolbarComponent} from './toolbar/toolbar.component';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatCardModule} from '@angular/material/card';
import {MatMenuModule} from '@angular/material/menu';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HomeComponent} from './home/home.component';
import {MatTabsModule} from "@angular/material/tabs";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule} from "@angular/material/core";
import {UserDetailsComponent} from './user/user-details/user-details.component';
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatSortModule} from "@angular/material/sort";
import {MatDialogModule} from "@angular/material/dialog";
import {MatSelectModule} from "@angular/material/select";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {AdminModule} from "./admin/admin.module";
import {NgxMatDatetimePickerModule, NgxMatTimepickerModule} from "@angular-material-components/datetime-picker";
import {NgxMatMomentModule} from "@angular-material-components/moment-adapter";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {AccessDeniedComponent} from './login/access-denied/access-denied.component';
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatRadioModule} from '@angular/material/radio';
import {AccountViewComponent} from './user/user-details/account-view/account-view.component';
import {MyRidesViewComponent} from './user/user-details/my-rides-view/my-rides-view.component';
import {MyReviewsViewComponent} from './user/user-details/my-reviews-view/my-reviews-view.component';
import {
  MyArchivedRidesViewComponent
} from './user/user-details/my-archived-rides-view/my-archived-rides-view.component';
import {
  ReceivedReviewsComponent
} from './user/user-details/my-reviews-view/received-reviews/received-reviews.component';
import {WrittenReviewsComponent} from './user/user-details/my-reviews-view/written-reviews/written-reviews.component';
import {EditRideDialogComponent} from "./user/user-details/my-rides-view/edit-ride-dialog/edit-ride-dialog.component";
import {
  DeleteRideDialogComponent
} from "./user/user-details/my-rides-view/delete-ride-dialog/delete-ride-dialog.component";
import {DeleteCarDialogComponent} from "./user/user-details/account-view/delete-car-dialog/delete-car-dialog.component";
import {EditCarDialogComponent} from "./user/user-details/account-view/edit-car-dialog/edit-car-dialog.component";
import {EditUserDialogComponent} from "./user/user-details/account-view/edit-user-dialog/edit-user-dialog.component";
import {
  EditRatingDialogComponent
} from "./user/user-details/my-reviews-view/written-reviews/edit-rating-dialog/edit-rating-dialog.component";
import {
  DeleteRatingDialogComponent
} from "./user/user-details/my-reviews-view/written-reviews/delete-rating-dialog/delete-rating-dialog.component";
import {MeanRatingPipe} from "./user/user-rating/pipes/mean-rating.pipe";
import {
  MyArchivedRidesAsPassengerComponent
} from './user/user-details/my-archived-rides-view/my-archived-rides-as-passenger/my-archived-rides-as-passenger.component';
import {
  MyArchivedRidesAsDriverComponent
} from './user/user-details/my-archived-rides-view/my-archived-rides-as-driver/my-archived-rides-as-driver.component';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {LeafletModule} from "@asymmetrik/ngx-leaflet";
import { MyRidesAsPassengerComponent } from './user/user-details/my-rides-view/my-rides-as-passenger/my-rides-as-passenger.component';
import { MyRidesAsDriverComponent } from './user/user-details/my-rides-view/my-rides-as-driver/my-rides-as-driver.component';
import { MyRidesAsDriverDialogComponent } from './user/user-details/my-rides-view/my-rides-as-driver/my-rides-as-driver-dialog/my-rides-as-driver-dialog.component';
import { MyRidesAsPassengerDialogComponent } from './user/user-details/my-rides-view/my-rides-as-passenger/my-rides-as-passenger-dialog/my-rides-as-passenger-dialog.component';
import { CreateNewRideDialogComponent } from './rides/create-new-ride-dialog/create-new-ride-dialog.component';
import { CreateCarDialogComponent } from './user/user-details/account-view/create-car-dialog/create-car-dialog.component';
import { RefreshSearchingDialogComponent } from './rides/rides-list/refresh-searching-dialog/refresh-searching-dialog.component';
import { CreateRatingDialogComponent } from './user/user-details/my-archived-rides-view/my-archived-rides-as-passenger/create-rating-dialog/create-rating-dialog.component';
import { SignInComponent } from './login/sign-in/sign-in.component';
import { RegisterComponent } from './login/register/register.component';

registerLocaleData(localePl);

@NgModule({
  declarations: [
    AppComponent,
    RidesListComponent,
    RideReservationComponent,
    UserRatingComponent,
    LoginComponent,
    ToolbarComponent,
    HomeComponent,
    UserDetailsComponent,
    AccessDeniedComponent,
    AccountViewComponent,
    MyRidesViewComponent,
    MyReviewsViewComponent,
    MyArchivedRidesViewComponent,
    ReceivedReviewsComponent,
    WrittenReviewsComponent,
    EditRideDialogComponent,
    DeleteRideDialogComponent,
    DeleteCarDialogComponent,
    EditCarDialogComponent,
    DeleteRatingDialogComponent,
    EditRatingDialogComponent,
    EditUserDialogComponent,
    MeanRatingPipe,
    MyArchivedRidesAsPassengerComponent,
    MyArchivedRidesAsDriverComponent,
    MyRidesAsPassengerComponent,
    MyRidesAsDriverComponent,
    MyRidesAsDriverDialogComponent,
    MyRidesAsPassengerDialogComponent,
    CreateNewRideDialogComponent,
    CreateCarDialogComponent,
    RefreshSearchingDialogComponent,
    CreateRatingDialogComponent,
    SignInComponent,
    RegisterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    FormsModule,
    MatTabsModule,
    MatTooltipModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatMenuModule,
    MatDialogModule,
    MatSelectModule,
    MatCheckboxModule,
    AdminModule,
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    MatSnackBarModule,
    NgxMatMomentModule,
    MatSidenavModule,
    MatRadioModule,
    MatProgressSpinnerModule,
    LeafletModule
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'pl'
    },
    {
      provide: DEFAULT_CURRENCY_CODE,
      useValue: 'PLN'
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
