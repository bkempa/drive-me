import { TestBed } from '@angular/core/testing';
import { CanActivateFn } from '@angular/router';

import { userDetailsGuard } from './user-details.guard';

describe('userDetailsGuard', () => {
  const executeGuard: CanActivateFn = (...guardParameters) => 
      TestBed.runInInjectionContext(() => userDetailsGuard(...guardParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeGuard).toBeTruthy();
  });
});
