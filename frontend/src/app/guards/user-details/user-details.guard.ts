import {CanActivateFn, Router} from '@angular/router';
import {inject} from "@angular/core";
import {AuthService} from "../../login/service/auth.service";
import {map, take} from "rxjs";
import {Role} from "../../user/user";

export const userDetailsGuard: CanActivateFn = (route, state) => {
  const router = inject(Router);
  return inject(AuthService).currentUser$.pipe(
    take(1),
    map((user) => {
      if (!user) {
        router.navigate(['/login/0']);
        return false;
      }

      const usernameParam = state.url.split('/').pop();
      if (user.username === usernameParam || user.role === Role.ADMIN)
        return true;
      else {
        router.navigate(['/access-denied']);
        return false;
      }
    })
  );
};
