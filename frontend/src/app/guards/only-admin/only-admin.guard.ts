import {CanActivateFn, Router} from '@angular/router';
import {inject} from "@angular/core";
import {AuthService} from "../../login/service/auth.service";
import {map, take} from "rxjs";
import {Role} from "../../user/user";

export const onlyAdminGuard: CanActivateFn = (route, state) => {
  const router = inject(Router);
  return inject(AuthService).currentUser$.pipe(
    take(1),
    map((user) => {
      if (!user) {
        router.navigate(['/login/0']);
        return false;
      }

      if (user.role === Role.ADMIN)
        return true;
      else {
        router.navigate(['/access-denied']);
        return false;
      }
    })
  );
};
