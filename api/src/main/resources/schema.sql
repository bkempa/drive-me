CREATE TABLE IF NOT EXISTS car
(
    id              SERIAL      NOT NULL,
    make            varchar(25) NOT NULL,
    model           varchar(20) NOT NULL,
    color           varchar(25) NOT NULL,
    plate_number    varchar(10) NOT NULL UNIQUE,
    production_year smallint    NOT NULL,
    max_passengers  smallint,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS app_user
(
    id               SERIAL                NOT NULL,
    username         varchar(45)           NOT NULL,
    password         varchar(100)          NOT NULL,
    first_name       varchar(45)           NOT NULL,
    last_name        varchar(45)           NOT NULL,
    date_of_birth    date                  NOT NULL,
    phone_number     varchar(12)           NOT NULL,
    email_address    varchar(45)           NOT NULL UNIQUE,
    role             varchar(5)            NOT NULL,
    blocked          boolean DEFAULT false NOT NULL,
    unblock_datetime timestamp,
    PRIMARY KEY (id)
);
CREATE UNIQUE INDEX IF NOT EXISTS app_user_username
    ON app_user (username);
CREATE INDEX IF NOT EXISTS app_user_email_address
    ON app_user (email_address);

CREATE TABLE IF NOT EXISTS ride
(
    id               SERIAL           NOT NULL,
    starting_time    timestamp        NOT NULL,
    places_available smallint         NOT NULL,
    car_id           SERIAL           NOT NULL,
    driver_id        SERIAL           NOT NULL,
    price            integer          NOT NULL,
    start_location   geography(POINT) NOT NULL,
    end_location     geography(POINT) NOT NULL,
    description      varchar(200),
    PRIMARY KEY (id),
    FOREIGN KEY (car_id) REFERENCES car (id),
    FOREIGN KEY (driver_id) REFERENCES app_user (id)
);
CREATE INDEX IF NOT EXISTS ride_starting_time
    ON ride (starting_time);
CREATE INDEX IF NOT EXISTS ride_start_location
    ON ride USING gist (start_location);
CREATE INDEX IF NOT EXISTS ride_end_location
    ON ride USING gist (end_location);

CREATE TABLE IF NOT EXISTS app_user_ride
(
    user_id SERIAL NOT NULL,
    ride_id SERIAL NOT NULL,
    PRIMARY KEY (user_id, ride_id),
    FOREIGN KEY (user_id) REFERENCES app_user (id),
    FOREIGN KEY (ride_id) REFERENCES ride (id)
);

CREATE TABLE IF NOT EXISTS app_user_car
(
    app_user_id SERIAL NOT NULL,
    car_id      SERIAL NOT NULL,
    PRIMARY KEY (app_user_id, car_id),
    FOREIGN KEY (app_user_id) REFERENCES app_user (id),
    FOREIGN KEY (car_id) REFERENCES car (id)
);

CREATE TABLE IF NOT EXISTS app_user_rating
(
    driver_id        SERIAL   NOT NULL,
    passenger_id     SERIAL   NOT NULL,
    rate             smallint NOT NULL,
    comment          varchar(200),
    rating_datetime  timestamp,
    edition_datetime timestamp,
    PRIMARY KEY (driver_id, passenger_id),
    FOREIGN KEY (driver_id) REFERENCES app_user (id),
    FOREIGN KEY (passenger_id) REFERENCES app_user (id)
);

CREATE TABLE IF NOT EXISTS archived_ride
(
    id             SERIAL           NOT NULL,
    starting_time  timestamp        NOT NULL,
    car_id         SERIAL,
    driver_id      SERIAL,
    price          integer          NOT NULL,
    start_location geography(POINT) NOT NULL,
    end_location   geography(POINT) NOT NULL,
    description    varchar(200),
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS app_user_archived_ride
(
    user_id          SERIAL NOT NULL,
    archived_ride_id SERIAL NOT NULL,
    PRIMARY KEY (user_id, archived_ride_id),
    FOREIGN KEY (user_id) REFERENCES app_user (id),
    FOREIGN KEY (archived_ride_id) REFERENCES archived_ride (id)
);

CREATE TABLE IF NOT EXISTS ride_reservation
(
    ride_id              SERIAL   NOT NULL,
    passenger_id         SERIAL   NOT NULL,
    number_of_passengers smallint NOT NULL,
    PRIMARY KEY (ride_id, passenger_id),
    FOREIGN KEY (ride_id) REFERENCES ride (id),
    FOREIGN KEY (passenger_id) REFERENCES app_user (id)
);