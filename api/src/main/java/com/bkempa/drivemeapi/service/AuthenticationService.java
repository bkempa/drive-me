package com.bkempa.drivemeapi.service;

import com.bkempa.drivemeapi.config.JwtService;
import com.bkempa.drivemeapi.exception.RegisteredEmailException;
import com.bkempa.drivemeapi.exception.UserBlockedException;
import com.bkempa.drivemeapi.exception.UserNotFoundException;
import com.bkempa.drivemeapi.exception.UsernameExistsException;
import com.bkempa.drivemeapi.model.AuthenticationRequest;
import com.bkempa.drivemeapi.model.AuthenticationResponse;
import com.bkempa.drivemeapi.model.RegisterRequest;
import com.bkempa.drivemeapi.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    public AuthenticationResponse register(RegisterRequest request) throws RegisteredEmailException, UsernameExistsException {
        User user = User.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .username(request.getUsername())
                .emailAddress(request.getEmailAddress())
                .password(passwordEncoder.encode(request.getPassword()))
                .dateOfBirth(request.getDateOfBirth())
                .phoneNumber(request.getPhoneNumber())
                .role(request.getRole())
                .build();
        userService.createUser(user);
        String jwtToken = jwtService.generateToken(user);
        return AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
    }

    public AuthenticationResponse authenticate(AuthenticationRequest request) throws UserNotFoundException, UserBlockedException {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getUsername(),
                        request.getPassword()
                )
        );
        User user = userService.getUserByUsername(request.getUsername());
        if (user.isBlocked()) {
            ZonedDateTime unblockDateTime = user.getUnblockDateTime();
            StringBuilder message = new StringBuilder("User " + user.getUsername() + " is blocked ");
            if (unblockDateTime == null)
                message.append("permanently");
            else
                message.append("until ").append(unblockDateTime);
            throw new UserBlockedException(message.toString());
        }
        String jwtToken = jwtService.generateToken(user);
        return AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
    }
}
