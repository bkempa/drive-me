package com.bkempa.drivemeapi.service;

import com.bkempa.drivemeapi.model.ArchivedRide;
import com.bkempa.drivemeapi.repository.ArchivedRideRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ArchivedRideService {
    private final ArchivedRideRepository archivedRideRepository;

    @Scheduled(cron = "* */1 * * * *")
    @Transactional
    public void deleteRidesFromArchiveAfterYear() {
        archivedRideRepository.deleteByStartingTimeBefore(LocalDateTime.now().minusYears(1).atZone(ZoneId.of("Europe/Warsaw")));
    }

    public List<ArchivedRide> getAllArchivedRides() {
        return archivedRideRepository.findAll();
    }

    public List<ArchivedRide> getAllArchivedRidesByDriverId(Long driverId) {
        return archivedRideRepository.findByDriverId(driverId);
    }

    public void addRideToArchive(ArchivedRide ride) {
        archivedRideRepository.save(ride);
    }

    public List<ArchivedRide> getAllArchivedRidesByPassengerId(Long passengerId) {
        return archivedRideRepository.findByPassengersId(passengerId);
    }
}
