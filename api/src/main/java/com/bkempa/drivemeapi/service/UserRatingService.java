package com.bkempa.drivemeapi.service;

import com.bkempa.drivemeapi.exception.RatingNotFoundException;
import com.bkempa.drivemeapi.exception.UserNotFoundException;
import com.bkempa.drivemeapi.model.User;
import com.bkempa.drivemeapi.model.UserRating;
import com.bkempa.drivemeapi.repository.UserRatingRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserRatingService {

    private final UserRatingRepository userRatingRepository;
    private final UserService userService;

    public List<UserRating> getAllReceivedRatingsByUsername(String username) {
        return userRatingRepository.findAllByDriver_UsernameOrderByRatingDateTimeAsc(username);
    }

    public List<UserRating> getAllWrittenRatingsByUsername(String username) {
        return userRatingRepository.findAllByPassenger_UsernameOrderByRatingDateTimeAsc(username);
    }

    public List<UserRating> getAllReceivedRatingsByUserId(Long id) {
        return userRatingRepository.findAllByDriver_IdOrderByRatingDateTimeAsc(id);
    }

    public List<UserRating> getAllWrittenRatingsByUserId(Long id) {
        return userRatingRepository.findAllByPassenger_IdOrderByRatingDateTimeAsc(id);
    }

    public UserRating getRatingById(Long id) throws RatingNotFoundException {
        return userRatingRepository.findById(id)
                .orElseThrow(() -> new RatingNotFoundException("Rating with this ID does not exist"));
    }

    @Transactional
    public void postRating(UserRating rating) throws UserNotFoundException {
        User author = userService.getUserById(rating.getId().getPassengerId());
        User driver = userService.getUserById(rating.getId().getDriverId());
        rating.setRatingDateTime(LocalDateTime.now().atZone(ZoneId.of("Europe/Warsaw")));
        author.addWrittenRate(rating);
        driver.addReceivedRate(rating);
        userRatingRepository.save(rating);
    }

    @Transactional
    public void updateRating(UserRating updatedRating) throws RatingNotFoundException {
        Optional<UserRating> rating = userRatingRepository
                .findById_DriverIdAndId_PassengerId(updatedRating.getId().getDriverId(), updatedRating.getId().getPassengerId());
        if (rating.isEmpty())
            throw new RatingNotFoundException("Rating not found");

        UserRating userRating = rating.get();
        userRating.setRate(updatedRating.getRate());
        userRating.setComment(updatedRating.getComment());
        userRating.setEditionDateTime(LocalDateTime.now().atZone(ZoneId.of("Europe/Warsaw")));
        userRatingRepository.save(userRating);
    }

    public List<UserRating> getAllRatings() {
        return userRatingRepository.findAll();
    }

    @Transactional
    public void removeRating(Long authorId, Long driverId) throws RatingNotFoundException {
        Optional<UserRating> rating = userRatingRepository.findById_DriverIdAndId_PassengerId(driverId, authorId);
        if (rating.isEmpty())
            throw new RatingNotFoundException("Rating not found");

        UserRating userRating = rating.get();
        userRating.getDriver().removeReceivedRate(userRating);
        userRating.getPassenger().removeWrittenRate(userRating);
        userRatingRepository.delete(userRating);
    }
}
