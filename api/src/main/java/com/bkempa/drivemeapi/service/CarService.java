package com.bkempa.drivemeapi.service;

import com.bkempa.drivemeapi.exception.CarNotFoundException;
import com.bkempa.drivemeapi.exception.UserNotFoundException;
import com.bkempa.drivemeapi.model.ArchivedRide;
import com.bkempa.drivemeapi.model.Car;
import com.bkempa.drivemeapi.model.User;
import com.bkempa.drivemeapi.repository.ActiveRideRepository;
import com.bkempa.drivemeapi.repository.CarRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CarService {

    private final CarRepository carRepository;
    private final UserService userService;
    private final ActiveRideRepository activeRideRepository;

    public List<Car> getAllCars() {
        return carRepository.findAll();
    }

    @Transactional
    public void addCar(Long userId, Car newCar) throws UserNotFoundException {
        User user = userService.getUserById(userId);
        Optional<Car> car = carRepository.findByPlateNumber(newCar.getPlateNumber());

        if (car.isEmpty()) {
            newCar.setPlateNumber(newCar.getPlateNumber().trim().replaceAll("\\s", ""));
            user.addCar(newCar);
            carRepository.save(newCar);
        } else {
            Car existingCar = car.get();
            user.addCar(existingCar);
            carRepository.save(existingCar);
        }
    }

    public void deleteCarById(Long id) throws CarNotFoundException {
        Car car = carRepository.findById(id)
                .orElseThrow(() -> new CarNotFoundException("Car with this ID does not exist"));

        activeRideRepository.deleteAll(car.getRides());
        car.getRides().clear();

        for (User owner : car.getOwners())
            owner.removeCar(car);

        for (ArchivedRide archivedRide : car.getArchivedRides())
            archivedRide.setCar(null);

        carRepository.delete(car);
    }

    public void deleteCarByPlateNumber(String plateNumber) throws CarNotFoundException {
        Car car = carRepository.findByPlateNumber(plateNumber.trim().replaceAll("\\s", ""))
                .orElseThrow(() -> new CarNotFoundException("Car with this plate number does not exist"));
        carRepository.delete(car);
    }

    public List<Car> getAllCarsByMinimumProductionYear(int year) {
        return carRepository.findByProductionYearGreaterThanEqual(year);
    }

    public Car getCarById(Long id) throws CarNotFoundException {
        return carRepository.findById(id).orElseThrow(() -> new CarNotFoundException("Car with this ID does not exist"));
    }

    public Car getCarByPlateNumber(String plateNumber) throws CarNotFoundException {
        return carRepository.findByPlateNumber(plateNumber.trim().replaceAll("\\s", ""))
                .orElseThrow(() -> new CarNotFoundException("Car with this plate number does not exist"));
    }


    @Transactional
    public void updateCarById(Long id, Car car) throws CarNotFoundException {
        Car carToUpdate = carRepository.findById(id)
                .orElseThrow(() -> new CarNotFoundException("Car with this ID does not exist"));
        if (!carToUpdate.getMake().equals(car.getMake().trim()))
            carToUpdate.setMake(car.getMake().trim());

        if (!carToUpdate.getModel().equals(car.getModel().trim()))
            carToUpdate.setModel(car.getModel().trim());

        if (!carToUpdate.getColor().equals(car.getColor().trim()))
            carToUpdate.setColor(car.getColor().trim());

        if (!carToUpdate.getPlateNumber().equals(car.getPlateNumber().trim().replaceAll("\\s", ""))
                && carRepository.existsByPlateNumber(car.getPlateNumber().replaceAll("\\s", "")))
            carToUpdate.setPlateNumber(car.getPlateNumber().trim().replaceAll("\\s", ""));

        if (carToUpdate.getProductionYear() != car.getProductionYear())
            carToUpdate.setProductionYear(car.getProductionYear());

        if (carToUpdate.getMaxPassengers() != car.getMaxPassengers())
            carToUpdate.setMaxPassengers(car.getMaxPassengers());

        carRepository.save(carToUpdate);
    }

    @Transactional
    public void removeCarFromUser(Long userId, Long carId) throws UserNotFoundException, CarNotFoundException {
        User user = userService.getUserById(userId);
        Car car = this.getCarById(carId);
        user.removeCar(car);
        if (car.getOwners().isEmpty())
            carRepository.delete(car);
        else
            carRepository.save(car);
    }

    public List<Car> getUserCars(Long userId) {
        return carRepository.findCarsByOwnerId(userId);
    }
}
