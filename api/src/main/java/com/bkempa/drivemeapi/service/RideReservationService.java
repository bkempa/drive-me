package com.bkempa.drivemeapi.service;

import com.bkempa.drivemeapi.exception.ReservationNotFoundException;
import com.bkempa.drivemeapi.exception.RideNotAvailableException;
import com.bkempa.drivemeapi.model.RideReservation;
import com.bkempa.drivemeapi.repository.RideReservationRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RideReservationService {

    private final RideReservationRepository rideReservationRepository;

    public List<RideReservation> getAllReservations() {
        return rideReservationRepository.findAll();
    }

    public RideReservation getReservationByRideIdAndPassengerId(Long rideId, Long passengerId) throws ReservationNotFoundException {
        return rideReservationRepository.findById_RideIdAndId_PassengerId(rideId, passengerId)
                .orElseThrow(() -> new ReservationNotFoundException("Reservation not found"));
    }

    @Transactional
    public void addRideReservation(RideReservation reservation) throws RideNotAvailableException {
        reservation.getPassenger().addRideAsPassenger(reservation);
        reservation.getRide().addReservation(reservation);
        rideReservationRepository.save(reservation);
    }

    public void deleteRideReservation(RideReservation reservation) {
        reservation.getRide().removeReservation(reservation);
        rideReservationRepository.delete(reservation);
    }

    public void deleteRideReservationByIds(Long rideId, Long passengerId) throws ReservationNotFoundException {
        RideReservation reservation = this.getReservationByRideIdAndPassengerId(rideId, passengerId);
        reservation.getRide().removeReservation(reservation);
        reservation.getPassenger().removeRideAsPassenger(reservation);
        rideReservationRepository.delete(reservation);
    }

    public List<RideReservation> getRideReservationsByPassengerId(Long passengerId) {
        return rideReservationRepository.findById_PassengerId(passengerId);
    }
}
