package com.bkempa.drivemeapi.service;

import com.bkempa.drivemeapi.exception.CarNotFoundException;
import com.bkempa.drivemeapi.exception.RideNotFoundException;
import com.bkempa.drivemeapi.exception.UserNotFoundException;
import com.bkempa.drivemeapi.model.ActiveRide;
import com.bkempa.drivemeapi.model.Car;
import com.bkempa.drivemeapi.model.RideReservation;
import com.bkempa.drivemeapi.model.User;
import com.bkempa.drivemeapi.repository.UserRepository;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Set;

@Service
@AllArgsConstructor
public class UserDeletionHandleService {
    private final UserRepository userRepository;
    private final CarService carService;
    private ActiveRideService activeRideService;
    private RideReservationService rideReservationService;

    private void deleteUserRidesInGivenTime(User user, ZonedDateTime maxDate) throws RideNotFoundException {
        List<ActiveRide> activeRides = activeRideService.getRidesByDriverId(user.getId());
        for (ActiveRide activeRide : activeRides) {
            if (activeRide.getStartingTime().isBefore(maxDate))
                activeRideService.deleteRideById(activeRide.getId());
        }

        List<RideReservation> reservations = rideReservationService.getRideReservationsByPassengerId(user.getId());
        for (RideReservation reservation : reservations) {
            if (reservation.getRide().getStartingTime().isBefore(maxDate))
                rideReservationService.deleteRideReservation(reservation);
        }
    }

    @Transactional
    public void deleteUser(User user) throws CarNotFoundException {
        this.deleteAllUserCars(user);
        userRepository.delete(user);
    }

    private void deleteAllFutureRides(User user) throws RideNotFoundException {
        List<ActiveRide> activeRides = activeRideService.getRidesByDriverId(user.getId());
        for (ActiveRide activeRide : activeRides)
            activeRideService.deleteRideById(activeRide.getId());

        List<RideReservation> reservations = rideReservationService.getRideReservationsByPassengerId(user.getId());
        for (RideReservation reservation : reservations)
            rideReservationService.deleteRideReservation(reservation);
    }

    private void deleteAllUserCars(User user) throws CarNotFoundException {
        Set<Car> cars = user.getCars();
        for (Car car : cars) {
            if (car.getOwners().size() > 1)
                user.removeCar(car);
            else
                carService.deleteCarById(car.getId());
        }
    }

    @Transactional
    public void deleteUserById(Long id) throws UserNotFoundException, CarNotFoundException {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("User with this ID does not exist"));

        this.deleteUser(user);
    }

    @Transactional
    public void blockUserById(Long id, int daysBlocked) throws UserNotFoundException, RideNotFoundException {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("User with this ID does not exist"));

        ZonedDateTime currentDate = LocalDateTime.now().atZone(ZoneId.of("Europe/Warsaw"));
        ZonedDateTime unblockDate = currentDate.plusDays(daysBlocked);

        if (!user.isBlocked()) {
            user.setBlocked(true);
            user.setUnblockDateTime(unblockDate);
            this.deleteUserRidesInGivenTime(user, unblockDate);
            userRepository.save(user);
        }
    }

    @Transactional
    public void blockUserForeverById(Long id) throws UserNotFoundException, RideNotFoundException {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("User with this ID does not exist"));

        if (!user.isBlocked()) {
            user.setBlocked(true);
            user.setUnblockDateTime(null);
            this.deleteAllFutureRides(user);
            userRepository.save(user);
        }
    }

    @Transactional
    public void unblockUserById(Long id) throws UserNotFoundException {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("User with this ID does not exist"));

        if (user.isBlocked()) {
            user.setBlocked(false);
            user.setUnblockDateTime(null);
            userRepository.save(user);
        }
    }

    @Transactional
    public void blockUser(User user, int daysBlocked) throws RideNotFoundException {
        if (!user.isBlocked()) {
            ZonedDateTime unblockDate = LocalDateTime.now().atZone(ZoneId.of("Europe/Warsaw")).plusDays(daysBlocked);
            user.setBlocked(true);
            user.setUnblockDateTime(unblockDate);
            this.deleteUserRidesInGivenTime(user, unblockDate);
            userRepository.save(user);
        }
    }

    @Transactional
    public void blockUserForever(User user) throws RideNotFoundException {
        if (!user.isBlocked()) {
            user.setBlocked(true);
            user.setUnblockDateTime(null);
            this.deleteAllFutureRides(user);
            userRepository.save(user);
        }
    }

    @Transactional
    public void unblockUser(User user) {
        if (user.isBlocked()) {
            user.setBlocked(false);
            user.setUnblockDateTime(null);
            userRepository.save(user);
        }
    }

    @Scheduled(cron = "0 */1 * * * *")
    public void unblockUsersScheduled() {
        List<User> blockedActiveUsers = userRepository
                .findByBlockedAndUnblockDateTimeIsBefore(true, LocalDateTime.now().atZone(ZoneId.of("Europe/Warsaw")));
        blockedActiveUsers.forEach(this::unblockUser);
    }
}
