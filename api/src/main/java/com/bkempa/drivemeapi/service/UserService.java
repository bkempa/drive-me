package com.bkempa.drivemeapi.service;

import com.bkempa.drivemeapi.exception.RegisteredEmailException;
import com.bkempa.drivemeapi.exception.UserNotFoundException;
import com.bkempa.drivemeapi.exception.UsernameExistsException;
import com.bkempa.drivemeapi.model.User;
import com.bkempa.drivemeapi.repository.UserRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public void createUser(User user) throws RegisteredEmailException, UsernameExistsException {
        Optional<User> byUsername = userRepository.findByUsername(user.getUsername());
        Optional<User> byEmailAddress = userRepository.findByEmailAddress(user.getEmailAddress());
        if (byUsername.isPresent())
            throw new UsernameExistsException("This username is not available");
        else if (byEmailAddress.isPresent())
            throw new RegisteredEmailException("An account for this e-mail already exists");
        else
            userRepository.save(user);
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public User getUserById(Long id) throws UserNotFoundException {
        return userRepository.findById(id).orElseThrow(() -> new UserNotFoundException("User not found"));
    }

    public User getUserByUsername(String username) throws UserNotFoundException {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new UserNotFoundException("User ".concat(username).concat(" does not exist")));
    }

    public User getUserByEmailAddress(String email) throws UserNotFoundException {
        return userRepository.findByEmailAddress(email)
                .orElseThrow(() -> new UserNotFoundException("User with ".concat(email).concat(" e-mail address does not exist")));
    }

    @Transactional
    public void updateUser(Long id, User updatedUser) throws UserNotFoundException {
        User existingUser = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("User with this ID does not exist"));

        if (updatedUser.getPassword() != null)
            existingUser.setPassword(passwordEncoder.encode(updatedUser.getPassword()));
        existingUser.setFirstName(updatedUser.getFirstName().trim());
        existingUser.setLastName(updatedUser.getLastName().trim());
        existingUser.setPhoneNumber(updatedUser.getPhoneNumber().trim());
        existingUser.setEmailAddress(updatedUser.getEmailAddress().trim());

        userRepository.save(existingUser);
    }

    @Transactional
    public void grantAdminRightsToUserById(Long id) throws UserNotFoundException {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("User with this ID does not exist"));

        user.setRole(User.Role.ADMIN);
        userRepository.save(user);
    }

    @Transactional
    public void revokeAdminRightsToUserById(Long id) throws UserNotFoundException {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("User with this ID does not exist"));

        user.setRole(User.Role.USER);
        userRepository.save(user);
    }

    public void grantAdminRightsToUser(User user) {
        user.setRole(User.Role.ADMIN);
        userRepository.save(user);
    }

    public void revokeAdminRightsToUser(User user) {
        user.setRole(User.Role.USER);
        userRepository.save(user);
    }
}
