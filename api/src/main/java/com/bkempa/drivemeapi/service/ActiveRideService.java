package com.bkempa.drivemeapi.service;

import com.bkempa.drivemeapi.exception.CarNotFoundException;
import com.bkempa.drivemeapi.exception.RideNotFoundException;
import com.bkempa.drivemeapi.exception.UserNotFoundException;
import com.bkempa.drivemeapi.model.*;
import com.bkempa.drivemeapi.repository.ActiveRideRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.PrecisionModel;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ActiveRideService {

    private final ActiveRideRepository activeRideRepository;
    private final UserService userService;
    private final CarService carService;
    private final ArchivedRideService archivedRideService;
    private final RideReservationService rideReservationService;

    public List<ActiveRide> getAllRides() {
        return activeRideRepository.findAll();
    }

    public ActiveRide getRideById(Long id) throws RideNotFoundException {
        return activeRideRepository.findById(id)
                .orElseThrow(() -> new RideNotFoundException("Ride with this ID does not exist"));
    }

    public void deleteRideById(Long id) throws RideNotFoundException {
        ActiveRide ride = activeRideRepository.findById(id)
                .orElseThrow(() -> new RideNotFoundException("Ride with this ID does not exist"));
        activeRideRepository.delete(ride);
    }

    @Transactional
    public void addRideAsDriver(ActiveRide ride) throws UserNotFoundException, CarNotFoundException {
        User driver = userService.getUserById(ride.getDriver().getId());
        Car car = carService.getCarById(ride.getCar().getId());
        ride.setDriver(driver);
        ride.setCar(car);
        driver.addRideAsDriver(ride);
        car.getRides().add(ride);
        activeRideRepository.save(ride);
    }

    public void removeRideAsDriver(Long driverId, Long rideId) throws UserNotFoundException, RideNotFoundException {
        User driver = userService.getUserById(driverId);
        ActiveRide ride = this.getRideById(rideId);
        driver.removeRideAsDriver(ride);
        activeRideRepository.delete(ride);
    }

    public List<ActiveRide> getRidesWithStartingTimeAfter(ZonedDateTime startingTime) {
        return activeRideRepository.findByStartingTimeAfter(startingTime);
    }

    public List<ActiveRide> getRidesWithMinimumPlacesAvailable(int minimumExpectedPlaces) {
        return activeRideRepository.findByPlacesAvailableGreaterThanEqual(minimumExpectedPlaces);
    }

    public List<ActiveRide> getRidesByCarId(Long carId) {
        return activeRideRepository.findByCarId(carId);
    }

    public List<ActiveRide> getRidesByDriverId(Long driverId) {
        return activeRideRepository.findByDriverId(driverId);
    }

    public List<ActiveRide> getRidesByMaximumPrice(int maxPrice) {
        return activeRideRepository.findByPriceLessThanEqual(maxPrice);
    }

    public List<ActiveRide> getRidesByStartLocationWithinDistance(double startLocationLatitude, double startLocationLongitude, int distanceInMeters) {
        GeometryFactory factory = new GeometryFactory(new PrecisionModel(), 4326);
        Point point = factory.createPoint(new Coordinate(startLocationLongitude, startLocationLatitude));
        return activeRideRepository.findRidesStartingWithinDistance(point, distanceInMeters);
    }

    public List<ActiveRide> getRidesByEndLocationWithinDistance(double endLocationLatitude, double endLocationLongitude, int distanceInMeters) {
        GeometryFactory factory = new GeometryFactory(new PrecisionModel(), 4326);
        Point point = factory.createPoint(new Coordinate(endLocationLongitude, endLocationLatitude));
        return activeRideRepository.findRidesEndingWithinDistance(point, distanceInMeters);
    }

    public List<ActiveRide> getRidesByStartAndEndLocationWithinDistance(double startLocationLatitude, double startLocationLongitude,
                                                                        double endLocationLatitude, double endLocationLongitude,
                                                                        int distanceInMeters) {
        GeometryFactory factory = new GeometryFactory(new PrecisionModel(), 4326);
        Point start = factory.createPoint(new Coordinate(startLocationLongitude, startLocationLatitude));
        Point end = factory.createPoint(new Coordinate(endLocationLongitude, endLocationLatitude));
        return activeRideRepository.findRidesStartingAndEndingWithinDistance(start, end, distanceInMeters);
    }

    @Transactional
    public void updateRideById(Long id, ActiveRide updatedRide) throws RideNotFoundException {
        ActiveRide ride = activeRideRepository.findById(id)
                .orElseThrow(() -> new RideNotFoundException("Ride with this ID does not exist"));

        if (updatedRide.getStartingTime() != null)
            ride.setStartingTime(updatedRide.getStartingTime());

        if (updatedRide.getPlacesAvailable() != 0)
            ride.setPlacesAvailable(updatedRide.getPlacesAvailable());

        if (updatedRide.getCar() != null)
            ride.setCar(updatedRide.getCar());

        if (updatedRide.getDriver() != null)
            ride.setDriver(updatedRide.getDriver());

        if (updatedRide.getReservations() != null)
            ride.setReservations(updatedRide.getReservations());

        if (updatedRide.getPrice() != 0)
            ride.setPrice(updatedRide.getPrice());

        if (!updatedRide.getStartLocation().isEmpty())
            ride.setStartLocation(updatedRide.getStartLocation());

        if (!updatedRide.getEndLocation().isEmpty())
            ride.setEndLocation(updatedRide.getEndLocation());

        if (updatedRide.getDescription() != null)
            ride.setDescription(updatedRide.getDescription());

        activeRideRepository.save(ride);
    }

    @Scheduled(cron = "* */1 * * * *")
    @Transactional
    public void movePastRidesToArchive() {
        ZonedDateTime currentDateTime = ZonedDateTime.now(ZoneId.of("Europe/Warsaw"));
        List<ActiveRide> pastRides = activeRideRepository.findByStartingTimeBefore(currentDateTime);

        for (ActiveRide pastRide : pastRides) {
            ArchivedRide rideToArchive = new ArchivedRide(pastRide);
            archivedRideService.addRideToArchive(rideToArchive);

            User driver = pastRide.getDriver();
            driver.addArchivedRideAsDriver(rideToArchive);

            Set<RideReservation> reservationsCopy = new HashSet<>(pastRide.getReservations());
            for (RideReservation reservation : reservationsCopy) {
                User passenger = reservation.getPassenger();
                passenger.addArchivedRideAsPassenger(rideToArchive);
                rideReservationService.deleteRideReservation(reservation);
                pastRide.removeReservation(reservation);
            }
        }
        activeRideRepository.deleteAll(pastRides);
    }

    public List<ActiveRide> getRidesWithStartingTimeBetween(ZonedDateTime minTime, ZonedDateTime maxTime) {
        return activeRideRepository.findByStartingTimeAfterAndStartingTimeBefore(minTime, maxTime);
    }

    public List<ActiveRide> getRidesByPassengerId(Long passengerId) {
        List<RideReservation> reservations = this.rideReservationService.getRideReservationsByPassengerId(passengerId);

        return reservations.stream()
                .map(RideReservation::getRide)
                .collect(Collectors.toList());
    }
}
