package com.bkempa.drivemeapi.model;

import com.bkempa.drivemeapi.validation.ValidCarProductionYear;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "car")
@Getter
@Setter
@NoArgsConstructor
public class Car implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String make;

    @Column(nullable = false)
    private String model;

    @Column(nullable = false)
    private String color;

    @Column(name = "plate_number", nullable = false)
    private String plateNumber;

    @Column(name = "production_year", nullable = false)
    @ValidCarProductionYear(message = "Production year is invalid")
    private int productionYear;

    @Column(name = "max_passengers")
    @Min(value = 1, message = "You have to be able to get at least one passenger")
    @Max(value = 8, message = "Number of passengers to high for a passenger car")
    private int maxPassengers;

    @OneToMany(mappedBy = "car", cascade = CascadeType.REMOVE)
    @JsonIgnore
    private Set<ActiveRide> rides = new HashSet<>();

    @OneToMany(mappedBy = "car")
    @JsonIgnore
    private Set<ArchivedRide> archivedRides = new HashSet<>();

    @ManyToMany(mappedBy = "cars")
    @JsonIgnore
    private Set<User> owners = new HashSet<>();
}
