package com.bkempa.drivemeapi.model;

import com.bkempa.drivemeapi.validation.DateOfBirth;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity(name = "app_user")
@NoArgsConstructor
@Builder
@AllArgsConstructor
@Data
public class User implements UserDetails, Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    @Pattern(regexp = "(?=^.{8,}$)(?=.*\\d)(?=.*[!@#$%^&*]+)(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*$",
            message = "The password must contain at least 8 characters, 1 uppercase and lowercase letter, a digit, and a special character")
    private String password;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "date_of_birth", nullable = false)
    @DateOfBirth
    private LocalDate dateOfBirth;

    @Column(name = "phone_number", nullable = false)
    @Pattern(regexp = "^(?:\\+?[0-9]([- ]?[0-9])*)?$", message = "Enter a valid phone number")
    private String phoneNumber;

    @Column(name = "email_address", nullable = false)
    @Email(message = "Enter a valid email address")
    private String emailAddress;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Role role;

    public enum Role {
        ADMIN, USER
    }

    @Column(nullable = false)
    private boolean blocked = false;

    @OneToMany(mappedBy = "driver", cascade = CascadeType.REMOVE)
    @JsonIgnore
    private Set<ActiveRide> ridesAsDriver = new HashSet<>();

    @OneToMany(mappedBy = "driver")
    @JsonIgnore
    private Set<ArchivedRide> archivedRidesAsDriver = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "app_user_car",
            joinColumns = @JoinColumn(name = "app_user_id"),
            inverseJoinColumns = @JoinColumn(name = "car_id")
    )
    private Set<Car> cars = new HashSet<>();

    @OneToMany(mappedBy = "passenger", cascade = CascadeType.REMOVE)
    @JsonIgnore
    Set<RideReservation> ridesAsPassenger = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "app_user_archived_ride",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "archived_ride_id")
    )
    @JsonIgnore
    private Set<ArchivedRide> archivedRidesAsPassenger = new HashSet<>();

    @OneToMany(mappedBy = "passenger", cascade = CascadeType.REMOVE)
    Set<UserRating> writtenRates = new HashSet<>();

    @OneToMany(mappedBy = "driver", cascade = CascadeType.REMOVE)
    Set<UserRating> receivedRates = new HashSet<>();

    @Column(name = "unblock_datetime")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private ZonedDateTime unblockDateTime;


    @Override
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(role.name()));
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !this.blocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return !(this.blocked && this.unblockDateTime == null);
    }

    public void addCar(Car car) {
        this.cars.add(car);
        car.getOwners().add(this);
    }

    public void removeCar(Car car) {
        this.cars.remove(car);
        car.getOwners().remove(this);
    }

    public void addWrittenRate(UserRating rating) {
        this.writtenRates.add(rating);
        rating.setPassenger(this);
    }

    public void removeWrittenRate(UserRating rating) {
        this.writtenRates.remove(rating);
    }

    public void addReceivedRate(UserRating rating) {
        this.receivedRates.add(rating);
        rating.setDriver(this);
    }

    public void removeReceivedRate(UserRating rating) {
        this.receivedRates.remove(rating);
    }

    public void addRideAsDriver(ActiveRide ride) {
        this.ridesAsDriver.add(ride);
        ride.setDriver(this);
    }

    public void removeRideAsDriver(ActiveRide ride) {
        this.ridesAsDriver.remove(ride);
    }

    public void addRideAsPassenger(RideReservation reservation) {
        this.ridesAsPassenger.add(reservation);
    }

    public void removeRideAsPassenger(RideReservation reservation) {
        this.ridesAsPassenger.remove(reservation);
        reservation.setPassenger(null);
    }

    public void addArchivedRideAsDriver(ArchivedRide ride) {
        this.archivedRidesAsDriver.add(ride);
        ride.setDriver(this);
    }

    public void addArchivedRideAsPassenger(ArchivedRide ride) {
        this.archivedRidesAsPassenger.add(ride);
        ride.getPassengers().add(this);
    }
}
