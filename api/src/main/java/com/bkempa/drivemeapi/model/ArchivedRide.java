package com.bkempa.drivemeapi.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Table(name = "archived_ride")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class ArchivedRide extends Ride implements Serializable {

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "car_id")
    private Car car;

    @ManyToOne
    @JoinColumn(name = "driver_id")
    private User driver;

    @ManyToMany(mappedBy = "archivedRidesAsPassenger")
    private Set<User> passengers = new HashSet<>();

    public ArchivedRide(ActiveRide ride) {
        this.setStartingTime(ride.getStartingTime());
        this.setCar(ride.getCar());
        this.setDriver(ride.getDriver());
        ride.getReservations().forEach(rideReservation -> this.getPassengers().add(rideReservation.getPassenger()));
        this.setPrice(ride.getPrice());
        this.setStartLocation(ride.getStartLocation());
        this.setEndLocation(ride.getEndLocation());
        this.setDescription(ride.getDescription());
    }
}
