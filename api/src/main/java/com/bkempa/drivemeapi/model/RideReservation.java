package com.bkempa.drivemeapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Entity
@Table(name = "ride_reservation")
@Getter
@Setter
@NoArgsConstructor
public class RideReservation implements Serializable {

    @EmbeddedId
    private RideReservationKey id;

    @ManyToOne
    @MapsId("rideId")
    @JoinColumn(name = "ride_id")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private ActiveRide ride;

    @ManyToOne
    @MapsId("passengerId")
    @JoinColumn(name = "passenger_id")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private User passenger;

    @Column(name = "number_of_passengers")
    @Min(1)
    private int numberOfPassengers;
}
