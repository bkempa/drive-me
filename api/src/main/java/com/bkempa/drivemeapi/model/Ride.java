package com.bkempa.drivemeapi.model;

import com.bkempa.drivemeapi.utils.PointDeserializer;
import com.bkempa.drivemeapi.utils.PointSerializer;
import com.bkempa.drivemeapi.validation.NotInPast;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import lombok.Getter;
import lombok.Setter;
import org.locationtech.jts.geom.Point;

import java.time.ZonedDateTime;

@MappedSuperclass
@Getter
@Setter
public abstract class Ride {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "ride_id_seq")
    @SequenceGenerator(name = "ride_id_seq", sequenceName = "ride_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "starting_time", nullable = false)
    @NotInPast(message = "Starting time cannot be in the past")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private ZonedDateTime startingTime;

    @Column(nullable = false)
    @Min(value = 0, message = "Price cannot be negative")
    private int price;

    @Column(name = "start_location", nullable = false, columnDefinition = "Geography")
    @JsonDeserialize(using = PointDeserializer.class)
    @JsonSerialize(using = PointSerializer.class)
    private Point startLocation;

    @Column(name = "end_location", nullable = false, columnDefinition = "Geography")
    @JsonDeserialize(using = PointDeserializer.class)
    @JsonSerialize(using = PointSerializer.class)
    private Point endLocation;

    private String description;
}
