package com.bkempa.drivemeapi.model;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Embeddable
@EqualsAndHashCode
@Getter
@Setter
@NoArgsConstructor
public class RideReservationKey implements Serializable {
    @Column(name = "ride_id")
    private Long rideId;

    @Column(name = "passenger_id")
    private Long passengerId;
}
