package com.bkempa.drivemeapi.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;

import java.io.Serializable;
import java.time.ZonedDateTime;

@Entity(name = "app_user_rating")
@Getter
@Setter
@NoArgsConstructor
public class UserRating implements Serializable {
    @EmbeddedId
    private UserRatingKey id;

    @ManyToOne
    @MapsId("passengerId")
    @JoinColumn(name = "passenger_id")
    @JsonIgnore
    private User passenger;

    @ManyToOne
    @MapsId("driverId")
    @JoinColumn(name = "driver_id")
    @JsonIgnore
    private User driver;

    @Column(nullable = false)
    @Range(min = 1, max = 5, message = "Rate should be in range between 1 and 5")
    private int rate;

    private String comment;

    @Column(name = "rating_datetime", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private ZonedDateTime ratingDateTime;

    @Column(name = "edition_datetime")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private ZonedDateTime editionDateTime;
}