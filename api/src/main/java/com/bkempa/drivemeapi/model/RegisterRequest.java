package com.bkempa.drivemeapi.model;

import com.bkempa.drivemeapi.validation.DateOfBirth;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterRequest {
    private String username;
    @Pattern(regexp = "(?=^.{8,}$)(?=.*\\d)(?=.*[!@#$%^&*]+)(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*$",
            message = "The password must contain at least 8 characters, 1 uppercase and lowercase letter, a digit, and a special character")
    private String password;
    @Pattern(regexp = "^[A-Za-z]+[- ]?[A-Za-z]*$", message = "Enter a valid name")
    private String firstName;
    @Pattern(regexp = "^[A-Za-z]+[- ]?[A-Za-z]*$", message = "Enter a valid name")
    private String lastName;
    @DateOfBirth
    private LocalDate dateOfBirth;
    @Email(message = "Enter a valid email address")
    private String emailAddress;
    @Pattern(regexp = "^(?:\\+?[0-9]([- ]?[0-9])*)?$", message = "Enter a valid phone number")
    private String phoneNumber;
    private User.Role role;
}
