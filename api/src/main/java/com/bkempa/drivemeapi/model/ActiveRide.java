package com.bkempa.drivemeapi.model;

import com.bkempa.drivemeapi.exception.RideNotAvailableException;
import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Table(name = "ride")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class ActiveRide extends Ride implements Serializable {

    @Column(name = "places_available", nullable = false)
    @Min(value = 0, message = "Number of passengers cannot be negative")
    @Max(value = 8, message = "Number of passengers to high for a passenger car")
    private int placesAvailable;

    @ManyToOne
    @JoinColumn(name = "car_id", nullable = false)
    private Car car;

    @ManyToOne
    @JoinColumn(name = "driver_id", nullable = false)
    private User driver;

    @OneToMany(mappedBy = "ride", cascade = CascadeType.ALL)
    private Set<RideReservation> reservations = new HashSet<>();

    public void addReservation(RideReservation reservation) throws RideNotAvailableException {
        if (reservation.getNumberOfPassengers() <= this.getPlacesAvailable()) {
            this.getReservations().add(reservation);
            this.setPlacesAvailable(this.getPlacesAvailable() - reservation.getNumberOfPassengers());
        } else
            throw new RideNotAvailableException("Max number of passengers exceeded");
    }

    public void removeReservation(RideReservation reservation) {
        this.getReservations().remove(reservation);
        this.setPlacesAvailable(this.getPlacesAvailable() + reservation.getNumberOfPassengers());
        reservation.setRide(null);
    }
}
