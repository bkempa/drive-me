package com.bkempa.drivemeapi.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.time.ZoneId;
import java.time.ZonedDateTime;

public class NotInPastValidator implements ConstraintValidator<NotInPast, ZonedDateTime> {
    @Override
    public boolean isValid(ZonedDateTime localDateTime, ConstraintValidatorContext constraintValidatorContext) {
        if (localDateTime == null)
            return true;

        ZonedDateTime currentDateTime = ZonedDateTime.now(ZoneId.of("Europe/Warsaw"));
        return !localDateTime.isBefore(currentDateTime);
    }
}
