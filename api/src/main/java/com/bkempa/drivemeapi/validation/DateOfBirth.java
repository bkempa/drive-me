package com.bkempa.drivemeapi.validation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import jakarta.validation.ReportAsSingleViolation;
import jakarta.validation.constraints.NotNull;

import java.lang.annotation.*;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {DateOfBirthValidator.class})
@Documented
@ReportAsSingleViolation
@NotNull(message = "Date of birth must be provided")
public @interface DateOfBirth {
    String message() default "Date of birth has to be in the past";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
