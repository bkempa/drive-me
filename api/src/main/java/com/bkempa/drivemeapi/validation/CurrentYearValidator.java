package com.bkempa.drivemeapi.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.time.Year;

public class CurrentYearValidator implements ConstraintValidator<ValidCarProductionYear, Integer> {
    @Override
    public boolean isValid(Integer productionYear, ConstraintValidatorContext constraintValidatorContext) {
        if (productionYear == null) {
            return true;
        }

        int currentYear = Year.now().getValue();
        return productionYear <= currentYear;
    }
}
