package com.bkempa.drivemeapi.validation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import jakarta.validation.ReportAsSingleViolation;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;

import java.lang.annotation.*;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {CurrentYearValidator.class})
@Documented
@ReportAsSingleViolation
@Min(value = 1950, message = "Production year incorrect")
@NotNull(message = "Production year is required")
public @interface ValidCarProductionYear {
    String message() default "Invalid production year";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}