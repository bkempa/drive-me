package com.bkempa.drivemeapi.repository;

import com.bkempa.drivemeapi.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {
    Optional<Car> findByPlateNumber(String plateNumber);
    List<Car> findByProductionYearGreaterThanEqual(int minimumYear);
    boolean existsByPlateNumber(String plateNumber);

    @Query("SELECT c FROM car c JOIN c.owners o WHERE o.id = :userId")
    List<Car> findCarsByOwnerId(@Param("userId") Long userId);
}
