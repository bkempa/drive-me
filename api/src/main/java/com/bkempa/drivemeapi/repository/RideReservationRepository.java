package com.bkempa.drivemeapi.repository;

import com.bkempa.drivemeapi.model.RideReservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RideReservationRepository extends JpaRepository<RideReservation, Long> {
    Optional<RideReservation> findById_RideIdAndId_PassengerId(Long rideId, Long passengerId);
    List<RideReservation> findById_PassengerId(Long passengerId);
}
