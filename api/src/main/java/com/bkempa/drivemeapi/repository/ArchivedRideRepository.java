package com.bkempa.drivemeapi.repository;

import com.bkempa.drivemeapi.model.ArchivedRide;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.ZonedDateTime;
import java.util.List;

public interface ArchivedRideRepository extends JpaRepository<ArchivedRide, Long> {
    void deleteByStartingTimeBefore(ZonedDateTime dateTime);
    List<ArchivedRide> findByDriverId(Long driverId);
    List<ArchivedRide> findByPassengersId(Long passengerId);
}
