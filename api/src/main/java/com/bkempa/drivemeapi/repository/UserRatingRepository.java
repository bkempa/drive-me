package com.bkempa.drivemeapi.repository;

import com.bkempa.drivemeapi.model.UserRating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRatingRepository extends JpaRepository<UserRating, Long> {
    List<UserRating> findAllByDriver_UsernameOrderByRatingDateTimeAsc(String username);
    List<UserRating> findAllByDriver_IdOrderByRatingDateTimeAsc(Long driverId);
    List<UserRating> findAllByPassenger_UsernameOrderByRatingDateTimeAsc(String username);
    List<UserRating> findAllByPassenger_IdOrderByRatingDateTimeAsc(Long driverId);
    Optional<UserRating> findById_DriverIdAndId_PassengerId(Long driverId, Long passengerId);
}
