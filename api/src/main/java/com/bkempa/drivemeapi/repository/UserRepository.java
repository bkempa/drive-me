package com.bkempa.drivemeapi.repository;

import com.bkempa.drivemeapi.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
    Optional<User> findByEmailAddress(String emailAddress);
    List<User> findByBlockedAndUnblockDateTimeIsBefore(boolean isBlocked, ZonedDateTime currentDateTime);
}
