package com.bkempa.drivemeapi.repository;

import com.bkempa.drivemeapi.model.ActiveRide;
import org.locationtech.jts.geom.Point;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;

@Repository
public interface ActiveRideRepository extends JpaRepository<ActiveRide, Long> {
    List<ActiveRide> findByStartingTimeAfter(ZonedDateTime startingTime);
    List<ActiveRide> findByPlacesAvailableGreaterThanEqual(int expectedPlaces);
    List<ActiveRide> findByCarId(Long carId);
    List<ActiveRide> findByDriverId(Long driverId);
    List<ActiveRide> findByPriceLessThanEqual(int maxPrice);
    List<ActiveRide> findByStartingTimeBefore(ZonedDateTime dateTime);
    List<ActiveRide> findByStartingTimeAfterAndStartingTimeBefore(ZonedDateTime minTime, ZonedDateTime maxTime);

    @Query(value = "SELECT * FROM ride WHERE st_distance(start_location, :point, false) < :distanceInMeters", nativeQuery = true)
    List<ActiveRide> findRidesStartingWithinDistance(Point point, int distanceInMeters);

    @Query(value = "SELECT * FROM ride WHERE st_distance(end_location, :point, false) < :distanceInMeters", nativeQuery = true)
    List<ActiveRide> findRidesEndingWithinDistance(Point point, int distanceInMeters);

    @Query(value = "SELECT * FROM ride WHERE st_distance(start_location, :start, false) < :distanceInMeters AND st_distance(end_location, :end, false) < :distanceInMeters", nativeQuery = true)
    List<ActiveRide> findRidesStartingAndEndingWithinDistance(Point start, Point end, int distanceInMeters);
}
