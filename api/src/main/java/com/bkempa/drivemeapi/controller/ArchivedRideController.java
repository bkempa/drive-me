package com.bkempa.drivemeapi.controller;

import com.bkempa.drivemeapi.model.ArchivedRide;
import com.bkempa.drivemeapi.service.ArchivedRideService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rides/archive")
public class ArchivedRideController {

    private final ArchivedRideService archivedRideService;

    @GetMapping("/all")
    public List<ArchivedRide> getAllArchivedRides() {
        return archivedRideService.getAllArchivedRides();
    }

    @GetMapping("/driver/{driverId}")
    public List<ArchivedRide> getAllArchivedRidesByDriverId(@PathVariable Long driverId) {
        return archivedRideService.getAllArchivedRidesByDriverId(driverId);
    }

    @GetMapping("/passenger/{passengerId}")
    public List<ArchivedRide> getAllArchivedRidesByPassengerId(@PathVariable Long passengerId) {
        return archivedRideService.getAllArchivedRidesByPassengerId(passengerId);
    }
}
