package com.bkempa.drivemeapi.controller;

import com.bkempa.drivemeapi.exception.CarNotFoundException;
import com.bkempa.drivemeapi.exception.UserNotFoundException;
import com.bkempa.drivemeapi.model.Car;
import com.bkempa.drivemeapi.service.CarService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/cars")
public class CarController {

    private final CarService carService;

    @GetMapping("/all")
    public List<Car> getAllCars() {
        return carService.getAllCars();
    }

    @PostMapping("")
    public void addCar(@RequestParam Long userId, @Valid @RequestBody Car newCar) throws UserNotFoundException {
        carService.addCar(userId, newCar);
    }

    @PatchMapping("/{carId}")
    public void removeCarFromUser(@RequestParam Long userId, @PathVariable Long carId) throws UserNotFoundException, CarNotFoundException {
        carService.removeCarFromUser(userId, carId);
    }

    @DeleteMapping("/{id}")
    public void deleteCarById(@PathVariable Long id) throws CarNotFoundException {
        carService.deleteCarById(id);
    }

    @GetMapping("/{id}")
    public Car getCarById(@PathVariable Long id) throws CarNotFoundException {
        return carService.getCarById(id);
    }

    @GetMapping("/plate/{plateNumber}")
    public Car getCarByPlateNumber(@PathVariable String plateNumber) throws CarNotFoundException {
        return carService.getCarByPlateNumber(plateNumber);
    }

    @PutMapping("/{id}")
    public void updateCarById(@PathVariable Long id, @Valid @RequestBody Car updatedCar) throws CarNotFoundException {
        carService.updateCarById(id, updatedCar);
    }

    @DeleteMapping("/plate/{plateNumber}")
    public void deleteCarByPlateNumber(@PathVariable String plateNumber) throws CarNotFoundException {
        carService.deleteCarByPlateNumber(plateNumber);
    }

    @GetMapping("/year/{year}")
    public List<Car> getAllCarsByMinimumProductionYear(@PathVariable int year) {
        return carService.getAllCarsByMinimumProductionYear(year);
    }

    @GetMapping("/owner/{userId}")
    public List<Car> getUserCars(@PathVariable Long userId) {
        return carService.getUserCars(userId);
    }
}
