package com.bkempa.drivemeapi.controller;

import com.bkempa.drivemeapi.exception.ReservationNotFoundException;
import com.bkempa.drivemeapi.exception.RideNotAvailableException;
import com.bkempa.drivemeapi.model.RideReservation;
import com.bkempa.drivemeapi.service.RideReservationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rides/reservations")
public class RideReservationController {

    private final RideReservationService rideReservationService;

    @GetMapping("/all")
    public List<RideReservation> getAllReservations() {
        return rideReservationService.getAllReservations();
    }

    @GetMapping("/{rideId}")
    public RideReservation getReservationByRideIdAndPassengerId(@PathVariable Long rideId, @RequestParam Long passengerId) throws ReservationNotFoundException {
        return rideReservationService.getReservationByRideIdAndPassengerId(rideId, passengerId);
    }

    @PostMapping("")
    public void addRideReservation(@RequestBody RideReservation reservation) throws RideNotAvailableException {
        rideReservationService.addRideReservation(reservation);
    }

    @DeleteMapping("/{rideId}")
    public void deleteReservation(@PathVariable Long rideId, @RequestParam Long passengerId) throws ReservationNotFoundException {
        rideReservationService.deleteRideReservationByIds(rideId, passengerId);
    }
}
