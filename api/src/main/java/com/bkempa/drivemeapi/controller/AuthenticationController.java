package com.bkempa.drivemeapi.controller;

import com.bkempa.drivemeapi.exception.RegisteredEmailException;
import com.bkempa.drivemeapi.exception.UserBlockedException;
import com.bkempa.drivemeapi.exception.UserNotFoundException;
import com.bkempa.drivemeapi.exception.UsernameExistsException;
import com.bkempa.drivemeapi.model.AuthenticationRequest;
import com.bkempa.drivemeapi.model.AuthenticationResponse;
import com.bkempa.drivemeapi.model.RegisterRequest;
import com.bkempa.drivemeapi.service.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user/auth")
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @PostMapping("/register")
    public AuthenticationResponse register(@RequestBody RegisterRequest request) throws RegisteredEmailException, UsernameExistsException {
        return authenticationService.register(request);
    }

    @PostMapping("/authenticate")
    public AuthenticationResponse authenticate(@RequestBody AuthenticationRequest request) throws UserNotFoundException, UserBlockedException {
        return authenticationService.authenticate(request);
    }
}
