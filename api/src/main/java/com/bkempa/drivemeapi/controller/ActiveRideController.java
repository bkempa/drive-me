package com.bkempa.drivemeapi.controller;

import com.bkempa.drivemeapi.exception.*;
import com.bkempa.drivemeapi.model.ActiveRide;
import com.bkempa.drivemeapi.service.ActiveRideService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.ZonedDateTime;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/rides")
public class ActiveRideController {

    private final ActiveRideService activeRideService;

    @GetMapping("/all")
    public List<ActiveRide> getAllRides() {
        return activeRideService.getAllRides();
    }

    @GetMapping("/{id}")
    public ActiveRide getRideById(@PathVariable Long id) throws RideNotFoundException {
        return activeRideService.getRideById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteRideById(@PathVariable Long id) throws RideNotFoundException {
        activeRideService.deleteRideById(id);
    }

    @PostMapping("")
    public void addRideAsDriver(@Valid @RequestBody ActiveRide ride) throws UserNotFoundException, CarNotFoundException {
        activeRideService.addRideAsDriver(ride);
    }

    @DeleteMapping("")
    public void removeRideAsDriver(@RequestParam Long driverId, @RequestParam Long rideId) throws UserNotFoundException, RideNotFoundException {
        activeRideService.removeRideAsDriver(driverId, rideId);
    }

    @GetMapping("/starting-time/{startingTime}")
    public List<ActiveRide> getRidesWithStartingTimeAfter(@PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) ZonedDateTime startingTime) {
        return activeRideService.getRidesWithStartingTimeAfter(startingTime);
    }

    @GetMapping("/between-time/{minTime}/{maxTime}")
    public List<ActiveRide> getRidesWithStartingTimeBetween(@PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) ZonedDateTime minTime,
                                                            @PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) ZonedDateTime maxTime) {
        return activeRideService.getRidesWithStartingTimeBetween(minTime, maxTime);
    }

    @GetMapping("/min-places/{minimumPlaces}")
    public List<ActiveRide> getRidesWithAtLeastPlaces(@PathVariable int minimumPlaces) {
        return activeRideService.getRidesWithMinimumPlacesAvailable(minimumPlaces);
    }

    @GetMapping("/car/{carId}")
    public List<ActiveRide> getRidesByCarId(@PathVariable Long carId) {
        return activeRideService.getRidesByCarId(carId);
    }

    @GetMapping("/driver/{driverId}")
    public List<ActiveRide> getRidesByDriverId(@PathVariable Long driverId) {
        return activeRideService.getRidesByDriverId(driverId);
    }

    @GetMapping("/passenger/{passengerId}")
    public List<ActiveRide> getRidesByPassengerId(@PathVariable Long passengerId) {
        return activeRideService.getRidesByPassengerId(passengerId);
    }

    @GetMapping("/max-price/{maxPrice}")
    public List<ActiveRide> getRidesWithMaximumPrice(@PathVariable int maxPrice) {
        return activeRideService.getRidesByMaximumPrice(maxPrice);
    }

    @GetMapping("/start")
    public List<ActiveRide> getRidesByStartLocation(@RequestParam(name = "lat") double startLocationLatitude,
                                              @RequestParam(name = "lon") double startLocationLongitude,
                                              @RequestParam(name = "dist") int distanceInMeters) {
        return activeRideService.getRidesByStartLocationWithinDistance(startLocationLatitude, startLocationLongitude, distanceInMeters);
    }

    @GetMapping("/end")
    public List<ActiveRide> getRidesByEndLocation(@RequestParam(name = "lat") double endLocationLatitude,
                                            @RequestParam(name = "lon") double endLocationLongitude,
                                            @RequestParam(name = "dist") int distanceInMeters) {
        return activeRideService.getRidesByEndLocationWithinDistance(endLocationLatitude, endLocationLongitude, distanceInMeters);
    }

    @GetMapping("/start-end")
    public List<ActiveRide> getRidesByStartAndEndLocation(@RequestParam(name = "start-lat") double startLocationLatitude,
                                                    @RequestParam(name = "start-lon") double startLocationLongitude,
                                                    @RequestParam(name = "end-lat") double endLocationLatitude,
                                                    @RequestParam(name = "end-lon") double endLocationLongitude,
                                                    @RequestParam(name = "dist") int distanceInMeters) {
        return activeRideService.getRidesByStartAndEndLocationWithinDistance(startLocationLatitude, startLocationLongitude,
                endLocationLatitude, endLocationLongitude, distanceInMeters);
    }

    @PutMapping("/{id}")
    public void updateRideById(@PathVariable Long id, @Valid @RequestBody ActiveRide updatedRide) throws RideNotFoundException {
        activeRideService.updateRideById(id, updatedRide);
    }
}
