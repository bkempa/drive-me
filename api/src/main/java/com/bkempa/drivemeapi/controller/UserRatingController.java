package com.bkempa.drivemeapi.controller;

import com.bkempa.drivemeapi.exception.RatingNotFoundException;
import com.bkempa.drivemeapi.exception.UserNotFoundException;
import com.bkempa.drivemeapi.model.UserRating;
import com.bkempa.drivemeapi.service.UserRatingService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/user/ratings")
public class UserRatingController {

    private final UserRatingService userRatingService;

    @GetMapping("/all")
    public List<UserRating> getAllRatings() {
        return userRatingService.getAllRatings();
    }

    @GetMapping("/username/{username}/received")
    public List<UserRating> getAllReceivedRatingsByUsername(@PathVariable String username) {
        return userRatingService.getAllReceivedRatingsByUsername(username);
    }

    @GetMapping("/username/{username}/written")
    public List<UserRating> getAllWrittenRatingsByUsername(@PathVariable String username) {
        return userRatingService.getAllWrittenRatingsByUsername(username);
    }

    @GetMapping("/id/{id}/received")
    public List<UserRating> getAllReceivedRatingsByUserId(@PathVariable Long id) {
        return userRatingService.getAllReceivedRatingsByUserId(id);
    }

    @GetMapping("/id/{id}/written")
    public List<UserRating> getAllWrittenRatingsByUserId(@PathVariable Long id) {
        return userRatingService.getAllWrittenRatingsByUserId(id);
    }

    @GetMapping("/{id}")
    public UserRating getRatingById(@PathVariable Long id) throws RatingNotFoundException {
        return userRatingService.getRatingById(id);
    }

    @PostMapping("")
    public void postNewRating(@Valid @RequestBody UserRating rating) throws UserNotFoundException {
        userRatingService.postRating(rating);
    }

    @DeleteMapping("")
    public void removeRating(@RequestParam Long authorId, @RequestParam Long driverId) throws RatingNotFoundException {
        userRatingService.removeRating(authorId, driverId);
    }

    @PutMapping("")
    public void updateRating(@Valid @RequestBody UserRating rating) throws RatingNotFoundException {
        userRatingService.updateRating(rating);
    }
}
