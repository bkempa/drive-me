package com.bkempa.drivemeapi.controller;

import com.bkempa.drivemeapi.exception.*;
import com.bkempa.drivemeapi.model.User;
import com.bkempa.drivemeapi.service.UserDeletionHandleService;
import com.bkempa.drivemeapi.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/user")
public class UserController {

    private final UserService userService;
    private final UserDeletionHandleService userDeletionHandleService;

    @GetMapping("/all")
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/{id}")
    public User getUserById(@PathVariable Long id) throws UserNotFoundException {
        return userService.getUserById(id);
    }

    @PostMapping("/new")
    public void createUser(@Valid @RequestBody User user) throws RegisteredEmailException, UsernameExistsException {
        userService.createUser(user);
    }

    @PutMapping("/{id}")
    public void updateUser(@PathVariable Long id, @Valid @RequestBody User updatedUser) throws UserNotFoundException {
        userService.updateUser(id, updatedUser);
    }

    @DeleteMapping("/{id}")
    public void deleteUserById(@PathVariable Long id) throws UserNotFoundException, CarNotFoundException {
        userDeletionHandleService.deleteUserById(id);
    }

    @DeleteMapping("")
    public void deleteUser(@Valid @RequestBody User user) throws CarNotFoundException {
        userDeletionHandleService.deleteUser(user);
    }

    @GetMapping("/username/{username}")
    public User getUserByUsername(@PathVariable String username) throws UserNotFoundException {
        return userService.getUserByUsername(username);
    }

    @GetMapping("/email/{email}")
    public User getUserByEmailAddress(@PathVariable String email) throws UserNotFoundException {
        return userService.getUserByEmailAddress(email);
    }

    @PatchMapping("/{id}/block/{days}")
    public void blockUserById(@PathVariable Long id, @PathVariable int days) throws UserNotFoundException, RideNotFoundException {
        userDeletionHandleService.blockUserById(id, days);
    }

    @PatchMapping("/{id}/unblock")
    public void unblockUserById(@PathVariable Long id) throws UserNotFoundException {
        userDeletionHandleService.unblockUserById(id);
    }

    @PutMapping("/block/{days}")
    public void blockUser(@Valid @RequestBody User user, @PathVariable int days) throws RideNotFoundException {
        userDeletionHandleService.blockUser(user, days);
    }

    @PutMapping("/unblock")
    public void unblockUser(@Valid @RequestBody User user) {
        userDeletionHandleService.unblockUser(user);
    }

    @PatchMapping("/{id}/block/perm")
    public void blockUserForeverById(@PathVariable Long id) throws UserNotFoundException, RideNotFoundException {
        userDeletionHandleService.blockUserForeverById(id);
    }

    @PutMapping("/block/perm")
    public void blockUserForever(@Valid @RequestBody User user) throws RideNotFoundException {
        userDeletionHandleService.blockUserForever(user);
    }

    @PutMapping("/admin/grant/{id}")
    public void grantAdminRightsToUserById(@PathVariable Long id) throws UserNotFoundException {
        userService.grantAdminRightsToUserById(id);
    }

    @PutMapping("/admin/revoke/{id}")
    public void revokeAdminRightsToUserById(@PathVariable Long id) throws UserNotFoundException {
        userService.revokeAdminRightsToUserById(id);
    }

    @PutMapping("/admin/grant")
    public void grantAdminRightsToUser(@Valid @RequestBody User user) {
        userService.grantAdminRightsToUser(user);
    }

    @PutMapping("/admin/revoke")
    public void revokeAdminRightsToUser(@Valid @RequestBody User user) {
        userService.revokeAdminRightsToUser(user);
    }
}
