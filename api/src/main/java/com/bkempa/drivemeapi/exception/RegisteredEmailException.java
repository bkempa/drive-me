package com.bkempa.drivemeapi.exception;

public class RegisteredEmailException extends Exception {
    public RegisteredEmailException(String s) {
        super(s);
    }

    public RegisteredEmailException(String message, Throwable cause) {
        super(message, cause);
    }

    public RegisteredEmailException(Throwable cause) {
        super(cause);
    }

    public RegisteredEmailException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public RegisteredEmailException() {
    }
}
