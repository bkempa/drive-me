package com.bkempa.drivemeapi.exception;

public class RideNotFoundException extends Exception {
    public RideNotFoundException() {
    }

    public RideNotFoundException(String message) {
        super(message);
    }

    public RideNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public RideNotFoundException(Throwable cause) {
        super(cause);
    }

    public RideNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
