package com.bkempa.drivemeapi.exception;

public class RideNotAvailableException extends Exception {
    public RideNotAvailableException() {
    }

    public RideNotAvailableException(String message) {
        super(message);
    }

    public RideNotAvailableException(String message, Throwable cause) {
        super(message, cause);
    }

    public RideNotAvailableException(Throwable cause) {
        super(cause);
    }

    public RideNotAvailableException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
